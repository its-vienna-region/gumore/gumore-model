#!/usr/bin/env python3
""" here are the functions to be used in the Gumore model"""

import math
from enum import Enum

# import append_sys_path


def binary_step(x, step_value=0.0):
    if x < step_value:
        return 0
    else:
        return 1


def logistic_distr(x, median=0.0, gradient=1.0):
    """ The cumulative function of the logistic distribution
        Paramters:
        x .......... independent variable
        median ..... the mean and median of the distribution
        gradient ... the bigger the steeper the function
        siehe https://de.wikipedia.org/wiki/Logistische_Verteilung
    """
    flattness = 0.25 / gradient
    retv = 1.0 / (1.0 + math.exp(-(x - median) / flattness))
    return retv


def logistic_mode(dist1, dist2, median=0.0, gradient=1.0):
    """ The logistic distribution for mode calculation
        Parameters:
        dist1 .... distance for first mode
        dist2 .... distance for second mode
        median,gradient ... parameters of the function
    """
    x = 2.0 * (dist1 - dist2) / (dist1 - dist2)
    return logistic_distr(x, median, gradient)
