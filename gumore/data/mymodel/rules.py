#!/usr/bin/env python3
""" This file should be used, to define 
the Gumore model, which is:
 - the traffic generation and distribution and
 - the rules for mode assignment and flow splitting

The traffic generation classes are derived from GumoreGen.
They have some attributes and a distribute() function
Relevant attributes are:
    commodity ..... the class of commodity, that is generated    
    generator ..... the attribute of the cells, that's quantity generates the flows
    mobilityrate .. the how many flows does a source attraction quantity generate


The rules are classes derived from GumoreRule
with basically 2 functions:
functions:
    - condition .. returns True if the rule applies
    - action ..... returns the modal split
"""

# import append_sys_path

import gumore.core.rules.means
from gumore.core.rules.means import GumoreGen
import gumore.core.runner
from gumore.core.rules.vehicle import Vehicle
from gumore.core.entities import Z, LS, L75, L26, LX, TransportMeansMix
from gumore.core.entities import com_nst01, com_nst02, com_nst03, com_nst04, com_nst09

# this singleton contains the context
app = gumore.core.runner.gumore_app
register = app.register


######################
# HOTSPOT SETTINGS
######################
size_to_factor = {1: 1.05, 2: 1.1, 3: 1.2}

######################
# TRAFFIC GENERATION
# and
# TRAFFIC DISTRIBUTION
######################

GumoreGen.flow_distribution_mode = "tpp_column_min"
GumoreGen.balance = False


# @register
class GenNST01(GumoreGen):
    commodity = com_nst01
    coefs = {"prod": {"A": 179.08280241798332}, "att": {"A": 175.4679004054318}}
    alpha = 1.75


# @register
class GenNST02(GumoreGen):
    commodity = com_nst02
    coefs = {
        "prod": {"B": 149.29838455438954},
        "att": {"B": 68.85127882534846, "D": 20.921596855981456},
    }
    alpha = 1.0


@register
class GenNST03(GumoreGen):
    commodity = com_nst03
    coefs = {
        "prod": {"B": 22458.589531257316},
        "att": {"population": 17.57},
    }  # {"B": 22311.231451409858}}
    # alpha = 3.25
    alpha = 2.0


# @register
class GenNST09(GumoreGen):
    commodity = com_nst09
    coefs = {"prod": {"B": 7371.625084909427}, "att": {"F": 187.63965954633258}}
    alpha = 1.75


# @register
# class NST01(GumoreGen):
#     commodity = com_nst01
#     generator = "einwohner"
#     reverse = True
#     mobilityrate = 100
#
#     def distribute(self, i):
#         return self.potential(i, "besch_a", "einwohner", "hgv_hours", alfa=1.5)


##################
# TRANSPORT MEANS ASSIGNMENT
# and
# FLOW SPLITTING
##################


# @register
class SimpleLS(gumore.core.rules.means.GumoreRule):
    mindist = 0.01
    maxdist = 3
    distmat_name = "hgv_hours"

    def condition(self, flow):
        if flow.commodity not in (self.ent.com_nst01, self.ent.com_nst02):
            return False
        if self.distmat(flow.f, flow.t) <= self.maxdist:
            return True

    def action(self, flow):
        flow.setmeans(self.ent.LS)


# @register
class SplitNST01(
    gumore.core.rules.means.GumoreRule
):  # TODO: modify means assignment results when splitting
    mindist = 5
    maxdist_term_t = 2
    distmat_name = "hgv_hours"

    def condition(self, flow):
        if flow.f == flow.t:
            return False
        if flow.commodity != self.ent.com_nst01:
            return False
        terminal = self.findzones.containerTerminal(self, self.distmat)
        if flow.t == terminal(flow.t).id:
            return False
        if terminal.distance_to(flow.t) > self.maxdist_term_t:
            return False
        if self.distmat(flow.f, flow.t) > self.mindist:
            return True

    def action(self, flow):
        terminal = self.findzones.containerTerminal(self, self.distmat)
        for leg in flow.split(terminal(flow.t)):
            if leg.f == flow.f:
                leg.setmeans(self.ent.L26)
            else:
                leg.setmeans(self.ent.LS)


# NST 02 -------------------------------------------------------------------------------


# @register
class RuleNST02(gumore.core.rules.means.GumoreRule):
    distmat_name = "hgv_km"

    def condition(self, flow):
        if flow.commodity != self.ent.com_nst02:
            return False
        self.freight_yard = self.findzones.FreightYard(self, self.distmat)
        self.cells_ratios = self.freight_yard(flow.f) + self.freight_yard(flow.t)
        # Check if source freight yard is the same as target freight yard
        if self.cells_ratios[0].id == self.cells_ratios[1].id:
            self.case = 0
        else:
            self.case = 1
        return True

    def action(self, flow):
        if self.case == 0:
            flow.setmeans(self.ent.L26)
        elif self.case == 1:
            flow.reload(self.cells_ratios, [self.ent.L26, self.ent.Z, self.ent.L26])


# NST 03 -------------------------------------------------------------------------------


@register
class RuleNST03(gumore.core.rules.means.GumoreRule):
    distmat_name = "hgv_km"

    def condition(self, flow):
        if flow.commodity != self.ent.com_nst03:
            return False
        return True

    def action(self, flow):
        flow.setmeans([self.ent.L75, self.ent.L26, self.ent.LX], [0.2, 0.6, 0.2])


# NST 09 -------------------------------------------------------------------------------


# @register
class RuleNST09(gumore.core.rules.means.GumoreRule):
    distmat_name = "hgv_km"

    def condition(self, flow):
        if flow.commodity != self.ent.com_nst09:
            return False
        flow_distance = self.get_flow_distance(flow)
        if flow_distance >= 1000:
            self.case = 1
        elif (flow_distance < 1000) and (flow_distance >= 100):
            self.case = 2
        elif flow_distance < 100:
            self.case = 3
        return True

    def action(self, flow):
        if self.case == 1:
            flow.reload(self.cells_ratios, [self.ent.LX, self.ent.Z, self.ent.LX])
        elif self.case == 2:
            if self.zone_attribute_value(flow.f, "gbhf") & self.zone_attribute_value(
                flow.t, "gbhf"
            ):
                flow.setmeans(self.ent.Z)
            else:
                flow.setmeans(self.ent.LX)
        elif self.case == 3:
            flow.setmeans([self.ent.LX, self.ent.L26], [0.5, 0.5])


@register
class AllLX(gumore.core.rules.means.GumoreRule):
    """Default Rule, must be last rule: all the rest is 'LX' """

    def condition(self, flow):
        return True

    def action(self, flow):
        flow.setmeans(self.ent.LX)


###########################
# VEHICLE COUNT CALCULATION
###########################


# @register
class LSSpecializedNST01(Vehicle):
    name = "LS Specialized nst01"
    means = LS
    payload_distmat_name = "hgv_km"
    load = [com_nst01]

    def empty(self, flow):
        return self.trips.loc[flow.t, flow.f] * 0.5
        # return max(self.trips.loc[flow.t,flow.f]-self.trips.loc[flow.f,flow.t], 0)


# @register
class LSSpecializedNST02(Vehicle):
    name = "LS Specialized nst02"
    means = LS
    payload_distmat_name = "hgv_km"
    load = [com_nst02]

    def empty(self, flow):
        return self.trips.loc[flow.t, flow.f] * 0.2


# @register
class L26Coal(Vehicle):
    name = "L26 Coal"
    means = L26
    payload_distmat_name = "hgv_km"
    load = [com_nst02]

    def empty(self, flow):
        return self.trips.loc[flow.t, flow.f] * 0.8


@register
class L75NST03(Vehicle):
    name = "L75 NST 03"
    means = L75
    payload_distmat_name = "hgv_km"
    load = [com_nst03]

    def empty(self, flow):
        return self.trips.loc[flow.t, flow.f] * 0.5


# L26 ----------------------------------------------------------------------------------


# @register
class L26NST09(Vehicle):
    name = "L26 NST 09"
    means = L26
    payload_distmat_name = "hgv_km"
    load = [com_nst09]

    def empty(self, flow):
        return self.trips.loc[flow.t, flow.f] * 0.5


@register
class L26Allround(Vehicle):
    name = "L26 Allround"
    means = L26
    payload_distmat_name = "hgv_km"
    load = [com_nst01, com_nst02, com_nst03, com_nst04]

    def empty(self, flow):
        return self.trips.loc[flow.t, flow.f] * 0.5


# LX -----------------------------------------------------------------------------------


# @register
class LXNST09(Vehicle):
    name = "LX NST 09"
    means = LX
    payload_distmat_name = "hgv_km"
    load = [com_nst09]

    def empty(self, flow):
        return self.trips.loc[flow.t, flow.f] * 0.5


@register
class LXAllround(Vehicle):
    name = "LX Allround"
    means = LX
    payload_distmat_name = "hgv_km"
    load = [com_nst01, com_nst02, com_nst03, com_nst04]

    def empty(self, flow):
        return self.trips.loc[flow.t, flow.f] * 0.5


# Z ------------------------------------------------------------------------------------
@register
class ZAllround(Vehicle):
    name = "Z Allround"
    means = Z
    payload_distmat_name = "hgv_km"
    load = [com_nst01, com_nst02, com_nst03, com_nst04, com_nst09]

    def empty(self, flow):
        return self.trips.loc[flow.t, flow.f] * 0
