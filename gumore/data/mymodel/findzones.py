from gumore.core.findzone import FindZone


class containerTerminal(FindZone):
    """ Cell finder for the container terminal: used in SplitContainer
        count = 1 ... select only the nearest Cell
        mindist ..... give some distance to Cell interior flows
        select() .... use only cells, that have an attribute 'umstp'
    """

    mindist = 1.0
    count = 1

    def select(self, c):
        """ select only zones, that have an attribute 'gvz' """
        if self.zone_has_attribute("gvz"):
            return 1.0
        return False


class FreightYard(FindZone):
    count = 1

    def select(self, c):
        if self.zone_attribute_is_true("gbhf"):
            return 1.0
        return False
