"""
Module for traffic assignment.
"""

from typing import Iterable

# import append_sys_path
from gumore.util.conf import (
    get_config,
    get_graph_table_name,
    get_path_table_name,
    scenario_name,
    is_scenario,
)
from gumore.util.database import get_db_connection, create_cursor, TABLES
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


def assign_trips(modes: Iterable[str] = ("hgv",)) -> None:
    """
    Calculates traffic assignment and writes the results into the database.

    The individual trips in the `trips` table are aggregated for all network links,
    grouped by transport means. Results are stored in the table `assignment`.
    Additionally, the views `geoview__assignment` and `geoview__assignment__hgv` is 
    created which join the link geometries for GIS visualization.

    Relevant config sections:

        [database.<DBNAME>]
        graph_table_name=<TABLE_NAME>
        zones_table_name=<TABLE_NAME>

    Args:
        modes (`Iterable[str]`, defaults to `('hgv',)`): Modes for which the traffic
            assignment shall be calculated
    """
    conn = get_db_connection()
    c = create_cursor(conn)
    graph_table_name = get_graph_table_name()

    for mode in modes:
        path_table_name = get_path_table_name(mode)
        logger.info(f"[ ] Starting assignment for mode '{mode}' using path table {path_table_name}")

        sql = f"""
            INSERT into {TABLES['assignment']} (ways_id, means, count)
            SELECT p.ways_id, t.means, sum(t.count)
            FROM 
                {path_table_name} p,
                {TABLES['trips']} t
            where
                t.f = p.vb_from
                and t.t = p.vb_to
            group by
                p.ways_id,
                t.means"""
        c.execute(sql)

        logger.info(f"[X] Finished assignment for mode '{mode}'")

    sql = f"""
        CREATE OR REPLACE VIEW {TABLES['geoview__assignment']} AS
        SELECT 
            a.*, 
            CASE
		        WHEN g.reverse_cost < 0 THEN a.count / 2
		        ELSE 0
	        END drawing_offset,
            g.geom_way
        FROM
            {TABLES['assignment']} a,
            {graph_table_name} g
        WHERE
            a.ways_id = g.id"""
    c.execute(sql)

    means_hgv = get_config().get("count_values", "model_classes_hgv").split(",")
    means_hgv_str = f"{means_hgv}"[1:-1]
    sql = f"""
        CREATE OR REPLACE VIEW {TABLES['geoview__assignment']}_hgv AS
        SELECT a.ways_id, sum(a.count) count, g.reverse_cost = -1 isoneway, g.geom_way
        FROM
            {TABLES['assignment']} a,
            {graph_table_name} g
        WHERE
            a.ways_id = g.id
            and a.means in ({means_hgv_str})
        GROUP BY
            a.ways_id, g.reverse_cost, g.geom_way"""
    c.execute(sql)

    if is_scenario():
        sql = f"""
            CREATE OR REPLACE VIEW {scenario_name()}.geoview__assignment_diff AS
            SELECT DISTINCT ON (s.geom_way)
                s.ways_id,
                s.means,
                sum(s.count) count_diff,
                s.geom_way
            FROM (
                SELECT 
                    ways_id,
                    means,
                    count * -1 count,
                    geom_way
                FROM
                    baseline.geoview__assignment
                UNION
                SELECT 
                    ways_id,
                    means,
                    count,
                    geom_way
                FROM
                    {scenario_name()}.geoview__assignment
            ) s
            GROUP BY s.ways_id, s.means, s.geom_way"""
        c.execute(sql)

    conn.commit()

def assign_meansflows(modes: Iterable[str] = ("hgv",)) -> None:
    # TODO: Handle road and train individually
    commodities = (
        get_config()
        .get("model_run", "commodities_for_meansflows_assignment")
        .split(","))
    commodities_str = f"{commodities}"[1:-1]
    means_hgv = get_config().get("count_values", "model_classes_assignment_qty").split(",")
    means_hgv_str = f"{means_hgv}"[1:-1]

    conn = get_db_connection()
    c = create_cursor(conn)
    graph_table_name = get_graph_table_name()

    for mode in modes:
        logger.info(f"[ ] Starting meansflows assignment for mode '{mode}'")
        path_table_name = get_path_table_name(mode)

        sql = f"""
            INSERT into {TABLES['assignment_qty']} (ways_id, commodity, qty)
            SELECT p.ways_id, m.commodity, sum(m.qty)
            FROM 
                {path_table_name} p,
                {TABLES['meansflows']} m
            where
                m.commodity in ({commodities_str})
                and m.means in ({means_hgv_str})
                and m.f = p.vb_from
                and m.t = p.vb_to
            group by
                p.ways_id,
                m.commodity"""
        c.execute(sql)

        logger.info(f"[X] Finished meansflows assignment for mode '{mode}'")

    sql = f"""
        CREATE OR REPLACE VIEW {TABLES['geoview__assignment_qty']} AS
        SELECT 
            a.*, 
            CASE
		        WHEN g.reverse_cost < 0 THEN a.qty / 2
		        ELSE 0
	        END drawing_offset,
            g.geom_way
        FROM
            {TABLES['assignment_qty']} a,
            {graph_table_name} g
        WHERE
            a.ways_id = g.id"""
    c.execute(sql)

    c.execute(f"DROP VIEW IF EXISTS {TABLES['geoview__assignment_qty']}_piv")
    sql = f"""
        CREATE VIEW {TABLES['geoview__assignment_qty']}_piv AS
        select 
            ways_id, {get_config().get("model_run", "commodities_for_meansflows_assignment")},
            {" + ".join([f"coalesce({x}, 0)" for x in commodities])} qty_total,
            g.reverse_cost,
            g.geom_way
        from
            {graph_table_name} g,
            crosstab('
                select ways_id, commodity, qty
                from {TABLES['geoview__assignment_qty']}
                where """
    for com in commodities:
        sql += f"commodity = ''{com}'' or "
    sql = sql[:-4]
    sql += """
                order by 1, 2') 
                as ct(ways_id int4, """
    for com in commodities:
        sql += f"{com} float4, "
    sql = sql[:-2]
    sql +=""")
        WHERE
            ways_id = g.id"""
    c.execute(sql)
    conn.commit()

              