from collections import defaultdict
import json
from typing import Dict, List, Tuple

import pandas as pd

import gumore.core.entities as ent
from gumore.util.log import *

pd.options.display.float_format = "{:.2f}".format

initialize_logger()
logger = logging.getLogger(__name__)


class Hotspot:
    def __init__(
        self, zone_id: int, factor: float, nsts_prod: List[int], nsts_att: List[int]
    ):
        self.zone_id = zone_id
        self.factor = factor
        self.nsts_prod = nsts_prod
        self.nsts_att = nsts_att

    def __repr__(self):
        return (
            f"Hotspot({self.zone_id}, {self.factor}, {self.nsts_prod}, {self.nsts_att})"
        )


class HotspotSet:
    def __init__(
        self, df_prod: pd.DataFrame, df_att: pd.DataFrame, size_to_factor, graph
    ):
        self.hotspots = defaultdict(list)
        self.df_prod = df_prod
        self.df_prod_result = df_prod.copy()
        self.df_att = df_att
        self.df_att_result = df_att.copy()
        self.size_to_factor = size_to_factor
        self.graph = graph
        self.zones = graph.zones
        self._register_all()

    def _register_one(self, hotspot: Hotspot):
        self.hotspots[hotspot.zone_id].append(hotspot)

    def _register_all(self):
        for index, row in self.zones.df[self.zones.df.hotspots.notnull()].iterrows():
            hotspots = json.loads(row.hotspots)
            for hotspot in hotspots:
                self._register_one(
                    Hotspot(
                        index, self.size_to_factor[hotspot[0]], hotspot[1], hotspot[2]
                    )
                )

    def _apply_hotspots_change(self, zone_id, factor, nsts, prod_or_att: str):
        assert prod_or_att in ("prod", "att")
        df = self.__dict__[f"df_{prod_or_att}"]
        df_res = self.__dict__[f"df_{prod_or_att}_result"]
        nsts_in_df = df.commodity.unique()
        for nst in nsts:
            nst_name = ent.commodities_dict[str(nst)][0]
            if nst_name not in nsts_in_df:
                continue
            median_qty = df.loc[
                (df.commodity == nst_name) & (df[prod_or_att] > 0), prod_or_att
            ].median()
            qty = df.loc[
                (df.index == zone_id) & (df.commodity == nst_name), prod_or_att
            ].values[0]
            qty = qty if qty > 0 else median_qty
            qty_change = qty * (factor - 1)
            df_res.loc[
                (df_res.index == zone_id) & (df_res.commodity == nst_name), prod_or_att
            ] += qty_change
            n_other_zones = len(
                df_res.loc[
                    (df_res.index != zone_id) & (df_res.commodity == nst_name), :
                ]
            )
            df_res.loc[
                (df_res.index != zone_id) & (df_res.commodity == nst_name), prod_or_att
            ] -= (qty_change / n_other_zones)
            df_res.loc[
                (df_res.index != zone_id)
                & (df_res.commodity == nst_name)
                & (df_res[prod_or_att] < 0),
                prod_or_att,
            ] = 0

    @staticmethod
    def _calc_factor_per_nst(factor: float, nsts: List[int]) -> float:
        diff_per_nst = (factor - 1.0) / len(nsts)
        return 1.0 + diff_per_nst

    def execute(self):
        for zone_id, hotspots in self.hotspots.items():
            for hotspot in hotspots:
                self._apply_hotspots_change(
                    zone_id, hotspot.factor, hotspot.nsts_prod, "prod"
                )
                self._apply_hotspots_change(
                    zone_id, hotspot.factor, hotspot.nsts_att, "att"
                )

        return self.df_prod_result, self.df_att_result
