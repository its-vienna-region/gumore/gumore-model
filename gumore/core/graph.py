"""
Classes to represent network objects in memory and in the database.
"""

import json
import os
import pickle

# import geopandas as gpd
import pandas as pd

# import append_sys_path
from gumore.util.conf import (
    get_absolute_path,
    get_activate_db_conf_name,
    get_config,
    get_distmat_table_name,
    get_graph_table_name,
    get_zones_table_name,
)

from gumore.util.database import get_db_connection, create_cursor, get_engine
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


class Zones:
    @log_tracebacks()
    def __init__(self):
        conf = get_config()
        self.zones_table_name = get_zones_table_name()
        self.df = None
        self.zone_ids = []
        self.id_column_name = get_config().get("graph", "zones_id_column_name")

    def __repr__(self):
        return "Zones ( no of zones: {}, from id {} to id {}, zones_table_name: {} )".format(
            len(self.df),
            self.df.index.values[0],
            self.df.index.values[-1],
            self.zones_table_name,
        )

    def get_zone(self, id):
        return self.df.loc[id]

    def get_atts_for_zone(self, id):
        return self.df.loc[id]

    def as_named_tuples(self):
        return self.df.itertuples()

    # FIXME:
    @property
    def is_gpd(self):
        # if type(self.df) == gpd.geodataframe.GeoDataFrame:
        #     return True
        # else:
        return False

    @log_tracebacks()
    def read_from_db(
        self, read_geometry=False, read_sample_table=False, sample_table_fraction=None
    ):
        id_column_name = get_config().get("graph", "zones_id_column_name")
        if read_sample_table:
            assert sample_table_fraction
            modulo_str = "WHERE mod({}, {}) = 0".format(
                id_column_name, sample_table_fraction
            )
        else:
            modulo_str = ""
        columns_to_read = (
            id_column_name + ", " + get_config().get("graph", "zones_columns_to_read")
        )
        # columns_to_read = "id, levl_code, cntr_code, nuts_name, fid, attributes"

        con = get_db_connection()
        # TODO: set and reset autocommit: https://www.psycopg.org/docs/connection.html#connection.autocommit

        # FIXME:
        if read_geometry or not read_geometry:
            #     columns_to_read += ", geom"
            #     self.df = gpd.read_postgis(
            #         "SELECT {} FROM {} {} ORDER BY {}".format(
            #             columns_to_read, self.zones_table_name, modulo_str, id_column_name
            #         ),
            #         con,
            #     )
            # else:
            cursor = create_cursor(con)
            cursor.execute(
                "SELECT {} FROM {} {} ORDER BY {}".format(
                    columns_to_read, self.zones_table_name, modulo_str, id_column_name
                )
            )
            rows = cursor.fetchall()
            self.df = pd.DataFrame(rows, columns=columns_to_read.split(", "))
        self.df.set_index(id_column_name, inplace=True)
        self.zone_ids = list(self.df.index)

        if read_sample_table:
            logging.debug(
                "Sample Zones table read with {} zones".format(len(self.zone_ids))
            )


class Distmat:
    """
    This class handles distance matrices.

    Reads graph_table_name from config file and creates an empty Distmat object at initialization.

    Attributes:
        df (:obj:`pandas.DataFrame`): holds the actual data matrix. Access a single element
            with get_dist(from_zone_id, to_zone_id).
        metadata (dict): holds the matrix metadata.
    """

    @log_tracebacks()
    def __init__(self):
        conf = get_config()
        self.graph_table_name = conf.get(
            get_activate_db_conf_name(), "graph_table_name"
        )
        self.reset()
        logger.debug("Distmat object created: {}".format(self))

    def __repr__(self):
        return "Distmat()".format(self.df.shape)

    def __str__(self):
        return (
            "Distmat(shape: {}, distmat_table_name: {}, resistance_type: {}, "
            "means_of_transport: {}, connector_table_name: {}, directed: {}".format(
                self.df.shape,
                self.metadata["distmat_table_name"],
                self.metadata["resistance_type"],
                self.metadata["means_of_transport"],
                self.metadata["connector_table_name"],
                self.metadata["directed"],
            )
        )

    def __call__(self, from_zone_id, to_zone_id):
        return self.get_dist(from_zone_id, to_zone_id)

    @property
    def holds_data(self):
        """
        bool: Returns True if Distmat object holds data or metadata.
        """
        holds_data = all(
            x is not None for x in self.metadata.values()
        ) or self.df.shape != (0, 0)
        return bool(holds_data)

    @property
    def holds_na(self):
        holds_na = self.df.isna().any().any()
        return holds_na

    @property
    def na_relations(self):
        na_relations = {}
        for label, row in self.df.iterrows():
            zones = []
            for zone in row[row.isna()].index:
                zones.append(zone)
            na_relations[label] = zones
        return na_relations

    @log_tracebacks()
    def reset(self):
        """
        Deletes data and metadata of Distmat object.
        """
        self.df = pd.DataFrame()
        self.metadata = {
            "distmat_table_name": None,
            "resistance_type": None,
            "means_of_transport": None,
            "connector_table_name": None,
            "directed": None,
        }

    @log_tracebacks()
    def get_dist(self, from_zone_id, to_zone_id):
        return self.df.loc[from_zone_id, to_zone_id]

    @log_tracebacks()
    def create_in_db(
        self,
        new_distmat_table_name: str,
        resistance_type: str,
        means_of_transport: str,
        connector_table_name: str,
        directed: bool,
        delete_existing_table=False,
        create_sample_table=False,
        zones_where_clause=None,
    ):
        """
        Calculates a new distance matrix with pg_routing (pgr_dijkstraCost) and stores
        it in the database.

        The name of the graph table needs to be defined in the database config.
        The table needs to have the following fields: id, source, target, cost,
        reverse_cost, km.

        The created distance matrix table has the following fields:
        vb_from_id, vb_to_id, start_vid, end_vid, dijk_cost

        Args:
            new_distmat_table_name (str): Name of database table to create.
            resistance_type ({"travel_time", "distance", "beeline"}): Type of
                resistance to calculate costs.
            means_of_transport ({"hgv", "train"}): Means of transport for which costs
                are calculated.
            connector_table_name: (str): Name of database table containing the mapping
                zone -> connector
            directed (bool): True if graph is directed.
            delete_existing_table (bool, default *False*): If True, delete distance
                matrix table in database, if it already exists. Cannot be used, if
                zones_where_clause is set.
            create_sample_table (bool, default *False*): If True, create distance
                matrix only for a sample of all possible relations.
            zones_where_clause (str, default *None*): SQL clause to select zones for
                which the distances shall be calculated, e.g. "WHERE vb.gid IN (SELECT
                gid FROM zones WHERE ew > 20000)".
                Cannot be used if create_sample_table is True.
        """
        logger.info(
            "Starting creation of new distmat database table '{}'".format(
                new_distmat_table_name
            )
        )
        if create_sample_table and zones_where_clause:
            raise ValueError(
                f"Only one of 'create_sample_table' or "
                f"'zones_where_clause' can be set, but "
                f"create_sample_table={create_sample_table} and"
                f"zones_where_clause={zones_where_clause}."
            )
        if resistance_type == "travel_time":
            param_cost = "cost"
            param_reverse_cost = "reverse_cost"
        elif resistance_type == "distance":
            param_cost = "km"
            param_reverse_cost = "km"
        elif resistance_type == "beeline":
            pass
        else:
            raise ValueError
        directed = str(directed)
        if create_sample_table:
            # TODO: name of primary key column as parameter
            modulo_str = "WHERE mod(vb.gid, 200) = 0"
        else:
            modulo_str = ""
        if zones_where_clause:
            where_str = zones_where_clause
        else:
            where_str = ""

        con = get_db_connection()
        cursor = create_cursor(con)

        cursor.execute(
            "SELECT to_regclass('public.{}');".format(new_distmat_table_name)
        )
        distmat_table_exists = bool(cursor.fetchone()[0])
        if distmat_table_exists:
            if delete_existing_table:
                cursor.execute("DROP TABLE {}".format(new_distmat_table_name))
                basepath = get_config().get("paths", "basepath")
                pickles_path = os.path.join(basepath, "pickles")
                pickle_path = os.path.join(pickles_path, new_distmat_table_name)
                if not os.path.exists(pickles_path):
                    os.mkdir(pickles_path)
                if os.path.exists(pickle_path):
                    os.remove(pickle_path)
            else:
                raise ValueError(
                    f"Distmat table '{new_distmat_table_name}' already exists and "
                    f"delete_existing_table is set to False. "
                    f"Set it (or parameter --delete-existing-table if you are running "
                    f"this from the gm-zonesprep script) to True if you want to "
                    f"overwrite the existing table."
                )

        # Calculate the costs for all external relations
        if resistance_type == "beeline":
            raise NotImplementedError
        # TODO: name of primary key column as parameter
        else:
            sql = """
                CREATE TABLE {new_distmat_table_name} AS
                SELECT
                    vb_from.gid vb_from_id,
                    vb_to.gid vb_to_id,
                    dijk.start_vid,
                    dijk.end_vid,
                    dijk.agg_cost dijk_cost
                FROM
                    pgr_dijkstraCost('
                                SELECT 
                                    id, source, target, {param_cost} as cost, 
                                    {param_reverse_cost} as reverse_cost
                                FROM 
                                    {graph_table_name} 
                                WHERE 
                                    {means_of_transport} = True',
                                array(
                                    select vb.ways_source
                                    from {connector_table_name} vb
                                    {where_str}
                                    {modulo_str}
                                    ),
                                array(
                                    select vb.ways_target
                                    from {connector_table_name} vb
                                    {where_str}
                                    {modulo_str}
                                    ),
                                directed := {directed}
                            ) dijk
                    INNER JOIN 
                        {connector_table_name} vb_from ON (dijk.start_vid = 
                        vb_from.ways_source)
                    INNER JOIN	
                        {connector_table_name} vb_to ON (dijk.end_vid = 
                        vb_to.ways_target)
                ORDER BY
                    vb_from_id,
                    vb_to_id;""".format(
                new_distmat_table_name=new_distmat_table_name,
                param_cost=param_cost,
                param_reverse_cost=param_reverse_cost,
                graph_table_name=self.graph_table_name,
                means_of_transport=means_of_transport,
                where_str=where_str,
                modulo_str=modulo_str,
                connector_table_name=connector_table_name,
                directed=directed,
            )
        cursor.execute(sql)

        # Change costs to value 0 for all internal flows
        sql = f"""
            UPDATE {new_distmat_table_name}
            SET dijk_cost = 0
            WHERE vb_from_id = vb_to_id"""
        cursor.execute(sql)

        # Add primary key
        cursor.execute(
            """ALTER TABLE {} ADD PRIMARY KEY (vb_from_id, vb_to_id);""".format(
                new_distmat_table_name
            )
        )

        # Add table metadata
        metadata = {
            "distmat_table_name": new_distmat_table_name,
            "resistance_type": resistance_type,
            "means_of_transport": means_of_transport,
            "connector_table_name": connector_table_name,
            "directed": directed,
        }
        cursor.execute(
            """
            COMMENT ON TABLE {} IS '{}';""".format(
                new_distmat_table_name, json.dumps(metadata)
            )
        )

        con.commit()

        # Check for missing relations and repair, if necessary.
        # This problem can appear because of directly adjacent connector ways for
        # two zones: If ways_source == ways_target, pg_routing does not return
        # value 0, it just skips the row.
        self.reset()
        self.read_from_db(
            distmat_table_name=new_distmat_table_name, recreate_pickle=True
        )
        if self.holds_na:
            logger.warning("Distmat contains NA-values!")
            na_relations = []
            logger.warning("Relations with NA-values")
            logger.warning("========================")
            for key, value in self.na_relations.items():
                if len(value) > 0:
                    logger.warning(f"{key}:  {value}")
                    na_relations += [(key, x) for x in value]
            logger.warning(f"NA-relations: {na_relations}")
            logger.warning(
                "Repairing NA-relations by inserting new relations with "
                "dijk_cost = 0."
            )
            for relation in na_relations:
                sql = f"""
                    INSERT INTO {new_distmat_table_name} 
                        (vb_from_id, vb_to_id, start_vid, end_vid, dijk_cost)
                    (
                        SELECT 
                        {relation[0]}, 
                        {relation[1]}, 
                        (
                            SELECT start_vid 
                            FROM {new_distmat_table_name} 
                            WHERE vb_from_id = {relation[0]}
                            LIMIT 1
                        ),
                        (
                            SELECT end_vid 
                            FROM {new_distmat_table_name} 
                            WHERE vb_to_id = {relation[1]}
                            LIMIT 1
                        ),
                        0
                    )
                """
                cursor.execute(sql)
            con.commit()
            logger.warning(f"Finished inserting {len(na_relations)} new relations.")
            logger.info("NA-relations repaired.")

        logger.info(
            "Finished creation of new distmat database table '{}'".format(
                new_distmat_table_name
            )
        )

    @log_tracebacks()
    def read_from_db(self, distmat_table_name: str, recreate_pickle: bool = False):
        """
        Reads a distance matrix (including metadata) from database.

        Args:
            distmat_table_name (str): Table name of distance matrix to read.
            recreate_pickle (bool, default *False*): Force recreate pickle, regardless
                of config option value.
        """
        logger.debug(
            "Reading distmat table from database '{}'".format(distmat_table_name)
        )

        if self.holds_data:
            raise RuntimeError(
                "Distmat object already holds data. Use reset() to delete it first."
            )

        basepath = get_config().get("paths", "basepath")

        pickle_path = os.path.join(basepath, "pickles", distmat_table_name)

        read_distmats_from_pickle = get_config().getboolean(
            "graph", "read_distmats_from_pickles"
        )

        if recreate_pickle:
            read_distmats_from_pickle = False

        if read_distmats_from_pickle:
            try:
                with open(pickle_path, "rb") as f:
                    self.__dict__ = pickle.load(f)

                logger.debug(
                    "Finished reading distmat table from pickle ({}): {}".format(
                        pickle_path, self
                    )
                )
            except FileNotFoundError:
                logger.warning(
                    "Could not read distmat table from pickle ({}): {}.\n"
                    "Trying to read from database.".format(pickle_path, self)
                )
                read_distmats_from_pickle = False

        if not read_distmats_from_pickle:
            con = get_db_connection()
            cursor = create_cursor(con)

            cursor.execute(
                "SELECT pg_catalog.obj_description('{}'::regclass, 'pg_class');".format(
                    distmat_table_name
                )
            )
            table_description = cursor.fetchone()[0]
            if table_description:
                self.metadata = json.loads(table_description)

            cursor.execute(
                """
                SELECT 
                    vb_from_id,
                    vb_to_id,
                    dijk_cost
                FROM 
                    {}
                ORDER BY
                    vb_from_id, 
                    vb_to_id""".format(
                    distmat_table_name
                )
            )

            rows = cursor.fetchall()
            df = pd.DataFrame(rows)
            self.df_long = df.copy()
            self.df_long.columns = ["vb_from_id", "vb_to_id", "dijk_cost"]
            self.df = df.pivot(index=0, columns=1, values=2)
            if not os.path.exists(os.path.dirname(pickle_path)):
                os.mkdir(os.path.dirname(pickle_path))
            if not os.path.exists(pickle_path):
                with open(pickle_path, "wb") as f:
                    pickle.dump(self.__dict__, f)
                logger.info(
                    "Distmat not yet pickled. Created pickle {}".format(pickle_path)
                )
            else:
                with open(pickle_path, "wb") as f:
                    pickle.dump(self.__dict__, f)
                logger.info(
                    f"Distmat was already pickled, but replaced with pickle "
                    f"{pickle_path}"
                )
            logger.debug(
                "Finished reading distmat table from database: {}".format(self)
            )

        if get_config().getboolean("debugging", "fill_na_in_distmats"):
            self.df = self.df.fillna(
                get_config().getfloat("debugging", "na_in_distmats_fill_value")
            )


class VirtualLinks:
    """
    This class provides methods for virtual link elements.

    Relevant configuration sections::

        [graph]
        virtual_links_file=<PATH_TO_CSV_FILE>

        [database.<DBNAME>]
        graph_table_name=<TABLE_NAME>
    """

    def __init__(self):
        conf = get_config()
        self.graph_table_name = conf.get(
            get_activate_db_conf_name(), "graph_table_name"
        )
        self.virtual_links_path = get_absolute_path(conf, "graph", "virtual_links_file")
        logger.debug("Links object created: {}".format(self))

    @staticmethod
    def _create_virtual_link_table_in_database():
        """
        Re-creates table 'virtual_links' in database.

        The existing table is dropped before creation.
        """
        conf = get_config()
        graph_table_name = conf.get(get_activate_db_conf_name(), "graph_table_name")
        conn = get_db_connection()
        c = create_cursor(conn)
        c.execute("""DROP TABLE IF EXISTS virtual_links""")
        c.execute(
            """CREATE TABLE virtual_links (
                            id serial,
                            osm_source_id bigint,
                            osm_target_id bigint,
                            source integer,
                            target integer,
                            km float,
                            kmh integer,
                            mode_switch_hours float,
                            cost float,
                            reverse_cost float,
                            x1 float,
                            y1 float,
                            x2 float,
                            y2 float,
                            hgv boolean,
                            rail boolean,
                            virtual boolean,
                            geom_way geometry(Linestring, 4326))"""
        )
        c.execute(
            """SELECT SETVAL('virtual_links_id_seq', (SELECT MAX(id) + 1 FROM {graph_table_name}))
                  """.format(
                graph_table_name=graph_table_name
            )
        )
        conn.commit()
        logger.debug("Table 'virtual_links' re-created.")

    @staticmethod
    def _add_column_virtual_to_graph_table():
        """
        Adds a column 'virtual' (boolean) to the active graph table, if not yet existing.
        """
        conf = get_config()
        graph_table_name = conf.get(get_activate_db_conf_name(), "graph_table_name")
        conn = get_db_connection()
        c = create_cursor(conn)
        sql = """
                        alter table {graph_table_name}
                        add column if not exists virtual boolean
                    """.format(
            graph_table_name=graph_table_name
        )
        c.execute(sql)
        conn.commit()

    @staticmethod
    def delete_virtual_links_from_graph_table():
        """
        Deletes all virtual links from active graph table.

        The table 'virtual_links' is not touched.
        """
        conf = get_config()
        graph_table_name = conf.get(get_activate_db_conf_name(), "graph_table_name")
        conn = get_db_connection()
        c = create_cursor(conn)
        sql = f"""
                    delete
                    from 
                        {graph_table_name}
                    where
                        virtual = true
                    """
        c.execute(sql)
        conn.commit()
        deleted_rows_n = c.rowcount
        logging.info(f"{deleted_rows_n} rows deleted from {graph_table_name}.")
        logging.info(
            "Table 'virtual_links' may still hold imported virtual links for reference, but "
            "computation of distance matrices etc. won't be affected by that."
        )

    def read_virtual_links_from_csv(self):
        """
        Reads csv file containing info for creation of virtual links.

        .. note::
            The csv file must contain the following columns:
                * description
                * osm_source_id
                * osm_target_id
                * hgv
                * rail
                * kmh
                * mode_switch_hours

        Returns:
            map: Iterator over csv rows as namedtuples
        """
        REQUIRED_COLUMNS = {
            "description",
            "osm_source_id",
            "osm_target_id",
            "hgv",
            "rail",
            "kmh",
            "mode_switch_hours",
        }

        df = pd.read_csv(self.virtual_links_path, sep=";")
        csv_columns = set(df.columns)
        if REQUIRED_COLUMNS != csv_columns:
            raise ValueError(
                f"The csv file {self.virtual_links_path} does not match the required columns "
                f"({REQUIRED_COLUMNS})"
            )
        df.drop("description", axis=1, inplace=True)
        return df.itertuples(index=False, name="Link")

    def create_virtual_links_in_database(self, links):
        """
        Adds virtual links from csv to table 'virtual_links'.

        Links are created with Linestring geometry, whereas source and target points are connected with a straight line.

        The following attributes are automatically added:
            * `x1, y1, x2, y2`: Calculated using PostGIS functionality
            * `virtual`: set to 'true'
            * `km`: Calculated using PostGIS functionality
            * `cost, reverse_cost`: km / 1000 / kmh + mode_switch_hours
            * `source, target`: Retrieved from active graph table by matching
              osm_source_id and osm_target_id, respectively.

        Args:
            links (Iterator over namedtuples): Data for creation of virtual links
        """
        self._create_virtual_link_table_in_database()
        conf = get_config()
        graph_table_name = conf.get(get_activate_db_conf_name(), "graph_table_name")
        conn = get_db_connection()
        c = create_cursor(conn)

        links_n = 0
        for link in links:
            logging.info(f"Inserting virtual link {link}.")
            sql = """
                insert into virtual_links ({column_names}, x1, y1, x2, y2, virtual, geom_way)
                    select
                        {values}, 
                        ST_X(source.geom),
                        ST_Y(source.geom),
                        ST_X(target.geom),
                        ST_Y(target.geom),
                        True,
                        ST_MakeLine(source.geom, target.geom)::geometry(Linestring, 4326) geom_way
                    from
                    (
                        select 
                            ST_StartPoint(geom_way) geom
                        from 
                            {graph_table_name}
                        where 
                            osm_source_id = {osm_source_id}
                        limit(1)
                    ) source,
                    (
                        select 
                            ST_EndPoint(geom_way) geom
                        from 
                            {graph_table_name}
                        where 
                            osm_target_id = {osm_target_id}
                        limit(1)
                    ) target
                returning id
                """.format(
                column_names=", ".join(link._fields),
                graph_table_name=graph_table_name,
                values=", ".join(str(x) for x in list(link)),
                osm_source_id=link.osm_source_id,
                osm_target_id=link.osm_target_id,
            )
            c.execute(sql)
            result = c.fetchone()
            if not result:
                raise ValueError(
                    f"Could not connect virtual link {link} to existing links in graph table.\n"
                    f"Check 'osm_source_id' and 'osm_target_id' in your CSV file!"
                    f"SQL statement:\n {sql}"
                )
            else:
                id = result[0]

            sql = """
                with link as (
                    select 
                        ST_Length(ST_Transform(geom_way, 3035)) / 1000 as km,
                        ST_Length(ST_Transform(geom_way, 3035)) / 1000 / kmh + mode_switch_hours as cost
                    from
                        virtual_links         
                    where 
                        id = {id}
                )
                update virtual_links
                set
                    km=link.km,
                    cost=link.cost,
                    reverse_cost=link.cost
                from link
                where id = {id}  
                """.format(
                id=id
            )
            c.execute(sql)

            sql = """
                with link as (
                    select
                        {graph_table_name}.source
                    from
                        {graph_table_name},
                        virtual_links
                    where
                        virtual_links.id = {id}
                        and virtual_links.osm_source_id = {graph_table_name}.osm_source_id
                    limit(1)
                )
                update virtual_links
                set
                    source=link.source
                from link
                where id = {id}          
                """.format(
                id=id, graph_table_name=graph_table_name
            )
            c.execute(sql)

            sql = """
                with link as (
                    select
                        {graph_table_name}.target
                    from
                        {graph_table_name},
                        virtual_links
                    where
                        virtual_links.id = {id}
                        and virtual_links.osm_target_id = {graph_table_name}.osm_target_id
                    limit(1)
                )
                update virtual_links
                set
                    target=link.target
                from link
                where id = {id}
                """.format(
                id=id, graph_table_name=graph_table_name
            )
            c.execute(sql)
            links_n += 1

        conn.commit()
        logger.debug(f"{links_n} virtual links created in table 'virtual_links'")

    def append_virtual_links_to_graph_table(self):
        """
        Appends virtual links from table 'virtual_links' to active graph table.
        """
        self._add_column_virtual_to_graph_table()

        conf = get_config()
        graph_table_name = conf.get(get_activate_db_conf_name(), "graph_table_name")
        conn = get_db_connection()
        c = create_cursor(conn)

        sql = """
            insert into {graph_table_name} ( 
                id,
                osm_source_id,
                osm_target_id,
                source,
                target,
                km,
                kmh,
                cost,
                reverse_cost,
                x1,
                y1,
                x2,
                y2,
                hgv,
                rail,
                virtual,
                geom_way
            )
            select
                id,
                osm_source_id,
                osm_target_id,
                source,
                target,
                km,
                kmh,
                cost,
                reverse_cost,
                x1,
                y1,
                x2,
                y2,
                hgv,
                rail,
                virtual,
                geom_way
            from
                virtual_links
        """.format(
            graph_table_name=graph_table_name
        )
        c.execute(sql)

        conn.commit()

        sql = f"""
            select 
                count(*)
            from 
                {graph_table_name}
            where
                virtual = true
            """
        c.execute(sql)
        virtual_links_n = c.fetchone()[0]
        logger.info(
            f"Virtual links added to graph table {graph_table_name}. "
            f"Table now holds {virtual_links_n} virtual links."
        )

        virtual_links_in_csv_n = len(list(self.read_virtual_links_from_csv()))
        if virtual_links_n != virtual_links_in_csv_n:
            logger.warning(
                f"Number of virtual links in csv ({virtual_links_in_csv_n}) does not match "
                f"number of virtual links in graph table ({virtual_links_n})!"
            )


class Graph:
    """ the Graph that can calculate shortest paths
        has nodes, cells and links
    """

    @log_tracebacks()
    def __init__(self):
        """ set up the Graph"""
        # Zones
        self.zones = Zones()
        read_sample_table = get_config().getboolean(
            "debugging", "read_zones_sample_table"
        )
        sample_table_fraction = get_config().getint(
            "debugging", "zones_sample_table_fraction"
        )
        self.zones.read_from_db(
            read_sample_table=read_sample_table,
            sample_table_fraction=sample_table_fraction,
        )

        # Distmats
        # TODO: load distmats for hours and distance
        self.distmats = dict(
            (
                ("hgv_hours", Distmat()),
                ("hgv_km", Distmat()),
                ("train_hours", Distmat()),
                ("train_km", Distmat()),
                ("beeline_km", Distmat()),
            )
        )

        distmat_types = ("hgv_hours", "hgv_km", "train_hours", "train_km", "beeline_km")
        distmat_table_names = (
            get_distmat_table_name("distmat_hgv_hours"),
            get_distmat_table_name("distmat_hgv_km"),
            get_distmat_table_name("distmat_train_hours"),
            get_distmat_table_name("distmat_train_km"),
            get_distmat_table_name("distmat_beeline_km"),
        )

        self.distmats = {x: Distmat() for x in distmat_types}
        logger.debug(f"Active distance matrices: {distmat_table_names}")
        for distmat_type, distmat_table_name in zip(distmat_types, distmat_table_names):
            if len(distmat_table_name) > 0:
                self.distmats[distmat_type].read_from_db(distmat_table_name)
                if self.distmats[distmat_type].holds_na:
                    logger.warning(
                        f"Distance matrix '{distmat_type}' from table '{distmat_table_name}' "
                        f"holds NA-values. Results may be incorrect!"
                    )
        # TODO: check if all distmats have the same structure and indices

        # Nodes
        self.nodes = {}

        # Links
        self.links = []

        # Cells
        self.cells = {}


if __name__ == "__main__":
    # run a test
    # g=Graph()
    # g.read("../sqlite/qgis1.sql")
    # g.calc_costs()
    # potmat=g.potential('ew','besch',g.distmat(),mobrate=1.5)
    # g.list_mat(potmat)

    # === Graph Synopsis
    # g = Graph()
    # print(g)
    # print(g.zones)
    # print(g.zones.get_atts_for_zone(200))
    # print(g.zones.zone_ids)
    # print(g.distmats)
    # print(g.distmats["hgv"].get_dist(200, 400))
    # print(g.distmats["hgv"](200, 400))
    # for zone in g.zones.as_named_tuples():
    #     print(zone)

    # === Zones Synopsis
    # z = Zones()
    # z.read_from_db(read_geometry=False, read_sample_table=True)
    # print(z.df)
    # for index, zone in z.df.iterrows():
    #     print(index, zone.get_values())
    # print(z)

    # === Distmat Synopsis
    dm = Distmat()
    print(dm)
    dm.create_in_db(
        new_distmat_table_name="cost_vb_dense_hgv_hours",
        resistance_type="travel_time",
        means_of_transport="hgv",
        connector_table_name="connect__vb__to__eu_dense_2po_4pgr__hgv",
        directed=True,
        delete_existing_table=True,
        create_sample_table=False,
    )
    print(dm)
    dm.read_from_db(distmat_table_name="cost_vb_dense_hgv_hours")
    print(dm)
    print(dm.holds_na)
    print(dm.df)
    import pprint

    pprint.pprint(dm.na_relations)

    # dm = Distmat()
    # dm.create_in_db(new_distmat_table_name="debug_test",
    #                 resistance_type="travel_time",
    #                 means_of_transport="hgv",
    #                 connector_table_name="connect__vb__to__eu_dense_2po_4pgr__hgv",
    #                 directed=True,
    #                 delete_existing_table=True,
    #                 create_sample_table=True)
    # print(dm)

    # dm = Distmat()
    # print(dm)
    # dm.create_in_db(new_distmat_table_name="cost_vb_dense_hgv_km",
    #                 resistance_type="distance",
    #                 means_of_transport="hgv",
    #                 connector_table_name="connect__vb__to__eu_dense_2po_4pgr__hgv",
    #                 directed=True,
    #                 delete_existing_table=True,
    #                 create_sample_table=False)
    # print(dm)
    # dm.read_from_db(distmat_table_name="cost_vb_dense_hgv_km")
    # print(dm)
    # print(dm.holds_na)
    # print(dm.df)
    # import pprint
    # pprint.pprint(dm.na_relations)

    # dm = Distmat()
    # print(dm)
    # dm.create_in_db(new_distmat_table_name="cost_vb_grob_hgv_hours",
    #                 resistance_type="travel_time",
    #                 means_of_transport="hgv",
    #                 connector_table_name="connect__vb_grob__to__ad_2po_4pgr__hgv",
    #                 directed=False,
    #                 delete_existing_table=True,
    #                 create_sample_table=False)
    # print(dm)
    # dm.read_from_db(distmat_table_name="cost_vb_grob_hgv_hours")
    # print(dm)
    # print(dm.holds_na)
    # print(dm.df)
    # import pprint
    # pprint.pprint(dm.na_relations)

    #
    # dm = Distmat()
    # print(dm)
    # dm.create_in_db(new_distmat_table_name="cost_vb_grob_hgv_km",
    #                 resistance_type="distance",
    #                 means_of_transport="hgv",
    #                 connector_table_name="vb_grob_nearest_way_hgv",
    #                 directed=True,
    #                 delete_existing_table=True,
    #                 create_sample_table=False)
    # print(dm)
    # dm.read_from_db(distmat_table_name="cost_vb_grob_hgv_km")
    # print(dm)
    # print(dm.holds_na)
    # print(dm.df)
    # import pprint
    # pprint.pprint(dm.na_relations)

    # dm = Distmat()
    # dm.create_in_db(new_distmat_table_name="cost_hgv_hours",
    #                 resistance_type="travel_time",
    #                 means_of_transport="hgv",
    #                 connector_table_name="vb_nearest_way_hgv_2",
    #                 directed=True,
    #                 delete_existing_table=True,
    #                 create_sample_table=False)
    # print(dm)
    # print(dm.holds_na)

    # dm = Distmat()
    # dm.create_in_db(new_distmat_table_name="cost_hgv_distance",
    #                 resistance_type="distance",
    #                 means_of_transport="hgv",
    #                 connector_table_name="vb_nearest_way_hgv_2",
    #                 directed=True,
    #                 delete_existing_table=True,
    #                 create_sample_table=False)
    # dm.read_from_db(distmat_table_name="cost_hgv_distance")
    # print(dm.holds_na)

    # dm.read_from_db(distmat_table_name="cost_hgv_test_from_python")
    # print(dm)
    # dm.reset()
    # dm.read_from_db(distmat_table_name="cost_hgv_test_from_python")
    # print(dm)
    # print(dm.df)
    # print(dm.holds_data)
    # print(dm.metadata)

    # === VirtualLinks Synopsis
    # links = VirtualLinks()
    # links.delete_virtual_links_from_graph_table()
    # links_namedtuples = links.read_virtual_links_from_csv()
    # links.create_virtual_links_in_database(links_namedtuples)
    # links.append_virtual_links_to_graph_table()

    # === Timing
    # import timeit
    # print(timeit.timeit('dm.read_from_db(distmat_table_name="cost_hgv_test_from_python")',
    #                     setup="dm = Distmat()",
    #                     globals=globals(),
    #                     number=1))

    pass
