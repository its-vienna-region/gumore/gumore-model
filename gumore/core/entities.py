#!/usr/bin/env python3
"""
Definition of entity objects.

Import with `from gumore.core.entities import *`
"""

from collections.abc import Sequence
import importlib

# import append_sys_path
from gumore.util import global_vars
from gumore.util.conf import get_config
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)

config = get_config()
if config:
    model_config = config["model_config"]

    mode_keys = tuple([x for x in model_config if "mode_obj_" in x])
    modes = tuple([[y.strip() for y in model_config[x].split("||")] for x in mode_keys])
    modes_dict = {x[0]: x[1:] for x in modes}

    means_keys = tuple([x for x in model_config if "means_obj_" in x])
    means = tuple(
        [[y.strip() for y in model_config[x].split("||")] for x in means_keys]
    )
    means_dict = {x[0]: x[1:] for x in means}

    commodity_keys = tuple([x for x in model_config if "commodity_obj_" in x])
    commodities = tuple(
        [[y.strip() for y in model_config[x].split("||")] for x in commodity_keys]
    )
    commodities_dict = {x[0]: x[1:] for x in commodities}


class TransportMode:
    def __init__(self, code, name):
        self.code = code
        self.name = name

    def __eq__(self, other):
        return self.code == other

    def __repr__(self):
        return "TransportMode('{}', '{}')".format(self.code, self.name)


class TransportMeans:
    def __init__(self, code, name, mode_code):
        self.code = code
        self.name = name
        self.mode = TransportMode(mode_code, *modes_dict[mode_code])
        try:
            self.load_factor_by_commodity = global_vars.matrices_model_config[
                "matrix_load_factor"
            ].loc[:, code]
        except KeyError:
            self.load_factor_by_commodity = None
            # logger.warning("TransportMeans: could not load matrix for code '{}'. "
            #                "Is it defined in model config spreadsheet?".format(code))

    def __eq__(self, other):
        return self.code == other

    def __hash__(self):
        return hash(str(self))

    def __repr__(self):
        return "TransportMeans('{}', '{}', '{}')".format(
            self.code, self.name, self.mode.code
        )


class TransportMeansMix:
    def __init__(self, code, means, ratios):
        assert all([isinstance(x, TransportMeans) for x in means])
        assert code in means_dict.keys()
        assert (sum(ratios) == 100) or (sum(ratios) == 1)
        assert set([x.code for x in means]).issubset(set(means_dict.keys()))
        assert len(means) == len(ratios)
        self.code = code
        self.means = list(means)
        self.means_codes = [x.code for x in means]
        self.ratios = list(ratios) if sum(ratios) == 1 else [x / 100.0 for x in ratios]

    def __repr__(self):
        codes = ", ".join(self.means_codes)
        return "TransportMeansMix('{}', [{}], {})".format(self.code, codes, self.ratios)


class Commodity:
    def __init__(self, id, code, density, description_short, description_long):
        assert type(density) == float

        self.id: int = id
        self.code: str = code
        self.density = density  # tons / m³
        self.description_short = description_short
        self.description_long = description_long

    def __eq__(self, other):
        return self.code == other

    def __hash__(self):
        return hash(str(self))

    def __repr__(self):
        return "Commodity('{}', {}, '{}', '{}')".format(
            self.code, self.density, self.description_short, self.description_long
        )

    def __str__(self):
        return self.code


# __all__ = ["TransportMeansMix"]

if config:
    for mode in modes:
        obj_name = "mod_" + mode[0]
        globals()[obj_name] = TransportMode(*mode)
        # if "mode" in config.get("entities", "import").split(","):
        #     __all__.append(obj_name)

    for mea in means:
        obj_name = mea[0]
        globals()[obj_name] = TransportMeans(*mea)
        # if "means" in config.get("entities", "import").split(","):
        #     __all__.append(obj_name)

    for com in commodities:
        obj_name = "com_" + com[1]
        com[2] = float(com[2])
        globals()[obj_name] = Commodity(*com)
        # exec("{} = Commodity(*com)".format(obj_name))
        # if "commodity" in config.get("entities", "import").split(","):
        #     __all__.append(obj_name)
    # com_nst01 = Commodity("nst01", 2.0, "short desc", "long desc")
else:  # this is only needed for building the documentation with sphinx
    com_nst15 = Commodity(15, "nst15", 1.0, "Parcels", "Parcels")


if __name__ == "__main__":
    # print(global_vars.matrices_model_config)
    print(dir())
    # print(__all__)

    print()
    print(mod_str)
    print(mod_str.code, mod_str.name, sep=" | ")
    print('"str" == mod_str: ', "str" == mod_str)

    print()
    print(LX)
    print(LX.code, LX.name, LX.mode, LX.mode.code, LX.mode.name, sep=" | ")
    print('"LX" == LX: ', "LX" == LX)
    print(LX.load_factor_by_commodity["nst01"])
    print(LX.load_factor_by_commodity["nst14"])

    print()
    mix = TransportMeansMix("SMIX", [LS, L75], [20, 80])
    print(mix)
    print(mix.code, mix.means, mix.means_codes, mix.ratios, sep=" | ")

    print()
    co = Commodity("nst01", 2.0, "short desc", "long desc")
    print(co)
