from abc import ABC, abstractmethod
from collections import namedtuple
from typing import List

import pandas as pd

Cell = namedtuple("Cell", "id weight")
ZoneRatio = namedtuple("ZoneRatio", "id ratio")


class FindZone(ABC):
    """ Base class to find cells according to different criteria.
        This is basically a mechanism to find the terminals for reloading commoditys.
        Different criteria for the searched cells can be customized by attributes and functions:
        Distance some cells:
            - maxdist ......... the maximum distance the Cell can be from the start Cell(s)
                                With more than 1 fromcell we use the sum of all distances here
                                None ... means no maximum distance considered (default)
            - metric()......... a function, that calculates the distance metric for the different resulting cells
                                Default is the inverse of the sum of the distances to all fromcells
            - mindist ......... The minimum sum of distances, a Cell can have from fromcells
                                We cannot guaranty a min distance in the distmat, so we pr  event division by zero here
                                default is a very small number
        Attributes of the Cell:
            - select() ........ a function, that selects the cells, that are considered
                                The functiin returns the cells and a weight for each Cell
                                Default: All cells are considered, weight is 1.0 for all cells
        General:
            - count ........... The number of resulting cells. Can be:
                                1 ........... just the Cell with the lowest metric is returned
                                <a number> .. this many "best" cells are returned
                                None ........ all found cells will be returned (default)
            - interior ........ may the Cell be in fromcells (default True)
        Returns:
            a List of 2-tuples consisting of (Cell, ratio) where
            - Cell ..... the name of a found Cell
            - ratio .... the ratio this Cell should be used
            The sum of all ratios is 1.0
            For all returned cells the product of weight*metric*ratio is constant
    """

    def __init__(
        self,
        ruleobj,
        count: int = 1,
        distance_min: float = None,
        distance_max: float = None,
        find_in_from_zone: bool = False,
    ) -> None:
        # assert not (
        #     (count > 1) and find_in_from_zone
        # ), "'find_in_from_zone' can only be set with 'count=1'"
        self.count = count
        self.distance_min = distance_min
        self.distance_max = distance_max
        self.find_in_from_zone = find_in_from_zone
        self.graph = ruleobj.ctxt.graph
        self.zones = ruleobj.ctxt.graph.zones
        self.distmat = ruleobj.distmat

    def __call__(self, from_zone):
        return self.calc_zones_ratios(from_zone)

    @abstractmethod
    def calc_weights(self) -> pd.Series:
        pass

    @staticmethod
    def calc_attraction(distance: pd.Series) -> pd.Series:
        return (1 / distance) / (1 / distance).sum()

    def calc_zones_ratios(self, from_zone: int) -> List[ZoneRatio]:
        weights = self.calc_weights()
        weights = weights[weights > 0]

        distance = self.distmat.df.loc[from_zone, weights.index]
        distance[distance == 0] = self.mindist
        if self.distance_min:
            distance = distance[distance >= self.distance_min]
            weights = weights[distance.index]
        if self.distance_max:
            distance = distance[distance <= self.distance_max]
            weights = weights[distance.index]
        distance = distance * weights
        distance = distance.sort_values(ascending=True)
        distance = distance[0 : self.count]

        attraction = self.calc_attraction(distance)
        attraction = attraction.sort_values(ascending=False)

        if self.find_in_from_zone:
            # ratio can be set to 1.0 in any case, because self.count has to be 1
            zones_ratios = [
                ZoneRatio(id=x[0], ratio=1.0) for x in attraction.iteritems()
            ]
        else:
            # otherwise use calculated ratios
            zones_ratios = [
                ZoneRatio(id=x[0], ratio=x[1]) for x in attraction.iteritems()
            ]
        return zones_ratios

    # def zone_has_attribute(self, attribute):
    #     return self.zone.get(attribute)
    #
    # def zone_attribute_is_true(self, attribute):
    #     if self.zone_has_attribute(attribute) is None:
    #         raise ValueError(
    #             f"No attribute '{attribute}' found while trying to check its truth "
    #             f"value. "
    #             f"Check zone table '{self.graph.zones.zones_table_name}' and config "
    #             f"option 'graph/zones_columns_to_read'! "
    #         )
    #     att_value = self.zone.get(attribute)
    #     if type(att_value) in (bool, np.bool_):
    #         return att_value
    #     else:
    #         raise TypeError("Attribute value is not of type bool")
    #
    # def distance_to(self, zone_id):
    #     try:
    #         distance = self.distmat(self.calculate(zone_id).id, zone_id)
    #         return distance
    #     except IndexError as e:
    #         raise ValueError(
    #             f"Could not find connection to zone id {zone_id} in distance matrix "
    #             f"{self.distmat.metadata['distmat_table_name']}."
    #         )
