import inspect
import time
from typing import Any, Callable, Tuple

import numpy as np
import pandas as pd

import gumore.calibrate.zones
import gumore.core.assignment
import gumore.core.entities
import gumore.core.flow
from gumore.core.rules import GumoreGen, GumoreRule
import gumore.core.rules.means
import gumore.core.rules.gen
import gumore.core.rules.parcelgen
from gumore.core.graph import Graph
import gumore.core.rules.vehicle
from gumore.util.conf import get_config, get_zones_table_name
from gumore.util.counts import load_count_vs_assignment_to_database
from gumore.util.database import create_cursor, get_db_connection, get_engine, TABLES
from gumore.util.filedata import load_regional_sums_for_structural_data
from gumore.util.log import *

pd.options.display.float_format = "{:.2f}".format

initialize_logger()
logger = logging.getLogger(__name__)


class GumoreRunner:
    """ the main loop for the gumore simulation.
        It also keeps the application context.
    """

    def __init__(self):
        """ initialise the various things:
        - read the Graph
        - set up the ruleset
        - call the function to set up the input test datat
        """
        self.structural_data_correction_factors = {}
        self.flow_corrections: Tuple[
            Tuple[Callable[[pd.DataFrame], pd.DataFrame], float], ...
        ] = ()
        self.graph = Graph()
        self.regional_sums_for_structural_data = (
            load_regional_sums_for_structural_data()
        )
        self.distmats = self.graph.distmats
        self.generators = gumore.core.rules.gen.GumoreGenSet(
            self.graph, self.regional_sums_for_structural_data
        )
        self.parcel_generators = gumore.core.rules.parcelgen.ParcelGenSet(self.graph)
        self.rules = gumore.core.rules.means.GumoreRuleSet(self)
        self.parcel_rules = gumore.core.rules.means.GumoreRuleSet(self)
        self.vessels = gumore.core.rules.vehicle.VehicleSet(self)

        self.rule_results_df = pd.DataFrame()

        # config options
        self.concurrency = get_config().get("debugging", "concurrency")
        self.is_perform_parcel_simulation = get_config().getboolean(
            "model_run", "perform_parcel_simulation"
        )
        self.is_perform_result_sanity_check = get_config().getboolean(
            "model_run", "perform_result_sanity_check"
        )
        self.is_perform_trips_assignment = get_config().getboolean(
            "model_run", "perform_trips_assignment"
        )
        self.is_perform_meansflows_assignment = get_config().getboolean(
            "model_run", "perform_meansflows_assignment"
        )
        self.is_perform_count_value_comparison = get_config().getboolean(
            "model_run", "perform_count_value_comparison"
        )

    def run(self):
        """This is the main method to run all simulation steps.

        The following steps are executed:
            * Correction of structural data errors (e.g. for headquarters)
            * Traffic generation, including:
                * Calculation of production anc attraction quantities
                * Modification of production and attraction quantities for 'hot spots'
                * Calibration of production and attraction quantities
                * Calculation of flows with gravity model
                * Balancing of flows (Furness algorithm)
            * Application of rules: flow splitting, reloading and means assignment
            * Parcel sub-model
            * Sanity check of rule application results
            * Trips generation based on vessel types
            * Trips assignment to graph
            * Comparison with count values

        The execution of many of these steps can be turned on or off by setting the
        appropriate configuration options (in the main configuration or for individual rules).
        """
        start_time = time.time()

        self._run_structural_data_correction()
        gen_results_df = self.generators.execute()

        gen_results_df = self._run_flow_correction(gen_results_df)
        gen_results_df.to_sql(
            TABLES["rawflows"] + "_corrected", get_engine(), if_exists="replace"
        )
        # TODO: write to db test
        # TODO: rename method and attribute

        rule_results = self.rules.execute(
            gen_results_df, mode="normal", concurrency=self.concurrency
        )

        if self.is_perform_parcel_simulation:
            parcel_gen_results_df = self.parcel_generators.execute(
                concurrency=self.concurrency
            )
            rule_results_parcels = self.parcel_rules.execute(
                parcel_gen_results_df, mode="parcels", concurrency=self.concurrency
            )
        else:
            rule_results_parcels = []
        rule_results_all = rule_results + rule_results_parcels
        self.rule_results_df = self._transform_rule_results_to_df(rule_results_all)

        if self.is_perform_result_sanity_check:
            gen_results_sum_df = gen_results_df.groupby("commodity").sum().qty
            self._perform_results_sanity_check(gen_results_sum_df, self.rule_results_df)

        _ = self.vessels.execute(concurrency=self.concurrency)
        self._check_if_everything_was_transported(self.rule_results_df)

        if self.is_perform_trips_assignment:
            gumore.core.assignment.assign_trips()

        if self.is_perform_count_value_comparison:
            _ = load_count_vs_assignment_to_database()

        if self.is_perform_meansflows_assignment:
            gumore.core.assignment.assign_meansflows()

        running_time = time.time() - start_time

        logger.info(f"🎂 🎇 🏁 All done after {running_time / 60:.1f} minutes! 🏁 🎇 🎂")

    def register(self, gen_or_rule_class):
        """
        Class decorator for registering rule classes in appropriate register object.

        Args:
            gen_or_rule_class (GumoreGen or GumoreRule): Rule class to register

        Returns:
            None

        """
        mro = gen_or_rule_class.__mro__
        if GumoreGen in mro and gumore.core.rules.means.GumoreRule in mro:
            raise TypeError(
                "Class {} cannot be registered. It has two or more incompatible base classes: {}".format(
                    gen_or_rule_class, mro
                )
            )
        elif GumoreGen in mro:
            self.generators.register(gen_or_rule_class)
        elif gumore.core.rules.parcelgen.ParcelGen in mro:
            self.parcel_generators.register(gen_or_rule_class)
        elif gumore.core.rules.means.GumoreRule in mro:
            if not gen_or_rule_class.is_parcel_rule:
                self.rules.register(gen_or_rule_class)
            else:
                self.parcel_rules.register(gen_or_rule_class)
        elif gumore.core.rules.vehicle.Vehicle in mro:
            self.vessels.register(gen_or_rule_class)
        else:
            raise TypeError(
                "Class {} cannot be registered because of wrong base class(es): {}. Only classes with base "
                "class 'GumoreGen', 'GumoreRule' or 'Vehicle' can be registered.".format(
                    gen_or_rule_class, mro
                )
            )

    def _run_structural_data_correction(self):
        """
        Modifies `self.graph.zones.df` to
        Returns:

        """

        # TODO: Apply correction also on nuts2 level
        if len(self.structural_data_correction_factors) > 0:
            logger.info(
                f"Applying structural data correction factors: {self.structural_data_correction_factors}"
            )
            # all_codes = list(self.structural_data_correction_factors.keys())
            for code, corrections in self.structural_data_correction_factors.items():
                for variable_name, factor in corrections.items():
                    zones_df_orig = self.graph.zones.df.copy()

                    # apply to selected zone
                    value_orig = self.graph.zones.df.loc[
                        self.graph.zones.df.code == code, variable_name
                    ]
                    value_corrected = value_orig * factor
                    self.graph.zones.df.loc[
                        self.graph.zones.df.code == code, variable_name
                    ] = value_corrected

                    # balance other zones to leave sum the same
                    region_name = self.graph.zones.df[
                        self.graph.zones.df.code == code
                    ].bundesland.values[0]
                    codes_with_variable_name = list(
                        x[0]
                        for x in self.structural_data_correction_factors.items()
                        if variable_name in x[1].keys()
                    )
                    # print(variable_name, codes_with_variable_name)
                    # other_zones = (
                    #     (~self.graph.zones.df.code.isin(codes_with_variable_name))
                    #     & (self.graph.zones.df[variable_name] > 0)
                    # & (self.graph.zones.df.bundesland == region_name)
                    # other_zones = (self.graph.zones.df.code != code) & (
                    #     self.graph.zones.df[variable_name] > 0
                    # )  # & (self.graph.zones.df.bundesland == region_name)
                    # other_zones = (self.graph.zones.df.code != code) & (self.graph.zones.df.bundesland != "Einfüllkordon")
                    # n_other_zones = len(self.graph.zones.df.loc[other_zones, :])
                    # value_diff = value_orig - value_corrected
                    # value_diff_per_other_zone = value_diff.values[0] / n_other_zones
                    # self.graph.zones.df.loc[
                    #     other_zones, variable_name
                    # ] += value_diff_per_other_zone
                    self.graph.zones.df.loc[
                        self.graph.zones.df[variable_name] < 0, variable_name
                    ] = 0

                    # Update regional sums (needed for calibration)
                    self.regional_sums_for_structural_data[
                        variable_name
                    ] = self.regional_sums_for_structural_data[variable_name] + (
                        self.graph.zones.df.groupby("bundesl")[variable_name].sum()
                        - zones_df_orig.groupby("bundesl")[variable_name].sum()
                    )

        else:
            logger.info("No structural data correction factors found in rules file.")

        conn = get_db_connection()
        cursor = create_cursor(conn)
        cursor.execute("DROP VIEW IF EXISTS geoview__zones_corrected")
        conn.commit()

        self.graph.zones.df.round(2).to_sql(
            "zones_corrected", get_engine(), "public", "replace"
        )

        sql = f"""
            CREATE VIEW geoview__zones_corrected AS
            SELECT zc.*, z.geom
            FROM {get_zones_table_name()} z
            JOIN zones_corrected zc
            ON z.gid = zc.gid"""
        cursor.execute(sql)
        conn.commit()

    def _run_flow_correction(self, gen_results_df):
        """
        
        Returns:

        """
        df = gen_results_df.copy()

        if len(self.flow_corrections) > 0:
            logger.info(f"Applying {len(self.flow_corrections)} flow corrections.")
            cols_orig = df.columns
            for condition, factor in self.flow_corrections:
                df.rename({"f": "from", "t": "to"}, axis=1, inplace=True)
                df = df.merge(self.graph.zones.df, left_on="from", right_index=True)
                df = df.merge(
                    self.graph.zones.df,
                    left_on="to",
                    right_index=True,
                    suffixes=("_f", "_t"),
                )
                df.loc[condition(df), "qty"] = df.loc[condition(df), "qty"] * factor
                df.rename({"from": "f", "to": "t"}, axis=1, inplace=True)
                df = df[cols_orig]
            return df
        else:
            logger.info("No flow corrections found in rules file.")
            return df

    @staticmethod
    def _transform_rule_results_to_df(rule_results):
        rule_results_df = pd.DataFrame(
            rule_results,
            columns=(
                "f",
                "t",
                "commodity",
                "qty",
                "means",
                "source",
                "sink",
                "p_company",
                "p_hub_name",
                "p_type",
            ),
        )
        rule_results_df["vessel_count"] = 0
        rule_results_df = rule_results_df.astype(
            dtype={
                "f": np.int32,
                "t": np.int32,
                "qty": np.float32,
                "vessel_count": np.int32,
                "source": np.int32,
                "sink": np.int32,
            }
        )
        rule_results_df = (
            rule_results_df.groupby(by=["f", "t", "commodity", "means"])
            .sum()
            .reset_index()
        )
        return rule_results_df

    @staticmethod
    def _perform_results_sanity_check(gen_results_sum_df, rule_results_df):
        logger.info(f"Calculating sums for sanity check")
        rule_results_check = (
            rule_results_df.groupby(by=["source", "sink", "commodity"])
            .min()
            .reset_index()
        )
        rule_results_sum_df = rule_results_check.groupby("commodity").sum().qty
        remaining_quantities_df = (gen_results_sum_df - rule_results_sum_df).round()
        if (remaining_quantities_df != 0).any():
            logger.warning(
                f"Not all generated flows were covered by rules for means assignment and flow splitting.\n"
                f"Generation results:\n{gen_results_sum_df}\n\n"
                f"Means assignment results:\n{rule_results_sum_df}\n\n"
                f"Remaining quantities:\n{remaining_quantities_df}"
            )

    @staticmethod
    def _check_if_everything_was_transported(rule_results_df):
        if rule_results_df.groupby(by="commodity").sum()["qty"].sum() > 0:
            logger.warning(
                "Not all generated trips were transported by registered vessels. "
                "Remaining quantities:\n{}".format(
                    rule_results_df.groupby(by=["commodity", "means"]).sum()["qty"]
                )
            )


# this is the app singleton:

gumore_app = GumoreRunner()
