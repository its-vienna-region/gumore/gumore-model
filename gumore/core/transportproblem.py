import numpy as np
import pandas as pd
import time

from gumore.util.conf import get_config


def run_tpp():
    # Der folgende Code dient zur Aufbereitung der Ausgangsdaten und zur Ausführung der Berechnungen
    # Die für die Berechnungen notwendigen Funktionen werden aus "meine Funktionen" abgerufen

    # --------------------------------Distanz---------------------------------------------

    # Datei importieren

    dist = pd.read_csv("distance_hours_austria.csv")
    print(dist)

    # Tabelle auf benötigte Spalten reduzieren
    cols = [0, 3, 6]
    dist2 = dist[dist.columns[cols]]

    # Pivot Tabelle (Spalten, Zeilen und Werte am richtigen Platz) erstellen - Ergebnis ist die Distanzmatrix
    dist_pivot = dist2.pivot_table("hours", index="from_id", columns="to_id")

    # Pivot Tabelle in numpy matrix konvertieren
    dist_matx = np.asarray(dist_pivot)
    print(dist_matx)
    print(type(dist_matx))

    # wenn die Distanz 0 beträgt (Verbindung von selben Zelle), wird der Wert auf 999 erhöht
    dist_matx[dist_matx == 0] = 999

    # ---------------------------------Dictionary - id, code, name---------------------------------------------------
    # Das Dictionary wird benötigt,
    # um nach den Berechnungen den Verkehrszellen wieder die richtige ID, code und name zuordnen zu können

    # Auswahl der für das Dictionary relevanten Spalten

    id_code_name = dist[dist.columns[[0, 1, 2]]]

    # Entfernen von Duplikaten, damit die Einträge nicht doppelt vorkommen

    id_code_name_unique = id_code_name.drop_duplicates(
        subset="from_id", keep="first", inplace=False
    )

    # Erstellen des Dictionaries
    dict_id_code_name = {}

    num_rows, num_col = dist_matx.shape
    id_Zellen = list(range(1, num_col + 1))

    for b in id_Zellen:
        dict_id_code_name[b] = id_code_name_unique.loc[
            id_code_name_unique.from_id == b
        ].values

    print(dict_id_code_name[100])

    # ---------------------------Attraktion-----------------------------------

    # Einlesen Tabelle für Attraktion
    attr = pd.read_csv("attraction_austria.csv")

    # relevante Spalten auswählen
    attr3 = attr[["commodity", "att"]]

    # Erstellt eine Liste an Zahlen im Format 01, 02 bis 20
    nst_numb = ["%02d" % x for x in range(1, 21)]
    nst_numb1 = [f"0{x}" for x in range(1, 21)]

    # Ersellt die kompletten nst Bezeichnungen
    nst = []
    for c in nst_numb:
        nst.append("nst" + str(c))

    # Erstellt ein Dictionary, dass die Attraktion pro NST ausgibt

    dict_att = {}

    for b in nst_numb:
        dict_att["nst" + b] = attr3.loc[attr3.commodity == "nst" + b, "att"].values

    # -----------------------------------Produktion----------------------------------------------

    # Einlesen Tabelle für Produktion
    prod = pd.read_csv("production_austria.csv")

    # relevante Spalten auswählen
    prod3 = prod[["commodity", "prod"]]

    # Erstellt ein Dictionary, dass die Attraktion pro NST ausgibt

    dict_prod = {}

    for b in nst_numb:
        dict_prod["nst" + b] = prod3.loc[prod3.commodity == "nst" + b, "prod"].values

    # Erstellt ein Dictionary, dass die Distanzmatrix pro NST enthält - nur für Vogelsche Approximationsmethode relevant

    # dict_dist_matx = {}

    # for b in nst_numb:
    # dict_dist_matx["nst"+b] = dist_matx.copy()

    # -----------------------------------Berechnungen--------------------------------------------------------
    # Die Berechnungen können nicht alle gleichzeitig durchgeführt werden,
    # da sich in den ersten drei Methoden die Spalte für Produktion und die Zeile für Attraktion entleeren
    # Die # müssen daher manuell entfernt werden

    # Durchführung der Spaltenmiminum-Methode für alle NST
    coordinates = []
    sort_mycoordinates_per_column(dist_matx, coordinates)
    tpp_min_method(
        nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, coordinates
    )

    # Durchführung der Zeilenminimum-Methode für alle NST
    # coordinates = []
    # sort_mycoordinates_per_row(dist_matx,coordinates)
    # tpp_min_method(nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, coordinates)

    # Durchführung der Matrixminimum-Methode für alle NST
    # coordinates=[]
    # sort_mycoordinates_whole_matrix(dist_matx,coordinates)
    # tpp_min_method(nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, coordinates)

    # Duchführung der Vogelschen Approximation
    # Vogelsche_Approximation(nst_numb=nst_numb, dict_prod=dict_prod, dict_att=dict_att,
    #                         dict_id_code_name=dict_id_code_name, dist_matx=dist_matx)


import numpy as np
import pandas as pd
import time


def run_tpp_column_min(gen, graph, df_prod_cal, df_att_cal):
    nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, coordinates = transform_input_data(
        gen, graph, df_prod_cal, df_att_cal
    )
    sort_mycoordinates_per_column(dist_matx, coordinates)
    flows = tpp_min_method(
        nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, coordinates
    )
    return transform_output_data(flows, df_prod_cal, df_att_cal, graph)


def run_tpp_row_min(gen, graph, df_prod_cal, df_att_cal):
    nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, coordinates = transform_input_data(
        gen, graph, df_prod_cal, df_att_cal
    )
    sort_mycoordinates_per_row(dist_matx, coordinates)
    flows = tpp_min_method(
        nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, coordinates
    )
    return transform_output_data(flows, df_prod_cal, df_att_cal, graph)


def run_tpp_matrix_min(gen, graph, df_prod_cal, df_att_cal):
    nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, coordinates = transform_input_data(
        gen, graph, df_prod_cal, df_att_cal
    )
    sort_mycoordinates_whole_matrix(dist_matx, coordinates)
    flows = tpp_min_method(
        nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, coordinates
    )
    return transform_output_data(flows, df_prod_cal, df_att_cal, graph)


def run_tpp_vogel_approx(gen, graph, df_prod_cal, df_att_cal):
    nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, _ = transform_input_data(
        gen, graph, df_prod_cal, df_att_cal
    )
    flows = tpp_vogel_approx_method(
        nst_numb, dict_prod, dict_att, dict_id_code_name, dist_matx
    )
    return transform_output_data(flows, df_prod_cal, df_att_cal, graph)


def transform_input_data(gen, graph, df_prod_cal, df_att_cal):
    conf = get_config()
    df_prod_cal = df_prod_cal.copy()
    df_att_cal = df_att_cal.copy()
    nst_numb = [gen.commodity.code[3:]]
    dict_prod = {gen.commodity.code: df_prod_cal["prod"].values}
    dict_att = {gen.commodity.code: df_att_cal["att"].values}
    dist_matx = graph.distmats[gen.distmat_name].df.copy()
    dist_matx[dist_matx == 0] = 999
    dist_matx = dist_matx.values
    dict_id_code_name = {
        x[0]: np.array([[x[0], x[1], x[2]]], np.object)
        for x in (
            graph.zones.df.loc[
                :,
                [
                    conf.get("graph", "zones_code_column_name"),
                    conf.get("graph", "zones_name_column_name"),
                ],
            ]  # graph.zones.df.type != "Einfüllkordon"
            .reset_index()
            .values
        )
    }
    coordinates = []
    return nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, coordinates


def transform_output_data(
    sum_results: pd.DataFrame,
    df_prod_cal: pd.DataFrame,
    df_att_cal: pd.DataFrame,
    graph,
):
    conf = get_config()
    sum_results = sum_results.copy()
    sum_results.rename(
        columns={"from_id": "vb_from_id", "to_id": "vb_to_id", "quantity": "flow"},
        inplace=True,
    )
    sum_results.drop(
        columns=["from_code", "from_name", "to_code", "to_name", "NST"], inplace=True
    )
    sum_results = sum_results.merge(
        df_prod_cal["prod"], left_on="vb_from_id", right_index=True
    )
    sum_results = sum_results.merge(
        df_att_cal["att"], left_on="vb_to_id", right_index=True
    )
    region_col_name = conf.get("graph", "zones_region_column_name")
    sum_results = sum_results.merge(
        graph.zones.df[region_col_name], left_on="vb_from_id", right_index=True
    )
    sum_results.rename(columns={region_col_name: "b_from"}, inplace=True)
    sum_results = sum_results.merge(
        graph.zones.df[region_col_name], left_on="vb_to_id", right_index=True
    )
    sum_results.rename(columns={region_col_name: "b_to"}, inplace=True)
    return sum_results


# --------------------------Funktionen für die Minimummethoden-------------------------------------------------------


# sortiert die Koordinaten in aufsteigender Reihenfolge pro Spalte
# x Werte werden durch die betrachtete Spaltennummer hinzugefügt


def sort_mycoordinates_per_column(dist_matx, coordinates):
    num_rows, num_col = dist_matx.shape
    col_num = list(range(num_rows))
    ind_min = np.argsort(dist_matx, axis=0)
    for coord_to in col_num:  # coord to entspricht der betrachteten Spalte
        for coord_from in ind_min[
            :, coord_to
        ]:  # es werden alle Koordinaten pro Spalte gelistet
            coordinates.append((coord_from, coord_to))


# sortiert die Koordinaten in aufsteigender Reihenfolge pro Reihe


def sort_mycoordinates_per_row(dist_matx, coordinates):
    num_rows, num_col = dist_matx.shape
    row_num = list(range(num_rows))
    ind_min = np.argsort(dist_matx, axis=1)
    for coord_from in row_num:
        for coord_to in ind_min[coord_from, :]:
            coordinates.append((coord_from, coord_to))


# sortiert die Koordinaten in aufsteigender Reihenfolge für die gesamte Matrix


def sort_mycoordinates_whole_matrix(dist_matx, coordinates):
    ind_min_flat = np.argsort(dist_matx, axis=None, kind="mergesort")
    ind_min_arr = np.unravel_index(ind_min_flat, dist_matx.shape)
    coord = np.vstack(ind_min_arr).T
    coord.tolist()
    for (x, y) in coord:
        coordinates.append((x, y))


# "Hauptfunktion": Führt alle Schritte zur Lösung des tpp für die einzelnen NST Gruppen durch
# (außer die Sortierung der Koordinaten)


def tpp_min_method(
    nst_numb, dict_prod, dict_att, dist_matx, dict_id_code_name, coordinates
):
    qty_matx: np.ndarray = np.zeros_like(
        dist_matx.astype(float)
    )  # wird benötigt, um die Mengen zuzuteilen
    dict_results = {}
    remain_pos_col = []
    remain_pos_row = []
    for (
        b
    ) in (
        nst_numb
    ):  # Schleife für die einzelnen NST Gruppen, da jeweils attr und prod verschieden hoch ist
        key = "nst" + b
        prod = dict_prod[key]
        attr = dict_att[key]
        kontr_prod = sum(prod)
        kontr_attr = sum(attr)
        # Wenn von Beginn an die Summe der Werte von Attraktion 0 ist, gibt es keinen Bedarf, die Methode durchzuführen
        if kontr_attr > 0:
            allocation_amount(coordinates, attr, prod, qty_matx)
            control_values(
                qty_matx,
                prod,
                attr,
                kontr_attr,
                kontr_prod,
                key,
                remain_pos_col,
                remain_pos_row,
            )
            results_to_list(dist_matx, qty_matx, dict_id_code_name, key, dict_results)
        else:
            print(key, "enthält keine Werte")
    return make_summarized_table(
        dict_results, nst_numb
    )  # Erstellt eine csv Datei mit allen Verbindungen


# Diese Funktion dient der Zuteilung der Menge
# Vergleicht die Mengen in Attraktion und Produktion (entsprechend der Reihenfolge der bereits geordneten Koordinaten)
# und teilt die größtmögliche Menge zu
# Aktualisiert die Werte in Attr. und Prod.


def allocation_amount(coordinates, attr, prod, qty_matx):
    for (coord_from, coord_to) in coordinates:
        if attr[coord_to] < prod[coord_from]:
            qty_matx[coord_from, coord_to] = attr[coord_to]
            prod[coord_from] = prod[coord_from] - attr[coord_to]
            attr[coord_to] = 0
        elif attr[coord_to] > prod[coord_from]:
            qty_matx[coord_from, coord_to] = prod[coord_from]
            attr[coord_to] = attr[coord_to] - prod[coord_from]
            prod[coord_from] = 0
        elif attr[coord_to] == prod[coord_from]:
            qty_matx[coord_from, coord_to] = attr[coord_to]
            attr[coord_to] = 0
            prod[coord_from] = 0


# -------------------------------Funktionen relevant für alle vier Methoden------------------------------------------


# dient zur Kontrolle, ob die entsprechenden Menge auch wirklich zugeteilt wurden


def control_values(
    qty_matx, prod, attr, kontr_attr, kontr_prod, key, remain_pos_col, remain_pos_row
):
    zugeteilt_attr = sum(qty_matx.sum(axis=0))
    zugeteilt_prod = sum(qty_matx.sum(axis=1))
    verbleibend_prod = sum(prod)
    verbleibend_attr = sum(attr)
    Differenz_ursp_Menge = kontr_attr - kontr_prod
    Prozentsatz_verbleibend = (verbleibend_attr / kontr_attr) * 100
    # print("zugeteilte Menge Attraktion:", zugeteilt_attr,
    #       "\nzugeteilte Menge Produktion:", zugeteilt_prod,
    #       "\nverbleibender Rest der ursprüngl. Produktionmenge:", verbleibend_prod,
    #       "\nverbleibender Rest der ursprüngl. Attraktionsmenge:", verbleibend_attr,
    #       "\nverbleibender Rest der ursprüngl. Attraktionsmenge in Prozent:", Prozentsatz_verbleibend,
    #       "\nnicht zuteilbare Differenz der urspr. Mengen:", Differenz_ursp_Menge)
    # control_val_file=open('control_values_file.txt', 'a')
    # control_val_file.writelines(["\n\nfuer ", str(key), f"\nzugeteilte Menge Attraktion: {zugeteilt_attr:.5f}",
    #                              f"\nzugeteilte Menge Produktion: {zugeteilt_prod:.5f}",
    #                              f"\nverbleibender Rest der urspruengl. Produktionmenge: {verbleibend_prod:.5f}",
    #                              f"\nverbleibender Rest der urspruengl. Attraktionsmenge: {verbleibend_attr:.5f}",
    #                              f"\nverbleibender Rest der urspruengl. Attraktionsmenge in Prozent: "
    #                              f"{Prozentsatz_verbleibend:.5f}",
    #                              f"\nnicht zuteilbare Differenz der urspr. Mengen: {Differenz_ursp_Menge:.5f}",
    #                              f"\nProduktion konnte an folgenden Stellen nicht vollständig verteilt werden "
    #                              f"{remain_pos_col}",
    #                              f"\nAttraktion konnte an folgenden Stellen nicht vollständig verteilt werden "
    #                              f"{remain_pos_row}"])


# fügt pro NST die entsprechenden Bezeichnungen hinzu (id, code, name), damit die Daten gelesen werden können


def results_to_list(dist_matx, qty_matx, dict_id_code_name, key, dict_results):
    num_rows, num_col = dist_matx.shape
    num_cells = list(range(num_col))
    results_rows = []
    # Es werden die Werte von allen Verbindungen gelistet
    for coord_to in num_cells:
        for coord_from in num_cells:
            if qty_matx[coord_from, coord_to] >= 0:
                # Erstellen von separaten Reihen
                a = dict_id_code_name[coord_from + 1][0].tolist()
                b = dict_id_code_name[coord_to + 1][0].tolist()
                # a_b = np.concatenate((a, b), axis=1)
                quantity = qty_matx[coord_from, coord_to]
                nst = key
                quantity_nst = [[quantity, nst]]
                # a_b_quantity_nst = np.concatenate((a_b, quantity_nst), axis=1)
                # results_rows.append(a_b_quantity_nst)
                results_rows.append(a + b + [quantity] + [nst])
    # results = np.concatenate(results_rows, axis=0)
    results_df = pd.DataFrame(
        results_rows,
        columns=[
            "from_id",
            "from_code",
            "from_name",
            "to_id",
            "to_code",
            "to_name",
            "quantity",
            "NST",
        ],
    )
    # wird benötigt,
    # damit die Ergebnisse pro NST erhalten bleiben und nicht in für die nächste Gruppe überschrieben werden
    dict_results[key] = results_df
    # print("done")


# Führt die Ergebnisse in eine einzelne Datei zusammen


def make_summarized_table(dict_results, nst_numb):
    sum_results = pd.DataFrame()
    for b in nst_numb:
        if "nst" + b in dict_results:
            sum_results = pd.concat(
                [sum_results, dict_results["nst" + b]], ignore_index=True
            )
        else:
            print("keine Werte für nst" + b)
    # print(sum_results)
    # sum_results.to_csv("results_tpp.csv", index=False)
    return sum_results


# ------------------------------Vogelsche Approximation-----------------------------------------------------
# ------------------------------zusätzliche Funktionen------------------------------------------------------


# Führt die Vogelsche Approximationsmethode durch - als "Hauptfunktion"
# Erstellung der richtigen Variablen pro NST, Berechnung der regret-Werte


def tpp_vogel_approx_method(
    nst_numb, dict_prod, dict_att, dict_id_code_name, dist_matx
):
    # wird benötigt, um später jeweils die Ergebnisse für die einzelnen NST separat speichern zu können
    start = time.time()
    dict_results = {}
    num_rows, num_cols = dist_matx.shape
    total_lenght = num_cols * num_rows
    num_var = list(range(0, num_rows))
    # Schleife für alle NST Gruppen
    for b in nst_numb:
        key = "nst" + b
        prod = dict_prod[key]
        attr = dict_att[key]
        kontr_prod = sum(prod)  # werden zur Kontrolle benötigt
        kontr_attr = sum(attr)
        # Eine Aufteilung ist nur dann notwendig, wenn für die jeweilige NST Gruppe von Beginn etwas zu verteilen ist
        if kontr_attr > 0:
            # Erstellt eine Liste (die sozusagen als Reihe gesehen werden kann) die kennzeichnet,
            # ob die Attraktion vollständig erfüllt wurde (dann 1 sonst 0)
            marked_in_row_attr = [0] * num_cols
            # Erstellt eine Liste (diesmal sozusagen als Spalte) die kennzeichnet,
            # ob die Produktion bereits vollständig verteilt wurde (dann 1 sonst 0)
            marked_in_col_prod = [0] * num_rows
            qty_matx: np.ndarray = np.zeros_like(
                dist_matx.astype(float)
            )  # quantity matrix zum Einfüllen der Mengen
            # Wenn bereits bei Beginn die Produktion 0 ist, dürfen auch die Distanzwerte nicht mehr betrachtet werden
            # daher werden die entsprechenden Reihen/Spalten mit 1 markiert
            Zeilen_streichen_bei_0_Produktion(num_var, prod, marked_in_col_prod)
            Spalten_streichen_bei_0_Attraktion(num_var, attr, marked_in_row_attr)
            # wird benötigt, um aus der Schleife aussteigen zu können,
            # wenn auf eine Zelle mit dem Distanz-Wert 999 zugeteilt werden würde
            blocked_connection = 0
            # Wenn die Summe der verbleibenden Werte von prod oder attr 0 sind
            # oder eine gesperrte Verbindung gewählt werden würde, ist die Aufteilung beendet
            for t in list(range(0, total_lenght)):
                if sum(prod) == 0:
                    # print("beendet, bei Durchlauf", t, "da Summe prod 0 ist")
                    break
                elif sum(attr) == 0:
                    # print("beendet, bei Durchlauf", t, "da Summe attr 0 ist")
                    break
                elif blocked_connection == 1:
                    # print("Verbindung nicht möglich, daher wurde Schleife für nst", key, "beendet")
                    break
                else:
                    # normale Vorgehensweise VAM
                    # print("das ist der", (t + 1), "te Durchgang")
                    # Distanzmatrix verringern auf jene Werte, bei denen prod und attr > 0
                    row_to_remove = []
                    col_to_remove = []
                    # Erstellt eine Liste mit den Indices aller Spalten und Reihen,
                    # die nicht mehr betrachtet werden müssen
                    for x in num_var:
                        if marked_in_row_attr[x] == 1:
                            col_to_remove.append(x)
                        if marked_in_col_prod[x] == 1:
                            row_to_remove.append(x)
                    # In jedem Durchgang werden von neuem die Spalten/Zeilen aus der urspr. Distanzmatrix gestrichen
                    # Dadurch wird die modifizierte Distanzmatrix erstellt
                    mod_dist_matx = dist_matx.copy()
                    mod_dist_matx = np.delete(mod_dist_matx, row_to_remove, axis=0)
                    mod_dist_matx = np.delete(mod_dist_matx, col_to_remove, axis=1)
                    mod_dist_matx_num_rows, mod_dist_matx_num_cols = mod_dist_matx.shape
                    # Sortieren der Distanzzeiten der Größe nach
                    sort_min_per_row = np.sort(mod_dist_matx)
                    sort_min_per_col = np.sort(mod_dist_matx, axis=0)
                    # Wird für die Berechnung der Regret-Werte benötigt
                    min_per_row_all = []
                    sec_min_per_row_all = []
                    min_per_col_all = []
                    sec_min_per_col_all = []
                    # Wird für die Kontrolle bei einer gesperrten Verbindung benötigt
                    remain_pos_col = []
                    remain_pos_row = []
                    # VAM nur solange, bis die Anzahl der Reihen oder Spalten nur mehr 2 beträgt,
                    # dann bedarf es keiner Regret-Werte mehr
                    if (mod_dist_matx_num_rows > 2) and (mod_dist_matx_num_cols > 2):
                        # Die Anzahl der Spalten und Reihen der Distanzmatrix kann verschieden sein,
                        # da nicht zwingend gleich viele Reihen und Spalten gelöscht werden
                        num_iteration_for_rows = list(range(0, mod_dist_matx_num_rows))
                        num_iteration_for_cols = list(range(0, mod_dist_matx_num_cols))
                        # Regret-Werte für die einzelenen Spalten und Reihen berechnen
                        # 1.Schritt: Zuerst wird der kleinste und zweitkleinste Wert ermittelt
                        for i in num_iteration_for_rows:
                            min_per_row_value = sort_min_per_row[i][0]
                            min_per_row_all.append(min_per_row_value)
                            sec_min_per_row_value = sort_min_per_row[i][1]
                            sec_min_per_row_all.append(sec_min_per_row_value)
                        for i in num_iteration_for_cols:
                            min_per_col_value = sort_min_per_col[0][i]
                            min_per_col_all.append(min_per_col_value)
                            sec_min_per_col_value = sort_min_per_col[1][i]
                            sec_min_per_col_all.append(sec_min_per_col_value)
                        # 2.Schritt: Regret-Wert werden berechnet (zweitkleinster Wert minus kleinster Wert)
                        regret_values_as_col = [
                            a - b for a, b in zip(sec_min_per_row_all, min_per_row_all)
                        ]
                        regret_values_as_row = [
                            a - b for a, b in zip(sec_min_per_col_all, min_per_col_all)
                        ]
                        # davon muss der größte Wert gewählt werden
                        max_regret_value = max(
                            regret_values_as_col + regret_values_as_row
                        )
                        # Die nächste Funktion nimmt die Zuteilung vor
                        Finding_relevant_indices_and_solving_TPP(
                            regret_values_as_col,
                            regret_values_as_row,
                            dist_matx,
                            max_regret_value,
                            attr,
                            prod,
                            qty_matx,
                            num_var,
                            mod_dist_matx,
                            marked_in_col_prod,
                            marked_in_row_attr,
                        )
                    else:
                        # Wenn nur mehr zwei Reihen oder Spalten übrig sind,
                        # dann wird es der Matrixminimum Methode folgend fertig berechnet
                        # Modifizierte Koordinaten der Reihe nach ordnen, bei der kleinsten Distanz beginnend
                        ind_min_flat = np.argsort(
                            mod_dist_matx, axis=None, kind="mergesort"
                        )
                        ind_min_arr = np.unravel_index(
                            ind_min_flat, mod_dist_matx.shape
                        )
                        coord_mod = np.vstack(ind_min_arr).T
                        coord_mod.tolist()
                        coord = []
                        # Modifizierte Koordinaten umrechnen
                        for (coord_from, coord_to) in coord_mod:
                            # wird zur Kontrolle benötig, ob die Distanzwerte gleich sind
                            mod_dist_value_2nd = mod_dist_matx[coord_from][coord_to]
                            count_rows_1_until_coord_from = 0
                            count_rows_0_until_coord_from = 0
                            # die mit 0 und 1 markierten Reihen werden gezählt,
                            # bis die entsprechende Anzahl an Reihen mit 0 durchgesehen wurden
                            for i in num_var:
                                if coord_from >= count_rows_0_until_coord_from:
                                    if marked_in_col_prod[i] == 1:
                                        count_rows_1_until_coord_from = (
                                            count_rows_1_until_coord_from + 1
                                        )
                                    if marked_in_col_prod[i] == 0:
                                        count_rows_0_until_coord_from = (
                                            count_rows_0_until_coord_from + 1
                                        )
                            # coord_from bekommt einen neuen Wert
                            # auch zuvor ausgeschlossene Reihen werden nun berücksichtigt
                            coord_from = (
                                count_rows_0_until_coord_from
                                + count_rows_1_until_coord_from
                                - 1
                            )
                            count_cols_1_until_coord_to = 0
                            count_cols_0_until_coord_to = 0
                            # selbes Prinzp für coord_to
                            for i in num_var:
                                if coord_to >= count_cols_0_until_coord_to:
                                    if marked_in_row_attr[i] == 1:
                                        count_cols_1_until_coord_to = (
                                            count_cols_1_until_coord_to + 1
                                        )
                                    if marked_in_row_attr[i] == 0:
                                        count_cols_0_until_coord_to = (
                                            count_cols_0_until_coord_to + 1
                                        )
                            coord_to = (
                                count_cols_0_until_coord_to
                                + count_cols_1_until_coord_to
                                - 1
                            )
                            # dient der Kontrolle, ob die Umrechnung richtig erfolgte
                            dist_value_2nd = dist_matx[coord_from][coord_to]
                            if mod_dist_value_2nd != dist_value_2nd:
                                # print("Distanzwerte der mod Matrix und normalen Matrix stimmen nicht überein")
                                exit()
                            # die richtig umgerechneten Koordinaten werden gesammelt
                            coord.append((coord_from, coord_to))
                        # Aufteilung der Mengen der Reihe nach an jene Stelle, die die Koordinaten angeben
                        for (coord_from, coord_to) in coord:
                            # In der jeweiligen Spalte und Zeile muss die attr und prod > 0 sein
                            if (marked_in_col_prod[coord_from] == 0) and (
                                marked_in_row_attr[coord_to] == 0
                            ):
                                # Wenn die Zelle nicht gesperrt ist, wird die Menge verteilt
                                if dist_matx[coord_from][coord_to] != 999:
                                    allocation_amount_VA(
                                        coord_from, coord_to, attr, prod, qty_matx
                                    )
                                    Spalten_Zeilen_streichen(
                                        prod,
                                        attr,
                                        coord_from,
                                        coord_to,
                                        marked_in_col_prod,
                                        marked_in_row_attr,
                                    )
                                else:
                                    # Wenn die Zelle gesperrt ist, werden die betroffenen Koordinaten ausgegeben
                                    # (auch in den control values)
                                    remain_pos_row = coord_from
                                    remain_pos_col = coord_to
                                    # print("Zuteilung nicht möglich, da die Verbindung von",
                                    #   coord_from, "nach", coord_to, "gesperrt ist")
                                    blocked_connection = blocked_connection + 1
                                    break
                            else:
                                continue
            control_values(
                qty_matx,
                prod,
                attr,
                kontr_attr,
                kontr_prod,
                key,
                remain_pos_col,
                remain_pos_row,
            )
            results_to_list(dist_matx, qty_matx, dict_id_code_name, key, dict_results)
        else:
            pass
            # print(key, "enthält keine Werte")
    make_summarized_table(dict_results, nst_numb)
    ende = time.time()
    # print('Gesamtzeit: {:5.3f}s'.format(ende - start))


# Überprüft, ob sich der regret wert in Zeile oder Spalte befindet


def Finding_relevant_indices_and_solving_TPP(
    regret_values_as_col,
    regret_values_as_row,
    dist_matx,
    max_regret_value,
    attr,
    prod,
    qty_matx,
    num_var,
    mod_dist_matx,
    marked_in_col_prod,
    marked_in_row_attr,
):
    elem_pos = []
    max_regret_value_in_col_and_row(
        max_regret_value,
        regret_values_as_col,
        regret_values_as_row,
        num_var,
        qty_matx,
        dist_matx,
        prod,
        attr,
        mod_dist_matx,
        marked_in_col_prod,
        marked_in_row_attr,
    )
    max_regret_value_in_col(
        max_regret_value,
        regret_values_as_col,
        regret_values_as_row,
        num_var,
        qty_matx,
        elem_pos,
        dist_matx,
        prod,
        attr,
        mod_dist_matx,
        marked_in_col_prod,
        marked_in_row_attr,
    )
    max_regret_value_in_row(
        max_regret_value,
        regret_values_as_col,
        regret_values_as_row,
        num_var,
        qty_matx,
        elem_pos,
        dist_matx,
        prod,
        attr,
        mod_dist_matx,
        marked_in_row_attr,
        marked_in_col_prod,
    )


# Die Koordinaten werden für den Regret-Wert berechnet
# Wenn der max regret Wert mehrmals vorkommt, wird jene Zelle gewählt, die die geringsten Kosten aufweist
# Anschließend Zuteilung der Menge in einer weiteren Unterfunktion


def max_regret_value_in_col_and_row(
    max_regret_value,
    regret_values_as_col,
    regret_values_as_row,
    num_var,
    qty_matx,
    dist_matx,
    prod,
    attr,
    mod_dist_matx,
    marked_in_col_prod,
    marked_in_row_attr,
):
    if max_regret_value in regret_values_as_col:
        if max_regret_value in regret_values_as_row:
            # Variablen werden für die Umrechnung benötigt
            counter_col = 0
            counter_row = 0
            elem_pos_col = []
            elem_pos_row = []
            prev_count = 1000
            act_index = 0
            reference = 0
            if_statement_coord_from = 0
            if_statement_coord_to = 0
            # Ermittelt den Index von doppelten Werten
            # counter fängt mit Index 0 an und erhöht sich dann in jeder Schleife um 1
            # durch den counter kann somit der Spalten und Zeilen Indices ermittelt werden
            for i in regret_values_as_col:
                if i == max_regret_value:
                    elem_pos_col.append(counter_col)
                counter_col = counter_col + 1
            for j in regret_values_as_row:
                if j == max_regret_value:
                    elem_pos_row.append(counter_row)
                counter_row = counter_row + 1
            # Ermittelt den max regret value mit dem geringsten Wert in der modifizierten Distanzmatrix
            dist_sort = np.sort(mod_dist_matx, axis=1)
            for x in elem_pos_col:
                count = dist_sort[x][0]
                if prev_count > count:
                    prev_count = count
                    coord_from = x
                    act_index = x
            dist_sort = np.sort(mod_dist_matx, axis=0)
            for y in elem_pos_row:
                count = dist_sort[0][y]
                # print("count_row", count)
                if prev_count > count:
                    prev_count = count
                    coord_to = y
                    act_index = y
                    reference = reference + 1
            # Überprüft, ob der letzte ersetzte Wert coord_from oder coord_to war
            # je nachdem wird der fehlende Wert berechnet
            # die Variable reference stellt sicher, dass der Wert eindeutig zuordenbar ist
            if (act_index == coord_from) and (reference == 0):
                mod_ind_min = np.argsort(mod_dist_matx, axis=1)
                coord_to = mod_ind_min[coord_from][0]
                # Rechnet die modifizierten Koordinaten um
                Modifizierte_Koordinaten_umrechnen(
                    num_var,
                    coord_to,
                    coord_from,
                    marked_in_row_attr,
                    marked_in_col_prod,
                    dist_matx,
                    attr,
                    prod,
                    qty_matx,
                    mod_dist_matx,
                )
                # Stellt sicher, dass die anderen beiden if statements nicht zusätzlich durchgeführt werden
                if_statement_coord_from = if_statement_coord_from + 1
            if (
                (act_index == coord_to)
                and not (act_index == coord_from)
                and not (if_statement_coord_from == 1)
            ):
                mod_ind_min = np.argsort(mod_dist_matx, axis=0)
                coord_from = mod_ind_min[0][coord_to]
                # Rechnet die modifizierten Koordinaten um
                Modifizierte_Koordinaten_umrechnen(
                    num_var,
                    coord_to,
                    coord_from,
                    marked_in_row_attr,
                    marked_in_col_prod,
                    dist_matx,
                    attr,
                    prod,
                    qty_matx,
                    mod_dist_matx,
                )
                # Stellt ebenfalls sicher, dass das folgende if statement nicht zusätzlich durchgeführt wird
                if_statement_coord_to = if_statement_coord_to + 1
            if (
                (act_index == coord_from)
                and (act_index == coord_to)
                and not (if_statement_coord_from == 1)
                and not (if_statement_coord_to == 1)
            ):
                mod_ind_min = np.argsort(mod_dist_matx, axis=1)
                coord_to = mod_ind_min[coord_from][0]
                # Rechnet die modifizierten Koordinaten um
                Modifizierte_Koordinaten_umrechnen(
                    num_var,
                    coord_to,
                    coord_from,
                    marked_in_row_attr,
                    marked_in_col_prod,
                    dist_matx,
                    attr,
                    prod,
                    qty_matx,
                    mod_dist_matx,
                )


def max_regret_value_in_col(
    max_regret_value,
    regret_values_as_col,
    regret_values_as_row,
    num_var,
    qty_matx,
    elem_pos,
    dist_matx,
    prod,
    attr,
    mod_dist_matx,
    marked_in_col_prod,
    marked_in_row_attr,
):
    counter = 0
    if max_regret_value in regret_values_as_col:
        if max_regret_value not in regret_values_as_row:
            # print("max_regret_value_in_col", max_regret_value)
            for i in regret_values_as_col:
                if i == max_regret_value:
                    elem_pos.append(
                        counter
                    )  # gibt den Index des Regret-Werts an, können auch mehrere Indices sein
                counter = counter + 1
            # Wenn nur ein Element den max regret value besitzt, dann kann direkt der Index genommen werden
            if len(elem_pos) == 1:
                coord_from = regret_values_as_col.index(max_regret_value)
            # Wenn es mehrere sind, muss wieder der geringste Wert in der Distanzmatrix eruiert werden
            else:
                dist_sort = np.sort(mod_dist_matx, axis=1)
                prev_count = 1000
                for x in elem_pos:
                    count = dist_sort[x][0]
                    if prev_count > count:
                        prev_count = count
                        coord_from = x
            mod_ind_min = np.argsort(mod_dist_matx, axis=1)
            coord_to = mod_ind_min[coord_from][0]
            # Umrechnen der modifierten Koordinaten
            Modifizierte_Koordinaten_umrechnen(
                num_var,
                coord_to,
                coord_from,
                marked_in_row_attr,
                marked_in_col_prod,
                dist_matx,
                attr,
                prod,
                qty_matx,
                mod_dist_matx,
            )


def max_regret_value_in_row(
    max_regret_value,
    regret_values_as_col,
    regret_values_as_row,
    num_var,
    qty_matx,
    elem_pos,
    dist_matx,
    prod,
    attr,
    mod_dist_matx,
    marked_in_row_attr,
    marked_in_col_prod,
):
    counter = 0
    if max_regret_value in regret_values_as_row:
        if max_regret_value not in regret_values_as_col:
            for i in regret_values_as_row:
                if i == max_regret_value:
                    elem_pos.append(
                        counter
                    )  # gibt den Index des Regret-Wertes an, können auch mehrere Indices sein
                counter = counter + 1
            # Wenn nur ein Element den max regret value besitzt, dann kann direkt der Index genommen werden
            if len(elem_pos) == 1:
                coord_to = regret_values_as_row.index(max_regret_value)
                # print("mod_coord_to", coord_to)
            else:
                # Wenn es mehrere sind, muss wieder der geringste Wert in der Distanzmatrix eruiert werden
                dist_sort = np.sort(mod_dist_matx, axis=0)
                prev_count = 1000
                for x in elem_pos:
                    count = dist_sort[0][x]
                    if prev_count > count:
                        prev_count = count
                        coord_to = x
            mod_ind_min = np.argsort(mod_dist_matx, axis=0)
            coord_from = mod_ind_min[0][coord_to]
            # Modifizierte Koordinaten umrechnen
            Modifizierte_Koordinaten_umrechnen(
                num_var,
                coord_to,
                coord_from,
                marked_in_row_attr,
                marked_in_col_prod,
                dist_matx,
                attr,
                prod,
                qty_matx,
                mod_dist_matx,
            )


# Rechnet die modifizierten Koordinaten zurück in die Ausgangskoordinaten
def Modifizierte_Koordinaten_umrechnen(
    num_var,
    coord_to,
    coord_from,
    marked_in_row_attr,
    marked_in_col_prod,
    dist_matx,
    attr,
    prod,
    qty_matx,
    mod_dist_matx,
):
    mod_dist_value = mod_dist_matx[coord_from][
        coord_to
    ]  # wird zur Kontrolle der Umrechnung benötigt
    count_rows_1_until_coord_from = 0
    count_rows_0_until_coord_from = 0
    for i in num_var:
        # die mit 0 und 1 markierten Reihen werden gezählt,
        # bis die entsprechende Anzahl an Reihen mit 0 durchgesehen wurden
        if coord_from >= count_rows_0_until_coord_from:
            if marked_in_col_prod[i] == 1:
                count_rows_1_until_coord_from = count_rows_1_until_coord_from + 1
            if marked_in_col_prod[i] == 0:
                count_rows_0_until_coord_from = count_rows_0_until_coord_from + 1
    # coord_from ergibt sich aus allen Reihen
    coord_from = count_rows_0_until_coord_from + count_rows_1_until_coord_from - 1
    count_cols_1_until_coord_to = 0
    count_cols_0_until_coord_to = 0
    for i in num_var:
        # die mit 0 und 1 markierten Spalten werden gezählt,
        # bis die entsprechende Anzahl an Spalten mit 0 durchgesehen wurden
        if coord_to >= count_cols_0_until_coord_to:
            if marked_in_row_attr[i] == 1:
                count_cols_1_until_coord_to = count_cols_1_until_coord_to + 1
            if marked_in_row_attr[i] == 0:
                count_cols_0_until_coord_to = count_cols_0_until_coord_to + 1
    coord_to = count_cols_0_until_coord_to + count_cols_1_until_coord_to - 1
    dist_value = dist_matx[coord_from][
        coord_to
    ]  # wird zur Kontrolle der Umrechnung benötigt
    # Überprüft, ob sich die Distanzwerte der modifizierten und zurückgerechneten Koordinaten entsprechen
    if mod_dist_value != dist_value:
        # print("Distanzwerte der mod Matrix und normalen Matrix stimmen nicht überein")
        exit()
    # Stellt sicher, dass nie auf eine gesperrte Verbindung zugeteilt wird
    if dist_matx[coord_from][coord_to] != 999:
        allocation_amount_VA(coord_from, coord_to, attr, prod, qty_matx)
        Spalten_Zeilen_streichen(
            prod, attr, coord_from, coord_to, marked_in_col_prod, marked_in_row_attr
        )
    else:
        # print("Zuteilung nicht möglich, da die Verbindung von", coord_from, "nach", coord_to, "gesperrt ist")
        exit()


# Kennzeichnet jene Zeilen bei denen die Produktion 0 ist
# Diese Zeilen der Distanzmatrix dürfen in weiterer Folge nicht in die Berechnungen einbezogen werden


def Zeilen_streichen_bei_0_Produktion(num_var, prod, marked_in_col_prod):
    for s in num_var:
        if prod[s] == 0:
            marked_in_col_prod[s] = 1
        else:
            continue


# Kennzeichnet jene Spalten bei denen die Produktion 0 ist
# Diese Spalten der Distanzmatrix dürfen in weiterer Folge nicht in die Berechnungen einbezogen werden


def Spalten_streichen_bei_0_Attraktion(num_var, attr, marked_in_row_attr):
    for s in num_var:
        if attr[s] == 0:
            marked_in_row_attr[s] = 1
        else:
            continue


# Kennzeichnet die Spalte/Zeile, deren Attraktion/Produktion nach der Zuteilung 0 beträgt
# (wenn diese 0 beträgt, wird die Makierung auf 1 gesetzt - das bedeutet, dass es bereits erfüllt wurde)


def Spalten_Zeilen_streichen(
    prod, attr, coord_from, coord_to, marked_in_col_prod, marked_in_row_attr
):
    if prod[coord_from] == 0:
        marked_in_col_prod[coord_from] = 1
    else:
        pass
        # print("in der Zeile gibt es noch mehr zu verteilen")
    if attr[coord_to] == 0:
        marked_in_row_attr[coord_to] = 1
    else:
        pass
        # print("in der Spalte gibt es noch mehr zu verteilen")


# Zuordnung der Mengen
# Unterschied zu min Methode ist, dass für jede Berechnung die Koordinaten neu berechnet werden und
# somit gibt es keine Sammlung der Koordinaten


def allocation_amount_VA(coord_from, coord_to, attr, prod, qty_matx):
    if attr[coord_to] < prod[coord_from]:
        # print("attr, kleiner", attr[coord_to])
        # print("prod, größer", prod[coord_from])
        qty_matx[coord_from, coord_to] = attr[coord_to]
        prod[coord_from] = prod[coord_from] - attr[coord_to]
        attr[coord_to] = 0
    elif attr[coord_to] > prod[coord_from]:
        # print("attr, größer", attr[coord_to])
        # print("prod, kleiner", prod[coord_from])
        qty_matx[coord_from, coord_to] = prod[coord_from]
        attr[coord_to] = attr[coord_to] - prod[coord_from]
        prod[coord_from] = 0
    elif attr[coord_to] == prod[coord_from]:
        # print("attr, equal", attr[coord_to])
        # print("prod, equal", prod[coord_from])
        qty_matx[coord_from, coord_to] = attr[coord_to]
        attr[coord_to] = 0
        prod[coord_from] = 0
