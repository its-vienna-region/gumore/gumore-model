from collections import namedtuple
import time
from typing import List, Type, Union

import pandas as pd

import gumore.core.graph
from gumore.core.entities import com_nst15
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)

HubGenDist = namedtuple(
    "HubGenDist",
    "commodity company hub_name hub_zone_id ptype parcels target_zones target_weights "
    "target_weights_rel",
)
HubInfo = namedtuple("HubInfo", "name id weight target_zones")


class ParcelGen:
    commodity: gumore.core.entities = com_nst15
    b_weight_columns = list("abcdefghijklmnopqrs")
    company: str = None
    hub_names: List[str] = None
    ptype: str = None
    total_parcels: float = None

    def __init__(self, graph: gumore.core.graph.Graph) -> None:
        self.graph: gumore.core.graph.Graph = graph
        self.df: pd.DataFrame = graph.zones.df
        self.hub_ids = [self._get_zone_id_for_hub_name(x) for x in self.hub_names]

    def calc_hub_weight(self, hub_name: str) -> Union[int, float]:
        if self.ptype == "2C":
            return self.df.loc[self.df["k_" + hub_name] > 0, "ew"].sum()
        elif self.ptype == "2B":
            return (
                self.df.loc[self.df["k_" + hub_name] > 0, self.b_weight_columns]
                .sum()
                .sum()
            )

    def calc_target_weights(self, target_zones) -> pd.DataFrame:
        if self.ptype == "2C":
            return self.df.loc[target_zones, "ew"]
        elif self.ptype == "2B":
            return self.df.loc[target_zones, self.b_weight_columns].sum(axis=1)

    def find_target_zones(self, hub_name: str) -> List[str]:
        return self.df.loc[self.df["k_" + hub_name] > 0].index.tolist()

    def _calc_target_weights_full(
        self, target_zones
    ) -> [List[Union[int, float]], List[float]]:
        target_weights = self.calc_target_weights(target_zones)
        target_weights = target_weights.fillna(value=0)
        target_weights = target_weights.tolist()
        target_weights_sum = sum(target_weights)
        target_weights_rel = [x / target_weights_sum for x in target_weights]
        return target_weights, target_weights_rel

    def _get_zone_id_for_hub_name(self, hub_name):
        hub_df_row = self.df.loc[self.df.hubname.str.contains(hub_name, na=False), :]
        assert len(hub_df_row) == 1, (
            "Each hub name needs to be present exactly one time in the zones table. "
            f"Hub name '{hub_name}' was found {len(hub_df_row)} times. Check column "
            f"'hubname' in your zones table!\n"
            f"{hub_df_row}"
        )
        return hub_df_row.index[0]


class ParcelGenSet:
    def __init__(self, graph: gumore.core.graph.Graph) -> None:
        self.graph: gumore.core.graph.Graph = graph
        self.generators: List[Type[ParcelGen]] = []

    def register(self, genclass: Type[ParcelGen]) -> None:
        self.generators.append(genclass)
        logger.debug(
            f"Parcel generation nr {len(self.generators)} <{genclass.__name__}> registered"
        )

    def execute(self, concurrency: str = "single_threaded") -> pd.DataFrame:
        """ run all the registered traffic generation classes """
        parcel_gens: List[ParcelGen] = []
        for gen_class in self.generators:
            gen = gen_class(self.graph)
            parcel_gens.append(gen)

        if concurrency in ("single_threaded", "debug", "parallel"):
            df_parcel_gen_results = self._execute_single_threaded(parcel_gens)
        else:
            raise ValueError(
                "Argument 'concurrency' needs to be in ('single_threaded', 'parallel', 'debug')"
            )

        return df_parcel_gen_results

    def _execute_single_threaded(self, parcel_gens):
        logging.info(
            "Starting single threaded parcel generation for {} zones and {} generation rules".format(
                len(self.graph.zones.zone_ids), len(parcel_gens)
            )
        )
        start_time_single = time.time()
        parcel_gen_results = map(
            self._distribute_parcels_to_hubs_and_find_target_zones, parcel_gens
        )
        parcel_gen_results = [x for y in parcel_gen_results for x in y]
        df = self._parcel_gen_results_to_df(parcel_gen_results)
        running_time = time.time() - start_time_single
        logging.info(
            "Single threaded parcel generation for {} zones and {} generation "
            "rules finished after {:.2f} seconds.".format(
                len(self.graph.zones.zone_ids), len(parcel_gens), running_time
            )
        )
        return df

    @staticmethod
    def _distribute_parcels_to_hubs_and_find_target_zones(gen: ParcelGen):
        hub_infos: List[HubInfo] = []
        for hub in zip(gen.hub_names, gen.hub_ids):
            hub_infos.append(
                HubInfo(
                    hub[0],
                    hub[1],
                    gen.calc_hub_weight(hub[0]),
                    gen.find_target_zones(hub[0]),
                )
            )
        hub_weights_sum = sum([x.weight for x in hub_infos])
        parcels_for_hub = [
            HubGenDist(
                gen.commodity,
                gen.company,
                info.name,
                info.id,
                gen.ptype,
                info.weight / hub_weights_sum * gen.total_parcels,
                info.target_zones,
                *gen._calc_target_weights_full(info.target_zones),
            )
            for info in hub_infos
        ]
        return parcels_for_hub

    @staticmethod
    def _parcel_gen_results_to_df(parcel_gen_results: List[HubGenDist]) -> pd.DataFrame:
        columns = [
            "commodity",
            "company",
            "hub_name",
            "ptype",
            "hub_zone_id",
            "target_zone",
            "parcels",
        ]
        if len(parcel_gen_results) > 0:
            df_out = pd.DataFrame()
            for x in parcel_gen_results:
                rows = [
                    (
                        str(x.commodity),
                        x.company,
                        x.hub_name,
                        x.ptype,
                        x.hub_zone_id,
                        y[0],
                        int(round(x.parcels * y[1])),
                    )
                    for y in zip(x.target_zones, x.target_weights_rel)
                ]
                df_out = df_out.append(rows)
            df_out.columns = columns
            df_out.reset_index(drop=True, inplace=True)
        else:
            df_out = pd.DataFrame(columns=columns)
        return df_out
