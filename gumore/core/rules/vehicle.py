from abc import ABC, abstractmethod
import time
from typing import Type, Union

import numpy as np
import pandas as pd
from psycopg2.extras import execute_values

from gumore.core.entities import *
from gumore.core.flow import Flow
from gumore.util.conf import get_config
from gumore.util.database import create_cursor, get_db_connection, TABLES
import gumore.util.global_vars as global_vars
from gumore.util.log import *

pd.set_option("display.max_columns", 20)
pd.set_option("display.width", 200)
pd.set_option("compute.use_bottleneck", True)
pd.set_option("compute.use_numexpr", True)

initialize_logger()
logger = logging.getLogger(__name__)


class Vehicle(ABC):
    """
    Abstract base class for vessel trip generation.
    """

    payload = False
    name = None

    def __init__(self, context) -> None:
        """
        Args:
            context: An object with an attribute 'rule_results_df' holding a `pandas.DataFrame`.
        """
        self.context = context
        if not self.name:
            self.name = self.__class__.__name__
        self.vessel_remainder_trips_generation_mode = None
        self.payload_matrix = global_vars.matrices_model_config["matrix_payload"]
        self.payload_distmat = self.context.distmats[self.payload_distmat_name].df

        distmat = list(self.context.distmats.values())[0].df
        distmat[:] = 0
        distmat.index.name = "f"
        distmat.columns.name = "t"
        if get_config().getboolean("debugging", "read_zones_sample_table"):
            zones_sample_table_fraction = get_config().getint(
                "debugging", "zones_sample_table_fraction"
            )
            fraction_zone_ids = [
                x for x in distmat.index if x % zones_sample_table_fraction == 0
            ]
            distmat = distmat.loc[fraction_zone_ids, fraction_zone_ids]
        self.trips = distmat

        self.full_trips_n = 0
        self.remainder_trips_n = 0
        self.empty_trips_n = 0

    @property
    @abstractmethod
    def means(self) -> TransportMeans:
        return self.means

    @property
    @abstractmethod
    def payload_distmat_name(self) -> str:
        return self.payload_distmat_name

    @property
    @abstractmethod
    def load(self) -> Commodity:
        return self.load

    def drive(self) -> Union[pd.DataFrame, None]:
        """
        For all applicable flows, trips of individual vessels are generated.

        This process has the following steps:

        * Read remainder trip generation mode from config
        * Filter flows to those, which are applicable to vessel type
        * Generate 'full trips': individual vessels filled up with one commodity each
        * Generate 'remainder trips': fill up additional vessels with the remaining commodities
        * Generate empty trips by applying the user defined concrete method self.empty()
        * Modify flows in context by changing the 'qty'-values
        * Prepare DataFrame with final results

        Returns:
            pandas.DataFrame: The generated trips in aggregated form.
        """
        self.vessel_remainder_trips_generation_mode = get_config().get(
            "model_run", "vessel_remainder_trips_generation_mode"
        )
        logger.info("    Driving vessels of type {} ".format(self.name))

        df = self._get_restricted_data()

        if len(df) == 0:
            logger.warning(
                "    No applicable flows found for Vessel type {}".format(self.name)
            )
            return

        logger.debug("      Generating full trips")
        df = self._generate_full_trips(df)

        logger.debug("      Generating remainder trips")
        df = self._generate_remainder_trips(df)

        df_melted = self.trips.reset_index().melt(id_vars="f")

        logger.debug("      Generating empty trips")
        empty_trips = self._generate_empty_trips(df_melted)

        logger.debug("      Preparing final results")
        df_melted = self._prepare_final_results(df_melted, empty_trips)

        self.context.rule_results_df.iloc[
            df.index, self.context.rule_results_df.columns.get_loc("qty")
        ] = df["qty"]

        logger.info(
            "    Vessels of type {} made {} full trips, {} remainder trips, "
            "and {} empty trips.".format(
                self.name, self.full_trips_n, self.remainder_trips_n, self.empty_trips_n
            )
        )

        return df_melted

    @abstractmethod
    def empty(self, flow: Flow) -> pd.DataFrame:
        """
        Calculates the 'empty trips'. As abstract method it needs to be overridden by the user.

        This function is applied to the DataFrame resulting from remainder trip generation and thus can only
        reference values which are stored therein.

        Examples::

            def empty(self, flow):
                return self.trips.loc[flow.t, flow.f] * 0.1

            def empty(self, flow):
                return max(self.trips.loc[flow.t,flow.f]-self.trips.loc[flow.f,flow.t], 0)

        Args:
            flow (pandas.Series): Individual flow = row of DataFrame to which empty() is applied.

        Returns:
            pandas.DataFrame: Needs to modify self.trips as a side effect.
        """
        pass

    def _get_restricted_data(self) -> pd.DataFrame:
        """
        Filters the DataFrame (passed as part of the context) by 'commodity' and 'means'.

        The column 'commodity' is filtered by reading self.load.
        The column 'means' is filtered by reading self.means

        Returns:
            pandas.DataFrame
        """
        candidate_index = self.context.rule_results_df.loc[
            (self.context.rule_results_df["commodity"].isin(self.load))
            & (self.context.rule_results_df["means"] == self.means)
        ].index
        return self.context.rule_results_df.loc[candidate_index, :]

    def _generate_full_trips(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        Generates 'full trips': individual vessels filled up with one commodity each.

        The following columns are added to df:

        * commodities
        * maxload
        * vessel_count
        * vessel_name

        The column 'qty' is reduced, depending on the mode of remainder trips calculation:

        * 'simple': qty is set to 0, which results in no further remainder trips caluclation
        * ' detailed': qty is reduced according to number and maxload of vessel class.

        The result of this process is written into the matrix `self.trips`.

        Args:
            df (pandas.DataFrame): The filtered DataFrame holding flow information.

        Returns:
            pandas.Dataframe: with added and re-calculated values

        """
        df["commodities"] = df.apply(lambda x: x.commodity if x.qty > 0 else "", axis=1)
        df["distance"] = df.apply(lambda x: self.payload_distmat.at[x.f, x.t], axis=1)
        distance_limits = np.array(
            list(self.payload_matrix.loc[:, self.means.code].columns)
        )
        if df["distance"].max() > distance_limits.max():
            raise ValueError(
                "Max Distance in flows is {}, but payload distance limits are {}. "
                "Check model config in {}".format(
                    df["distance"].max(),
                    distance_limits,
                    get_config().get("model_config", "sheet_path"),
                )
            )

        df["maxload"] = df.apply(
            lambda x: self._get_payload(x, distance_limits), axis=1
        )
        if self.vessel_remainder_trips_generation_mode == "simple":
            df["vessel_count"] = df["qty"] / df["maxload"]
            df["qty"] = 0
        elif self.vessel_remainder_trips_generation_mode == "detailed":
            df["vessel_count"] = (df["qty"] / df["maxload"]).astype(np.int32)
            df["vessel_count"] = df["vessel_count"].where(
                (df["qty"] > df["maxload"]) | (df["qty"] == 0), 1
            )
            df["qty"] = (df["qty"] - df["vessel_count"] * df["maxload"]).clip(lower=0)
        else:
            raise ValueError

        df["vessel_name"] = self.name
        self.commodities = (
            df.groupby(by=["f", "t"])["commodities"]
            .unique()
            .apply(lambda x: ",".join(filter(None, x)))
        )
        vessel_counts = (
            df[(df["commodity"].isin(self.load)) & (df.vessel_name == self.name)]
            .groupby(by=["f", "t", "vessel_name"], sort=False, as_index=False)
            .sum()
            .fillna(0)
        )

        # self.trips = vessel_counts.pivot_table(index="f", columns="t", values="vessel_count", aggfunc=np.sum,
        #                                        fill_value=0)

        trips = vessel_counts.pivot_table(
            index="f", columns="t", values="vessel_count", aggfunc=np.sum, fill_value=0
        )
        self.trips = self.trips.add(trips, axis=0, fill_value=0)

        if self.vessel_remainder_trips_generation_mode == "simple":
            self.trips = np.ceil(self.trips).astype(np.int32)

        self.full_trips_n = int(self.trips.sum().sum())

        return df

    def _get_payload(self, row: pd.Series, distance_limits: np.ndarray) -> float:
        if self.payload:
            return self.payload
        try:
            distance_class = distance_limits[
                list(row.distance <= distance_limits).index(True)
            ]
        except ValueError:
            raise ValueError(
                f"Could not find distance '{row.distance}' in distance_limits {distance_limits}. "
                f"Check if distance matrix {self.payload_distmat_name} holds nan values "
                f"or if distance limits in model config are set incorrectly."
            )
        try:
            return self.payload_matrix.loc[row.commodities, (row.means, distance_class)]
        except KeyError:
            return 0

    def _generate_remainder_trips(self, df: pd.DataFrame) -> pd.DataFrame:
        """
        Generates 'remainder trips': fill up the vessels with the remaining commodities.

        Remainder trips are only calculated if remainder trip generation mode is set to 'detailed' in config.

        The result of this process is written into the matrix `self.trips`.

        Args:
            df (pandas.DataFrame): The DataFrame holding the results of the full trip generation.

        Returns:
            pandas.DataFrame

        """
        self.remainder_trips_n = 0
        if self.vessel_remainder_trips_generation_mode == "detailed":
            while df["qty"].sum() > 0:
                df["qty_normalized"] = df["qty"] / df["maxload"]
                df_iter = df.sort_values(by=["f", "t", "qty_normalized"]).copy()
                from_to = tuple()
                qty = 0
                for row in df_iter.itertuples():
                    if (qty + row.qty_normalized > 1) or (from_to != (row.f, row.t)):
                        if qty > 0:
                            self.trips.loc[from_to] += 1
                            self.remainder_trips_n += 1
                        from_to = (row.f, row.t)

                        qty = 0
                    if qty + row.qty_normalized <= 1:
                        qty += row.qty_normalized
                        df.ix[row.Index, "qty"] = 0
                if qty > 0:
                    self.trips.loc[from_to] += 1
                    self.remainder_trips_n += 1

        return df

    def _generate_empty_trips(self, df_melted: pd.DataFrame) -> pd.Series:
        """
        Generates 'empty trips' by applying self.empty()

        The result of this process is written into the matrix `self.trips`.

        Args:
            df_melted (pandas.DataFrame): The melted DataFrame, holding the results of remainder trip generation.

        Returns:
            pandas.Series

        """
        empty_trips = df_melted.apply(self.empty, axis=1).round()
        self.empty_trips_n = int(empty_trips.sum())

        return empty_trips

    def _prepare_final_results(
        self, df_melted: pd.DataFrame, empty_trips: pd.Series
    ) -> pd.DataFrame:
        """
        Prepares the DataFrame which is finally returned by drive().

        The following columns are added:

        * vessel
        * means
        * commodities (by merging with self.commodities)

        Empty trips are added to column 'value'. Then the column is renamed to 'count'.

        Args:
            df_melted (pandas.DataFrame): The melted DataFrame, holding the results of emtpy trip generation.
            empty_trips (pandas.Series): The generated empty trips.

        Returns:
            pandas.DataFrame
        """
        df_melted["value"] = df_melted["value"] + empty_trips
        df_melted = df_melted.astype({"value": np.int32})
        df_melted = df_melted.rename(index=str, columns={"value": "count"})
        df_melted["vessel"] = self.name
        df_melted["means"] = self.means.code
        df_melted = df_melted.merge(self.commodities, how="left", on=["f", "t"])
        df_melted.loc[empty_trips > 0, "commodities"] = (
            df_melted.loc[empty_trips > 0, "commodities"].fillna("") + ",empty"
        )
        df_melted["commodities"] = df_melted["commodities"].str.lstrip(",")
        df_melted = df_melted[df_melted["count"] > 0]
        df_melted = df_melted[["f", "t", "commodities", "means", "vessel", "count"]]

        return df_melted


class VehicleSet:
    def __init__(self, context) -> None:
        self.context = context
        self.vessels = []

    def register(self, vessel_class: Type[Vehicle]) -> None:
        try:
            name = vessel_class.name
        except AttributeError:
            name = vessel_class.__name__
            vessel_class.name = vessel_class.__name__
        self.vessels.append(vessel_class)
        logger.debug("Vessel type nr %d <%s> registered" % (len(self.vessels), name))

    @log_tracebacks()
    def execute(self, concurrency="single_threaded") -> pd.DataFrame:
        vessel_objects = []
        for vessel_class in self.vessels:
            vessel_object = vessel_class(self.context)
            if not hasattr(vessel_object, "name"):
                vessel_object.name = vessel_class.__name__
            vessel_objects.append(vessel_object)

        vessel_flows_result = pd.DataFrame()
        vessel_remainder_trips_generation_mode = get_config().get(
            "model_run", "vessel_remainder_trips_generation_mode"
        )

        logger.info(
            "[ ] Starting single threaded vessel generation for {} vessel rules. "
            "Remainder trip generation mode '{}'".format(
                len(self.vessels), vessel_remainder_trips_generation_mode
            )
        )
        start_time = time.time()
        for vessel_object in vessel_objects:
            vessel_flows = vessel_object.drive()
            if type(vessel_flows) != type(None):
                vessel_flows_result = vessel_flows_result.append(vessel_flows)

        if len(vessel_flows_result) > 0:
            vessel_flows_result = vessel_flows_result[
                vessel_flows_result["f"] != vessel_flows_result["t"]
            ]
        else:
            vessel_flows_result = vessel_flows_result.reindex(columns=["f", "t", "commodities", "means", "vessel", "count"])

        running_time = time.time() - start_time
        logging.info(
            f"[X] Trip generation for {len(self.vessels)} vessel rules finished after "
            f"{running_time:.2f} seconds."
        )

        logging.info(
            f"[ ] Starting to write {len(vessel_flows_result)} aggregated trips to "
            f"database table '{TABLES['trips']}'."
        )
        conn = get_db_connection()
        cur = create_cursor(conn)

        execute_values(
            cur,
            f"INSERT INTO {TABLES['trips']} (f, t, commodities, means, vessel, count) "
            "VALUES %s",
            vessel_flows_result.itertuples(index=False),
            page_size=1000,
        )
        conn.commit()
        cur.close()

        logging.info("[X] Finished database export.")

        return vessel_flows_result
