from abc import ABC, abstractmethod
from collections import Counter, defaultdict, OrderedDict
import functools
import importlib
import math
from multiprocessing import current_process
import sys
import time
from typing import Dict, List, Tuple

import pandas as pd

from pathos.pools import ProcessPool
import psutil
from psycopg2.extras import execute_values

import gumore.core.flow
import gumore.calibrate.zones
import gumore.core.rules.vehicle
import gumore.core.entities
from gumore.core.flow import Flow
from gumore.core.graph import Graph
from gumore.core.rules.gen import GumoreGen
import gumore.core.assignment
import gumore.core.rules.gen
from gumore.util import global_vars
from gumore.util.conf import (
    get_active_config_as_dict,
    get_config,
    package_name,
    is_scenario,
    scenario_name,
)
from gumore.util.database import get_db_connection, create_cursor, TABLES
from gumore.util.log import *

pd.options.display.float_format = "{:.2f}".format

initialize_logger()
logger = logging.getLogger(__name__)
is_main_process = current_process().pid == global_vars.parent_pid
if is_main_process:
    logger.info(f"Active config: {get_active_config_as_dict()}")
    if is_scenario():
        logger.info(f"RUNNING IN SCENARIO MODE. Active scenario: {scenario_name()}")
    else:
        logger.info("RUNNING IN BASELINE MODE.")


# Load project specific configuration for findzones and functions (if they exist)
sys.path.insert(0, global_vars.proj_dir)
try:
    if is_main_process:
        logger.info(f"Importing findzones module {package_name()}.findzones")
    myfindzones = importlib.__import__(
        f"{package_name()}.findzones", globals(), locals(), [], 0
    )
except ModuleNotFoundError:
    if is_main_process:
        logger.warning(f"Findzones module {package_name()}.findzones not found!")
        logger.info("Importing findzones module mymodel.findzones")
    try:
        myfindzones = importlib.__import__(
            "mymodel.findzones", globals(), locals(), [], 0
        )
    except ModuleNotFoundError:
        logger.warning("Findzones module mymodel.findzones not found!")
try:
    if is_main_process:
        logger.info(f"Importing functs module {package_name()}.functs")
    myfuncts = importlib.__import__(
        f"{package_name()}.functs", globals(), locals(), [], 0
    )
except ModuleNotFoundError:
    if is_main_process:
        logger.warning(f"Functs module {package_name()}.functs not found!")
    try:
        myfuncts = importlib.__import__("mymodel.functs", globals(), locals(), [], 0)
    except ModuleNotFoundError:
        logger.warning("Functs module mymodel.functs not found!")


class GumoreRule(ABC):
    is_parcel_rule = False
    ent = gumore.core.entities
    try:
        findzones = myfindzones.findzones
    except NameError:
        findzones = None
    try:
        funct = myfuncts.functs
    except NameError:
        funct = None

    def __init__(self, context):
        self.ctxt = context
        self.counter = Counter()
        if hasattr(self, "distmat_name"):
            self.distmat = self.ctxt.distmats[self.distmat_name]

    @abstractmethod
    def condition(self, flow: Flow) -> bool:
        pass

    def action(self, flow: Flow):
        """ the action modifies the flows by adding the new
            flow as children to the original flow.
            This can be cascaded, so an original flow
            becomes a tree of flows.
            Only the leaf nodes are valid.
            Make sure to overwrite the default action.
            """
        raise NotImplementedError

    def get_flow_distance(self, flow):
        return self.distmat(flow.f, flow.t)

    def zone_attribute_value(self, zone_id, attribute_name):
        return self.ctxt.graph.zones.get_atts_for_zone(zone_id)[attribute_name]

    def zone_has_gbhf_for_nst(self, zone_id: int, nst: str) -> bool:
        return (
            bool(self.zone_attribute_value(zone_id, "gbhf"))
        ) and nst in self.zone_attribute_value(zone_id, "gbhf_nst").split(";")

    def flow_is_from_or_to_external(self, flow):
        return (self.zone_attribute_value(flow.f, "type") == "Einfüllkordon") or (
            self.zone_attribute_value(flow.t, "type") == "Einfüllkordon")
    
    def flow_is_internal(self, flow):
        return not self.flow_is_from_or_to_external(flow)

    def flow_is_from_external(self, flow):
        return self.zone_attribute_value(flow.f, "type") == "Einfüllkordon"

    def flow_is_to_external(self, flow):
        return self.zone_attribute_value(flow.t, "type") == "Einfüllkordon"


class ParcelRule(GumoreRule):
    is_parcel_rule = True


class GumoreRuleSet:
    conf = get_config()
    if conf:
        log_rule_fitting = conf.getboolean("logging", "log_rule_fitting")

    def __init__(self, context):
        self.rules = []
        self.context = context
        self.counters: Dict[str, Counter] = defaultdict(Counter)

    def __str__(self):
        return "GumoreRuleset ( {} )".format(self.rules)

    def register(self, rule: GumoreRule):
        """ register a rule to the ruleset
            the rules are tried in order of registration
            when the condition of a rule fits,
            the action of a rule is executed
        """
        self.rules.append(rule)

    def execute(
        self, gen_results, mode: str, concurrency="single_threaded"
    ) -> List[Tuple[int, int, str, float, str, int, str, str, str]]:
        rule_results = []

        if mode == "normal":
            row_to_flow = lambda row: gumore.core.flow.Flow(
                f=row.f, t=row.t, commodity=row.commodity, qty=row.qty
            )
        elif mode == "parcels":
            row_to_flow = lambda row: gumore.core.flow.Flow(
                f=row.hub_zone_id,
                t=row.target_zone,
                commodity=row.commodity,
                qty=row.parcels,
                p_company=row.company,
                p_hub_name=row.hub_name,
                p_type=row.ptype,
            )
        else:
            raise ValueError(
                f"Invalid mode '{mode}'. It needs to be either 'normal' "
                f"or 'parcels'"
            )

        flows_without_rule = []
        rules_count = Counter()

        if concurrency in ("parallel", "debug"):
            start_time_parallel = time.time()

            cpu_count = psutil.cpu_count()
            max_n_cpus = get_config().getint("debugging", "max_n_cpus")
            if max_n_cpus > cpu_count:
                logger.warning(
                    "max_n_cpus is set in config to {}, but system is limited to {} cpus. "
                    "Resetting max_n_cpus to {}.".format(
                        max_n_cpus, cpu_count, cpu_count
                    )
                )
                max_n_cpus = cpu_count
            len_gen_results = len(gen_results)

            min_batchsize_for_new_process = get_config().getint(
                "debugging", "min_batchsize_for_new_process"
            )
            n_processes = min(
                math.ceil(len_gen_results / min_batchsize_for_new_process), max_n_cpus
            )

            batchsize = math.ceil(len_gen_results / n_processes)
            batches = (
                gen_results.iloc[x * batchsize : x * batchsize + batchsize, :]
                for x in list(range(n_processes))
            )
            logging.info(
                "[ ] Starting parallel means assignment with {} processes".format(
                    n_processes
                )
            )
            with ProcessPool(nodes=n_processes) as pool:

                for rule_result, counters, rules_count_batch in pool.map(
                    functools.partial(
                        self._calc_mode_assigned_flows,
                        self.context,
                        self.rules,
                        row_to_flow,
                    ),
                    batches,
                ):
                    rule_results += rule_result
                    rules_count.update(rules_count_batch)
                    for key in counters.keys():
                        self.counters[key].update(counters[key])
            for nst, counter in self.counters.items():
                if len(counter) > 0:
                    counter_sorted = OrderedDict(
                        {k: v for k, v in sorted(dict(counter).items())}
                    )
                    logging.info(f"    Counted flags for {nst}: {counter_sorted}")
            running_time_parallel = time.time() - start_time_parallel
            logging.info(
                "[X] Parallel means assignment finished after {:.2f} seconds. ".format(
                    running_time_parallel
                )
            )
            logging.info(f"    Applied rules: {dict(rules_count)}")

        elif concurrency in ("single_threaded", "debug"):
            logging.info(f"[ ] Starting single threaded means assignment ({mode}).")
            logging.info(f"    Registered rules: {[x.__name__ for x in self.rules]}")
            start_time_single = time.time()
            twenty_percent = round(len(gen_results) / 5)
            for i, row in enumerate(gen_results.itertuples(index=False)):
                flow = row_to_flow(row)
                if i % twenty_percent == 0 and i > 0 and len(gen_results) > 50000:
                    logging.info(
                        f"    Finished means assignment for "
                        f"{i / twenty_percent * 20:.0f}% of the total flows."
                    )
                    logging.info(f"    Currently working on flows like {flow}")
                for rule_class in self.rules:
                    rule = rule_class(self.context)
                    if rule.condition(flow):
                        rule.action(flow)
                        self.counters[flow.commodity].update(rule.counter)
                        rule_results += flow.get_leaves(flow)
                        rules_count += Counter({rule_class.__name__: 1})
                        break
                else:
                    flows_without_rule.append(flow)
            for nst, counter in self.counters.items():
                if len(counter) > 0:
                    counter_sorted = OrderedDict(
                        {k: v for k, v in sorted(dict(counter).items())}
                    )
                    logging.info(f"    Counted flags for {nst}: {counter_sorted}")
            running_time_single = time.time() - start_time_single
            logging.info(
                f"[X] Single threaded means assignment finished after "
                f"{running_time_single:.2f} seconds."
            )
            logging.info(f"    Applied rules: {dict(rules_count)}")

        if len(flows_without_rule) > 0:
            logger.warning(
                f"There are {len(flows_without_rule)} flows for which no "
                f"means assignment rule could be fitted. The first 5 are: "
                f"{flows_without_rule[:5]}"
            )

        self._write_results_to_db(rule_results, mode)

        return rule_results

    @staticmethod
    def _write_results_to_db(
        rule_results: List[Tuple[int, int, str, float, str, int, str, str, str]],
        mode: str,
    ):
        conn = get_db_connection()
        cur = create_cursor(conn)

        if mode == "normal":
            logging.info(
                f"[ ] Starting to write {len(rule_results)} flows to database table "
                f"'{TABLES['meansflows']}'."
            )
            execute_values(
                cur,
                f"INSERT INTO {TABLES['meansflows']} (f, t, commodity, qty, means, source, sink) "
                "VALUES %s",
                (x[:7] for x in rule_results),
                page_size=1000,
            )
        elif mode == "parcels":
            logging.info(
                f"[ ] Starting to write {len(rule_results)} flows to database table "
                f"'{TABLES['parcelflows']}'."
            )
            execute_values(
                cur,
                f"INSERT INTO {TABLES['parcelflows']} (hub_zone_id, target_zone, commodity, parcels, "
                "means, source, sink, company, hub_name, ptype) VALUES %s",
                rule_results,
                page_size=1000,
            )

        conn.commit()
        cur.close()
        logging.info("[X] Finished database export.")

    @staticmethod
    def _calc_mode_assigned_flows(context, rules, row_to_flow, batch_of_flows):
        result_flows = []
        counters = defaultdict(Counter)
        rules_count = Counter()

        logging.debug(
            f"    Batch of length {len(batch_of_flows)} started "
            f"(PID {current_process().pid})."
        )
        for flow_tuple in batch_of_flows.itertuples(index=False):
            flow = row_to_flow(flow_tuple)
            for rule_class in rules:
                if not hasattr(rule_class, "name"):
                    rule_class.name = rule_class.__name__
                rule = rule_class(context)
                if rule.condition(flow):
                    rule.action(flow)
                    counters[flow.commodity].update(rule.counter)
                    rules_count += Counter({rule_class.__name__: 1})
                    leaves = flow.get_leaves(flow)
                    result_flows += leaves
                    break
        logging.info(
            f"    Batch of length {len(batch_of_flows)} finished (PID "
            f"{current_process().pid})."
        )
        return result_flows, counters, rules_count
