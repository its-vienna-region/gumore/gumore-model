"""
The `gen` module is responsible for the generation of flows of commodities between
the model zones. This is done by creating production and attraction values for all
zones with multiple regression models for each NST group. The results are used as
input for a gravitational traffic model.
"""

from abc import ABC, abstractmethod
from copy import deepcopy
import functools
import time
from typing import Dict, Iterable, List, Tuple, Type

import numpy as np
import pandas as pd
from psycopg2.extras import execute_values

import gumore.core.entities
import gumore.core.graph
from gumore.core.hotspot import HotspotSet
from gumore.core.transportproblem import (
    run_tpp_column_min,
    run_tpp_matrix_min,
    run_tpp_row_min,
    run_tpp_vogel_approx,
)
from gumore.util.conf import get_config, is_scenario, scenario_name, get_string_resource
from gumore.util.filedata import (
    load_cordon_relations,
    load_empirical_flow_data,
    load_regional_sums_for_structural_data,
)
from gumore.util import global_vars
from gumore.util.database import (
    create_cursor,
    get_active_schema,
    get_db_connection,
    get_engine,
    TABLES,
)
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


class ExternalFlowGen:
    def __init__(self, graph: gumore.core.graph.Graph, gens):
        """
        This class handles the calculation of the external flows.

        The model for the external flows is directly based on empirical flows. The main
        task of `ExternalFlowGen` is to map the international flows to cordon zones.
        Args:
            graph (`gumore.core.graph.Graph`):
            gens (`List[gumore.core.gen.GumoreGen]`:
        """
        self.graph: gumore.core.graph.Graph = graph
        self.gens: List[GumoreGen] = list(gens)
        self.flows_nsts_emp, _ = load_empirical_flow_data("external")
        self.regional_sums_nuts2 = load_regional_sums_for_structural_data(level="nuts2")
        self.input_relations, self.output_relations = load_cordon_relations()
        self.input_relations_expanded = self._expand_cordon_relations(
            self.input_relations
        ).fillna("")
        self.output_relations_expanded = self._expand_cordon_relations(
            self.output_relations
        ).fillna("")

    def execute(self) -> List[Tuple[int, int, str, float]]:
        result_long_all_gens = pd.DataFrame()
        for gen in self.gens:
            logging.info(
                f"    Starting external flow generation for {gen.commodity.code}."
            )
            export = self._calc_export(gen)
            impo = self._calc_import(gen)
            matrix = self._calc_matrix(export, impo, gen)
            df_export = self._calc_export_matrix(matrix)
            df_import = self._calc_import_matrix(matrix)
            df_transit = self._calc_transit_matrix(matrix)
            result = df_export + df_import + df_transit
            self._validate_result(result, gen)
            self._write_result_to_csv(result, gen)
            result_long = self._transform_result_to_long_table_format(result, gen)
            result_long_all_gens = pd.concat([result_long_all_gens, result_long])
        return list(result_long_all_gens.itertuples(index=False))

    def _get_zones_codes(self) -> List[str]:
        return sorted(
            self.graph.zones.df[
                self.graph.zones.df.type.isin(
                    [
                        get_string_resource("zone_type_name_planning_region"),
                        get_string_resource("zone_type_name_survey_region"),
                    ]
                )
            ].code
        )

    def _get_df_out_index(self) -> List[str]:
        zones_codes = self._get_zones_codes()
        cordon_names = sorted(
            self.graph.zones.df[
                self.graph.zones.df.type == get_string_resource("zone_type_name_cordon")
            ].name
        )  # type: ignore
        return zones_codes + cordon_names

    def _calc_export(self, gen):
        nuts2_prod = self._calc_prod_or_att(gen, self.regional_sums_nuts2, "prod")
        zones_prod = self._calc_prod_or_att(
            gen,
            self.graph.zones.df.set_index("code"),
            "prod",
            get_config().get("graph", "zones_region_column_name"),
        )
        zones_prod.rename(
            {get_config().get("graph", "zones_region_column_name"): "nuts2"},
            axis=1,
            inplace=True,
        )
        prod = zones_prod.merge(
            nuts2_prod["prod"],
            how="left",
            left_on="nuts2",
            right_index=True,
            suffixes=("", "_nuts2"),
        )
        prod["factor"] = prod["prod"] / prod["prod_nuts2"]
        prod = prod[["nuts2", "factor"]].copy()
        export = prod.merge(
            self.flows_nsts_emp[int(gen.commodity.id)],
            left_on="nuts2",
            right_index=True,
        )
        export = export.iloc[:, 2:].multiply(export.factor, axis=0)
        return export

    def _calc_import(self, gen):
        nuts2_att = self._calc_prod_or_att(gen, self.regional_sums_nuts2, "att")
        zones_att = self._calc_prod_or_att(
            gen,
            self.graph.zones.df.set_index("code"),
            "att",
            get_config().get("graph", "zones_region_column_name"),
        )
        zones_att.rename(
            columns={get_config().get("graph", "zones_region_column_name"): "nuts2"},
            inplace=True,
        )
        att = zones_att.merge(
            nuts2_att["att"],
            how="left",
            left_on="nuts2",
            right_index=True,
            suffixes=("", "_nuts2"),
        )
        att["factor"] = att["att"] / att["att_nuts2"]
        att = att[["nuts2", "factor"]].copy()
        impo = att.merge(
            self.flows_nsts_emp[int(gen.commodity.id)].T,
            left_on="nuts2",
            right_index=True,
        )
        impo = impo.iloc[:, 2:].multiply(impo.factor, axis=0)
        return impo

    def _calc_matrix(self, export, impo, gen):
        matrix = pd.concat([self.flows_nsts_emp[int(gen.commodity.id)], export], axis=0)
        matrix = pd.concat(
            [matrix.reset_index(), impo.T.reset_index(drop=True)], axis=1
        )
        matrix = matrix.set_index("index")
        matrix.drop(
            index=self.graph.zones.df.loc[
                self.graph.zones.df["type"]
                == get_string_resource("zone_type_name_planning_region"),
                get_config().get("graph", "zones_region_column_name"),
            ]
            .unique()
            .tolist(),
            inplace=True,
        )
        matrix.drop(
            columns=self.graph.zones.df.loc[
                self.graph.zones.df["type"]
                == get_string_resource("zone_type_name_planning_region"),
                get_config().get("graph", "zones_region_column_name"),
            ]
            .unique()
            .tolist(),
            inplace=True,
        )
        matrix.fillna(0, inplace=True)
        matrix = matrix.sort_index()
        matrix = matrix.sort_index(axis=1)
        return matrix

    def _calc_export_matrix(self, matrix):
        df_export = pd.DataFrame(
            0,
            index=self._get_df_out_index(),
            columns=self._get_df_out_index(),  # type: ignore
        )
        for i_row, row in matrix.loc[
            matrix.columns.isin(self._get_zones_codes()),
            ~matrix.columns.isin(self._get_zones_codes()),
        ].iterrows():
            for i_col, value in row.items():
                cordon_name = self.output_relations_expanded.at[i_row, i_col]
                if cordon_name and value:
                    df_export.at[i_row, cordon_name] += value
        return df_export

    def _calc_import_matrix(self, matrix):
        df_import = pd.DataFrame(
            0,
            index=self._get_df_out_index(),
            columns=self._get_df_out_index(),  # type: ignore
        )
        for i_row, row in matrix.loc[
            ~matrix.columns.isin(self._get_zones_codes()),
            matrix.columns.isin(self._get_zones_codes()),
        ].iterrows():
            for i_col, value in row.items():
                cordon_name = self.input_relations_expanded.at[i_row, i_col]
                if cordon_name and value:
                    df_import.at[cordon_name, i_col] += value
        return df_import

    def _calc_transit_matrix(self, matrix):
        df_transit = pd.DataFrame(
            0,
            index=self._get_df_out_index(),
            columns=self._get_df_out_index(),  # type: ignore
        )
        for i_row, row in matrix.loc[
            ~matrix.columns.isin(self._get_zones_codes()),
            ~matrix.columns.isin(self._get_zones_codes()),
        ].iterrows():
            for i_col, value in row.items():
                cordon_name_into_zones = self.input_relations_expanded.at[i_row, i_col]
                cordon_name_out_of_zones = self.output_relations_expanded.at[
                    i_row, i_col
                ]
                if cordon_name_into_zones and cordon_name_out_of_zones and value:
                    df_transit.at[
                        cordon_name_into_zones, cordon_name_out_of_zones
                    ] += value
        return df_transit

    @staticmethod
    def _write_result_to_csv(result, gen):
        result = result.copy()
        cordon_name_to_id = {
            "Bratislava": "EK06",
            "Drasenhofen": "EK05",
            "Graz-West": "EK01",
            "Kleinhaugsdorf": "EK12",
            "Klingenbach/Oedenburg": "EK08",
            "Neunagelberg": "EK04",
            "Nickelsdorf": "EK07",
            "Rattersdorf": "EK09",
            "Schachendorf": "EK10",
            "Semmering": "EK11",
            "Westautobahn": "EK02",
            "Wullowitz": "EK03",
        }
        result.set_index(
            pd.Index(
                list(result.index[:-11])
                + [cordon_name_to_id[x] + " " + x for x in result.index[-11:]]
            ),
            inplace=True,
        )
        result.columns = list(
            list(result.columns[:-11])
            + [cordon_name_to_id[x] + " " + x for x in result.columns[-11:]]
        )
        result = result.sort_index()
        result = result.sort_index(axis=1)
        result.to_csv(
            get_config().get("filedata", "export_path_external_flow_results")
            + "_"
            + gen.commodity.code
            + ".csv",
            sep=";",
            decimal=",",
        )

    def _transform_result_to_long_table_format(
        self, result: pd.DataFrame, gen
    ) -> pd.DataFrame:
        result_long = result.copy().reset_index().melt(id_vars="index")
        result_long = result_long[result_long.value > 0]
        result_long["nst"] = gen.commodity.code
        result_long.columns = ["vb_from_id", "vb_to_id", "flow", "nst"]
        result_long = result_long[["vb_from_id", "vb_to_id", "nst", "flow"]]
        cordon_name_to_id = {
            row.name: row.code
            for row in self.graph.zones.df[
                self.graph.zones.df.type == get_string_resource("zone_type_name_cordon")
            ][["name", "code"]].itertuples()
        }
        result_long.vb_from_id = result_long.vb_from_id.apply(
            lambda x: cordon_name_to_id[x] if x in cordon_name_to_id.keys() else x
        )
        result_long.vb_to_id = result_long.vb_to_id.apply(
            lambda x: cordon_name_to_id[x] if x in cordon_name_to_id.keys() else x
        )
        zone_code_to_id = {
            code: index for index, code in self.graph.zones.df.code.iteritems()
        }
        result_long.vb_from_id = result_long.vb_from_id.apply(
            lambda x: zone_code_to_id[x]
        )
        result_long.vb_to_id = result_long.vb_to_id.apply(lambda x: zone_code_to_id[x])
        return result_long

    @staticmethod
    def _calc_prod_or_att(
        gen, df_regions: pd.DataFrame, prod_or_att: str, region_column_name: str = None
    ) -> pd.DataFrame:
        cols = [x.lower() for x in list(gen.coefs[prod_or_att].keys())]
        df = df_regions.loc[
            :, cols + ([region_column_name] if region_column_name else [])
        ]
        df_res = pd.DataFrame()

        for k, v in zip(cols, gen.coefs[prod_or_att].values()):
            df.loc[:, k + "_coef"] = v
            df_res.loc[:, k + "_res"] = df[k] * df[k + "_coef"]
        df[prod_or_att] = df_res.sum(axis=1)
        df["commodity"] = str(gen.commodity)
        return df

    def _expand_cordon_relations(self, df_relations: pd.DataFrame) -> pd.DataFrame:

        df = df_relations.copy()
        zones = self.graph.zones.df

        nuts3_regions_zones = np.sort(
            zones[
                zones.type.isin(
                    (
                        get_string_resource("zone_type_name_planning_region"),
                        get_string_resource("zone_type_name_survey_region"),
                    )
                )
            ].nuts3_code.unique()
        )

        zones_codes_for_nuts3_region = {
            nuts3_region: list(zones[zones.nuts3_code == nuts3_region].code)
            for nuts3_region in nuts3_regions_zones
        }

        for nuts3_region in nuts3_regions_zones:
            row_old = df.loc[nuts3_region, :]
            rows_new = pd.DataFrame(row_old).T
            rows_new = pd.concat(
                [rows_new] * len(zones_codes_for_nuts3_region[nuts3_region])
            )
            rows_new.set_index(
                pd.Index(zones_codes_for_nuts3_region[nuts3_region]), inplace=True
            )
            rows_new.index.name = "from_cell"
            df = pd.concat([df, rows_new], axis=0)
            df.drop(index=nuts3_region, inplace=True)

            col_old = df.loc[:, nuts3_region]
            cols_new = pd.DataFrame(col_old)
            cols_new = pd.concat(
                [cols_new] * len(zones_codes_for_nuts3_region[nuts3_region]), axis=1
            )
            cols_new.columns = zones_codes_for_nuts3_region[nuts3_region]
            cols_new.columns.name = "to_cell"
            df = pd.concat([df, cols_new], axis=1)
            df.drop(columns=nuts3_region, inplace=True)
        df = df.sort_index().sort_index(axis=1)
        return df

    def _validate_export(self):
        pass

    def _validate_matrix(self):
        pass
        # code_to_nuts2 = {"1": "AT11",
        #                  "3": "AT12",
        #                  "4": "AT31",
        #                  "6": "AT22",
        #                  "9": "AT13"}
        # row_sums = matrix.groupby(matrix.index.str.slice(0, 1)).sum()
        # row_sums = row_sums.loc[row_sums.index.isin(list(code_to_nuts2.keys()))]
        # row_sums["nuts2"] = [code_to_nuts2[x] for x in row_sums.index]
        # row_sums.set_index("nuts2")

    def _validate_import(self):
        pass

    def _validate_transit(self):
        pass

    def _validate_result(self, result, gen):

        code_to_nuts2 = {
            "1": "AT11",
            "3": "AT12",
            "4": "AT31",
            "6": "AT22",
            "9": "AT13",
        }

        return


class GumoreGen(ABC):
    # noinspection PyUnresolvedReferences
    """
        This is an abstract base class for the definition of individual commodity generation
        rules.
        Example of a concrete `GumoreGen` class::
            class GenNST01(GumoreGen):
                commodity = com_nst01
                coefs = {"prod": {"A": 179.08}, "att": {"A": 175.47}}
                alpha = 1.75

        Attributes:
            commodity (`gumore.core.entities.Commodity`): The commodity which is generated
                by this class.
            coefs (Dict[str, Dict[str, float]]): the regression model's coefficients for
                attraction and production. Keys of the outer dictionary need to be either
                "prod" or "att". The inner dictionaries need to have the names of the
                independent variables as keys and the coefficients itselfs as values.
                Example::
                    {"prod": {"A": 179.08}, "att": {"A": 175.47}}
            alpha (float): Gravity Model: The alpha exponent.
            beta (float): Gravity Model: The beta exponent. Defaults to 1.0
            f (float): Gravity Model: The f factor. Defaults to 1.0
            change_cost_limit (float): Gravity Model: Costs between two zones (taken from
                the distance matrix) which are below this limit are set to the limit value.
                Defaults to 0.01
            change_cost_constant (float): Gravity Model: This value is added to the costs
                between two zones (taken from the distance matrix). Defaults to 0.0
            calibrate (bool): Gravity Model: Whether to calibrate the calculated flows
                with regional empirical flows. Defaults to `True`
            balance (bool): Gravity Model: Whether to balance the flows using marginal
                sums from the production / attraction regression models. Defaults to
                `True`
            distmat_name (str): The name of the distance matrix. Defaults to "hgv_hours"

        Args:
            graph (´gumore.core.graph.Graph´): The graph object holding context
                information.

        """

    beta: float = 1.0
    f: float = 1.0
    change_cost_limit: float = 0.01
    change_cost_constant: float = 0.0
    apply_hotspots: bool = True
    calibrate: bool = True
    flow_distribution_mode: str = "gravity_model"
    balance: bool = True
    distmat_name: str = "hgv_hours"

    @property
    @abstractmethod
    def commodity(self) -> gumore.core.entities.Commodity:
        return self.commodity

    @property
    @abstractmethod
    def coefs(self) -> Dict[str, Dict[str, float]]:
        return self.coefs

    @property
    @abstractmethod
    def alpha(self) -> float:
        return self.alpha

    def __init__(self, graph: gumore.core.graph.Graph) -> None:
        assert self.flow_distribution_mode in (
            "gravity_model",
            "tpp_column_min",
            "tpp_row_min",
            "tpp_matrix_min",
            "tpp_vogel_approx",
        )
        self.graph: gumore.core.graph.Graph = graph

    def __str__(self) -> str:
        return self.__class__.__name__


class GumoreGenSet:
    """
    The logic for registering and executing the ``GumoreGen`` commodity flow generation
    rules.

    Args:
        graph (gumore.core.graph.Graph): The graph object holding context
            information.

    Attributes:
        graph (gumore.core.graph.Graph): The graph object holding context
            information.
        generators (List[Type[GumoreGen]]): The registered ``GumoreGen`' classes.
            Defaults to []
    """

    def __init__(
        self,
        graph: gumore.core.graph.Graph,
        regional_sums_for_structural_data: pd.DataFrame,
    ) -> None:
        self.graph: gumore.core.graph.Graph = graph
        self.regional_sums_for_structural_data = regional_sums_for_structural_data
        self.generators: List[Type[GumoreGen]] = []

    def register(self, genclass: Type[GumoreGen]) -> None:
        """
        Registers a ``GumoreGen`` class.

        All registered classes are executed in the order of their registration.

        Args:
            genclass(Type[GumoreGen]): The ``GumoreGen`` class to register
        """
        self.generators.append(genclass)
        logger.debug(
            f"Generation nr {len(self.generators)} <{genclass.__name__}> registered"
        )

    def execute(self) -> pd.DataFrame:
        """
        Executes the registered ``GumoreGen`` rules in the order of their registration.

        Returns:
            List[Tuple[int, int, str, float]]: The generation results. List of tuples
            (from_zone, to_zone, commodity, quantity).
        """
        gens: List[GumoreGen] = [gen_class(self.graph) for gen_class in self.generators]

        if get_config().getboolean("model_run", "perform_internal_flow_generation"):
            flows_nsts_emp_rescaled = self._rescale_empirical_data()
            gen_results_internal = self._execute_internal_flow_generation(
                gens, flows_nsts_emp_rescaled
            )
        else:
            gen_results_internal = []

        if get_config().getboolean("model_run", "perform_external_flow_generation"):
            gen_results_external = self._execute_external_flow_generation(gens)
        else:
            gen_results_external = []

        gen_results = gen_results_internal + gen_results_external

        self._write_results_to_db(gen_results)

        gen_results_df = pd.DataFrame(
            gen_results, columns=("f", "t", "commodity", "qty")
        )

        return gen_results_df

    def _rescale_empirical_data(self) -> Dict[int, pd.DataFrame]:
        flows_nsts_emp, _ = load_empirical_flow_data(internal_or_external="internal")
        # regional_sums_full = load_regional_sums_for_structural_data()
        regional_sums_zones = self._calc_regional_sums_for_structural_data_of_zones()
        fractions = self._calc_structural_data_fractions(
            self.regional_sums_for_structural_data, regional_sums_zones
        )
        print(fractions)

        flows_rescaled = {}
        for gen in self.generators:
            nst = int(gen.commodity.id)

            prod_vars = [x.lower() for x in gen.coefs["prod"].keys()]
            prod_coefs = list(gen.coefs["prod"].values())
            prod_coefs_sum = sum(prod_coefs)
            prod_fractions = fractions.loc[:, prod_vars]
            prod_fractions = prod_fractions * prod_coefs
            prod_fraction = prod_fractions.sum(axis=1) / prod_coefs_sum

            att_vars = [x.lower() for x in gen.coefs["att"].keys()]
            att_coefs = list(gen.coefs["att"].values())
            att_coefs_sum = sum(att_coefs)
            att_fractions = fractions.loc[:, att_vars]
            att_fractions = att_fractions * att_coefs
            att_fraction = att_fractions.sum(axis=1) / att_coefs_sum

            print(flows_nsts_emp)

            flows_rescaled[nst] = flows_nsts_emp[nst].mul(prod_fraction, axis=0)
            flows_rescaled[nst] = flows_rescaled[nst].mul(att_fraction, axis=1)
        return flows_rescaled

    def _execute_external_flow_generation(
        self, gens: List[GumoreGen]
    ) -> List[Tuple[int, int, str, float]]:
        logging.info(
            f"[ ] Starting external flow generation for {len(gens)} NST groups"
        )
        start_time_single = time.time()
        external_flow_gen: ExternalFlowGen = ExternalFlowGen(self.graph, gens)
        result = external_flow_gen.execute()
        running_time = time.time() - start_time_single
        logging.info(
            f"[X] External flow generation for {len(gens)} NST groups "
            f"finished after {running_time:.2f} seconds."
        )
        return result

    def _execute_internal_flow_generation(
        self, gens: List[GumoreGen], flows_nsts_emp: Dict[int, pd.DataFrame]
    ) -> List[Tuple[int, int, str, float]]:
        logging.info(
            "[ ] Starting flow generation for {} zones and {} "
            "generation rules.".format(len(self.graph.zones.zone_ids), len(gens))
        )
        start_time_single = time.time()
        _calc_gendist_for_graph = functools.partial(
            self._calc_gendist, graph=self.graph, flows_nsts_emp=flows_nsts_emp
        )
        result = map(_calc_gendist_for_graph, gens)
        result = [x for y in result for x in y]
        running_time = time.time() - start_time_single
        logging.info(
            "[X] Flow generation for {} zones and {} generation "
            "rules finished after {:.2f} seconds.".format(
                len(self.graph.zones.zone_ids), len(gens), running_time
            )
        )
        return result

    def _calc_regional_sums_for_structural_data_of_zones(self):
        conf = get_config()
        df_zones = self.graph.zones.df
        region_col = conf.get("graph", "zones_region_column_name")
        cols = conf.get("graph", "zones_structural_data_columns").split(",") + [
            region_col
        ]
        df_zones = df_zones[cols]
        return df_zones.groupby(region_col).sum()

    @staticmethod
    def _calc_structural_data_fractions(
        regional_sums_full: pd.DataFrame, regional_sums_zones: pd.DataFrame
    ) -> pd.DataFrame:
        fractions = regional_sums_zones / regional_sums_full
        return fractions.dropna()

    @staticmethod
    def _write_results_to_db(gen_results: List[Tuple[int, int, str, float]]) -> None:
        """
        Writes the generated flows to the database (table ``rawflows``).

        Args:
            gen_results (List[Tuple[int, int, str, float]]): The generation results.
                List of tuples (from_zone, to_zone, commodity, quantity).
        """
        logging.info(
            f"[ ] Starting to write {len(gen_results)} flows to database table "
            f"'{TABLES['rawflows']}'."
        )
        conn = get_db_connection()
        cur = create_cursor(conn)
        # noinspection SqlResolve
        execute_values(
            cur,
            f"INSERT INTO {TABLES['rawflows']} (f, t, commodity, qty) VALUES %s",
            gen_results,
            page_size=1000,
        )
        conn.commit()
        cur.close()
        logging.info("[X] Finished database export.")

    @staticmethod
    def _calc_gendist(
        gen: GumoreGen,
        graph: gumore.core.graph.Graph,
        flows_nsts_emp: Dict[int, pd.DataFrame],
    ) -> Iterable[Tuple[int, int, str, float]]:
        """

        Args:
            gen (GumoreGen):
            graph (gumore.core.graph.Graph):
            flows_nsts_emp (Dict[int, pd.DataFrame]):

        Returns:
            Iterable[Tuple[int, int, str, float]]: The generation results. List of
            tuples (from_zone, to_zone, commodity, quantity).
        """

        """[(50, 50, 'nst02', 303833.3426632384), ...]"""

        logging.info(
            f"    Starting flow generation with rule {gen}. "
            f"Calibrate production and attraction: {gen.calibrate}. "
            f"Hot spot calculation: {gen.apply_hotspots}. "
            f"Flow distribution mode: {gen.flow_distribution_mode}. "
            f"Balance flows: {gen.balance}."
        )

        # Calculate Production and Attraction
        df_prod = GumoreGenSet._calc_prod_or_att(gen, graph, "prod")
        df_att = GumoreGenSet._calc_prod_or_att(gen, graph, "att")

        # Apply hotspots
        if gen.apply_hotspots:
            hotspots = HotspotSet(
                df_prod=df_prod,
                df_att=df_att,
                size_to_factor=global_vars.size_to_factor,
                graph=graph,
            )
            df_prod, df_att = hotspots.execute()

        # Store it to database
        df_prod[["commodity", "prod"]].to_sql(
            "prod", get_engine(), schema=get_active_schema(), if_exists="append"
        )
        df_att[["commodity", "att"]].to_sql(
            "att", get_engine(), schema=get_active_schema(), if_exists="append"
        )

        # Calibrate Production and Attraction and store it to database
        if gen.calibrate:
            commodity_id = int(gen.commodity.id)
            df_prod_cal, df_att_cal = GumoreGenSet._calibrate_prod_and_att(
                df_prod, df_att, flows_nsts_emp, commodity_id
            )
        else:
            df_prod_cal = df_prod
            df_att_cal = df_att
        df_prod_cal[["commodity", "prod"]].to_sql(
            "prod_cal", get_engine(), schema=get_active_schema(), if_exists="append"
        )
        df_att_cal[["commodity", "att"]].to_sql(
            "att_cal", get_engine(), schema=get_active_schema(), if_exists="append"
        )

        # Prepare input data for gravity model
        if gen.calibrate:
            X_gravity = GumoreGenSet._prepare_X_gravity(
                gen, graph, df_prod_cal, df_att_cal
            )
        else:
            X_gravity = GumoreGenSet._prepare_X_gravity(gen, graph, df_prod, df_att)

        if gen.flow_distribution_mode == "gravity_model":
            flows = GumoreGenSet._run_gravity_model(X_gravity, gen)
        elif gen.flow_distribution_mode == "tpp_column_min":
            flows = run_tpp_column_min(gen, graph, df_prod_cal, df_att_cal)
        elif gen.flow_distribution_mode == "tpp_row_min":
            flows = run_tpp_row_min(gen, graph, df_prod_cal, df_att_cal)
        elif gen.flow_distribution_mode == "tpp_matrix_min":
            flows = run_tpp_matrix_min(gen, graph, df_prod_cal, df_att_cal)
        elif gen.flow_distribution_mode == "tpp_vogel_approx":
            flows = run_tpp_vogel_approx(gen, graph, df_prod_cal, df_att_cal)

        # return empty list if no flows are generatedS
        if len(flows) == 0:
            return []

        # Balance flows
        if gen.balance:
            flows_balanced, _ = GumoreGenSet._balance_matrix(flows)
        else:
            flows_balanced = flows

        # Add NST ID
        flows_balanced.loc[:, "nst"] = gen.commodity.code
        flows_balanced = flows_balanced[["vb_from_id", "vb_to_id", "nst", "flow"]]
        flows_balanced = flows_balanced[flows_balanced["flow"] > 0]

        # Use flow_multiplier for debugging
        flow_multiplier = get_config().getfloat("debugging", "flow_multiplier")
        if flow_multiplier != 1.0:
            logging.info(f"    DEBUG: Applying flow_multiplier {flow_multiplier}.")
            flows_balanced["flow"] = flows_balanced["flow"] * flow_multiplier

        return flows_balanced.itertuples(index=False)

    @staticmethod
    def _calc_prod_or_att(
        gen: GumoreGen, graph: gumore.core.graph.Graph, prod_or_att: str
    ) -> pd.DataFrame:
        df_zones = graph.zones.df
        cols = [x.lower() for x in list(gen.coefs[prod_or_att].keys())]
        region_column_name = get_config().get("graph", "zones_region_column_name")
        df = df_zones.loc[:, [region_column_name] + cols]
        df_res = pd.DataFrame()

        for k, v in zip(cols, gen.coefs[prod_or_att].values()):
            df.loc[:, k + "_coef"] = v
            df_res.loc[:, k + "_res"] = df[k] * df[k + "_coef"]
        df[prod_or_att] = df_res.sum(axis=1)
        df["commodity"] = str(gen.commodity)
        return df

    @staticmethod
    def _calibrate_prod_and_att(
        df_prod: pd.DataFrame,
        df_att: pd.DataFrame,
        flows_nsts_emp: Dict[int, pd.DataFrame],
        commodity_id: int,
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        conf = gumore.util.conf.get_config()
        cell_names = conf.get("filedata", "flows_emp_cell_names").split(",")
        if is_scenario():
            if conf.getboolean(
                scenario_name(), "scale_flows_with_empirical_baseline_data"
            ):
                df_comp_reg_emp_bund = pd.read_sql_table(
                    "comp_reg_emp_bund_baseline", get_engine(), schema="baseline"
                )
            else:
                df_comp_reg_emp_bund = GumoreGenSet._comp_reg_emp_bundesl(
                    df_prod, df_att, flows_nsts_emp, cell_names, commodity_id
                )
                df_comp_reg_emp_bund.to_sql(
                    "comp_reg_emp_bund_baseline",
                    get_engine(),
                    schema=scenario_name(),
                    if_exists="append",
                )
        else:
            df_comp_reg_emp_bund = GumoreGenSet._comp_reg_emp_bundesl(
                df_prod, df_att, flows_nsts_emp, cell_names, commodity_id
            )
            df_comp_reg_emp_bund.to_sql(
                "comp_reg_emp_bund_baseline",
                get_engine(),
                schema="baseline",
                if_exists="append",
            )

        (
            df_prod_cal,
            df_att_cal,
        ) = GumoreGenSet._calibrate_regression_results_to_empirical_sums_on_bundesl(
            df_prod, df_att, df_comp_reg_emp_bund, cell_names, commodity_id
        )
        return df_prod_cal, df_att_cal

    @staticmethod
    def _comp_reg_emp_bundesl(
        df_prod: pd.DataFrame,
        df_att: pd.DataFrame,
        flows_nsts_emp: Dict[int, pd.DataFrame],
        cell_names: List[str],
        commodity_id: int,
    ) -> pd.DataFrame:
        df = pd.DataFrame(
            columns=[
                "nst",
                "bundesl_code",
                "pred_prod_sum",
                "pred_att_sum",
                "emp_prod_sum",
                "emp_att_sum",
            ]
        )
        region_column_name = get_config().get("graph", "zones_region_column_name")
        for cell_name in cell_names:
            pred_prod_sum = df_prod[df_prod[region_column_name] == cell_name][
                "prod"
            ].sum()
            pred_att_sum = df_att[df_att[region_column_name] == cell_name]["att"].sum()
            emp_prod_sum = flows_nsts_emp[commodity_id].loc[cell_name, :].sum()
            emp_att_sum = flows_nsts_emp[commodity_id].loc[:, cell_name].sum()
            temp = {
                "nst": commodity_id,
                "bundesl_code": cell_name,
                "pred_prod_sum": pred_prod_sum,
                "pred_att_sum": pred_att_sum,
                "emp_prod_sum": emp_prod_sum,
                "emp_att_sum": emp_att_sum,
            }
            df = df.append(temp, ignore_index=True)
            df["PROD_pred_div_emp"] = df.pred_prod_sum / df.emp_prod_sum
            df["ATT_pred_div_emp"] = df.pred_att_sum / df.emp_att_sum
            df["TOTAL_pred_div_emp"] = (df.pred_prod_sum + df.pred_att_sum) / (
                df.emp_prod_sum + df.emp_att_sum
            )
            df = df[
                [
                    "nst",
                    "bundesl_code",
                    "TOTAL_pred_div_emp",
                    "PROD_pred_div_emp",
                    "pred_prod_sum",
                    "emp_prod_sum",
                    "ATT_pred_div_emp",
                    "pred_att_sum",
                    "emp_att_sum",
                ]
            ]
        if get_config().getboolean("debugging", "read_zones_sample_table"):
            df.fillna(0, inplace=True)
        return df.sort_values("nst")

    @staticmethod
    def _calibrate_regression_results_to_empirical_sums_on_bundesl(
        df_prod: pd.DataFrame,
        df_att: pd.DataFrame,
        comp_reg_emp_bundesl: pd.DataFrame,
        cell_names: Iterable[str],
        commodity_id: int,
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        df_prod_cal = deepcopy(df_prod)
        df_att_cal = deepcopy(df_att)
        region_column_name = get_config().get("graph", "zones_region_column_name")
        for cell_name in cell_names:
            f_prod = comp_reg_emp_bundesl[
                (comp_reg_emp_bundesl.bundesl_code == cell_name)
                & (comp_reg_emp_bundesl.nst == commodity_id)
            ]["PROD_pred_div_emp"].values[0]
            df_prod_cal.loc[df_prod_cal[region_column_name] == cell_name, "prod"] = (
                df_prod_cal.loc[df_prod_cal[region_column_name] == cell_name, "prod"]
                / f_prod
            )

            f_att = comp_reg_emp_bundesl[
                (comp_reg_emp_bundesl.bundesl_code == cell_name)
                & (comp_reg_emp_bundesl.nst == commodity_id)
            ]["ATT_pred_div_emp"].values[0]
            df_att_cal.loc[df_att_cal[region_column_name] == cell_name, "att"] = (
                df_att_cal.loc[df_att_cal[region_column_name] == cell_name, "att"]
                / f_att
            )
        return df_prod_cal, df_att_cal

    @staticmethod
    def _prepare_X_gravity(
        gen: GumoreGen,
        graph: gumore.core.graph.Graph,
        df_prod: pd.DataFrame,
        df_att: pd.DataFrame,
    ) -> pd.DataFrame:
        X_gravity_raw = graph.distmats[gen.distmat_name].df_long
        region_column_name = get_config().get("graph", "zones_region_column_name")

        X_gravity = X_gravity_raw.merge(
            df_prod[["prod", region_column_name]],
            left_on="vb_from_id",
            right_index=True,
        )
        X_gravity = X_gravity.merge(
            df_att[["att", region_column_name]], left_on="vb_to_id", right_index=True
        )
        X_gravity.rename(
            {region_column_name + "_x": "b_from", region_column_name + "_y": "b_to"},
            axis=1,
            inplace=True,
        )

        # slice = (
        #     (X_gravity["prod"] > 0)
        #     & (X_gravity["att"] > 0)
        #     & (X_gravity["vb_from_id"] != X_gravity["vb_to_id"])
        # )
        # X_gravity = X_gravity.loc[slice, :]
        return X_gravity

    @staticmethod
    def _run_gravity_model(X_gravity: pd.DataFrame, gen: GumoreGen) -> pd.DataFrame:
        cost = X_gravity.dijk_cost.values.copy()
        if gen.change_cost_limit > 0:
            cost[(cost > 0) & (cost <= gen.change_cost_limit)] = gen.change_cost_limit
        if gen.change_cost_constant > 0:
            cost[cost > 0] = cost[cost > 0] + gen.change_cost_constant
        a = gen.f * (X_gravity["prod"] * X_gravity["att"]) ** gen.beta
        b = cost ** gen.alpha
        X_gravity["flow"] = np.divide(a, b, out=np.zeros_like(a), where=b != 0)
        X_gravity["flow"] = np.nan_to_num(X_gravity["flow"])
        X_gravity["prod"] = np.nan_to_num(X_gravity["prod"])
        X_gravity["att"] = np.nan_to_num(X_gravity["att"])
        return X_gravity

    @staticmethod
    def _balance_matrix(flows: pd.DataFrame) -> Tuple[pd.DataFrame, float]:
        qi, zj = GumoreGenSet._create_df_flows_margins(flows)
        qi = qi["prod"].values
        zj = zj["att"].values
        flows_piv = pd.pivot_table(
            flows, values="flow", index="vb_from_id", columns="vb_to_id"
        )
        flows_arr = flows_piv.values
        flows_arr = np.nan_to_num(flows_arr)
        flows_balanced_series, max_rel_err = GumoreGenSet._furness(qi, zj, flows_arr)
        flows_balanced = pd.DataFrame(
            flows_balanced_series, index=flows_piv.index, columns=flows_piv.columns
        )
        flows_balanced = GumoreGenSet._melt_flows(flows_balanced)
        return flows_balanced, max_rel_err

    @staticmethod
    def _melt_flows(flows_pivoted: pd.DataFrame) -> pd.DataFrame:
        df = pd.melt(flows_pivoted.T.reset_index(), id_vars=["vb_to_id"])
        df = df.rename({"value": "flow"}, axis=1)
        df = df[["vb_from_id", "vb_to_id", "flow"]]
        return df

    @staticmethod
    def _create_df_flows_margins(
        flows: pd.DataFrame,
    ) -> Tuple[pd.DataFrame, pd.DataFrame]:
        df_flows_margins_from = (
            flows.groupby("vb_from_id")
            .agg({"prod": "first", "flow": "sum", "b_from": "first"})
            .reset_index()
            .rename(columns={"flow": "from_flow"})
        )
        df_flows_margins_to = (
            flows.groupby("vb_to_id")
            .agg({"att": "first", "flow": "sum", "b_to": "first"})
            .reset_index()
            .rename(columns={"flow": "to_flow"})
        )
        return df_flows_margins_from, df_flows_margins_to

    @staticmethod
    def _furness(
        qi: np.ndarray,
        zj: np.ndarray,
        flows: np.ndarray,
        tol: float = 0.00001,
        max_iter: int = 100,
        verbose: bool = False,
    ) -> Tuple[np.ndarray, float]:
        np_err = np.geterr()
        np.seterr(divide="ignore")
        np.seterr(invalid="ignore")
        max_rel_err = np.inf
        c = 0
        fqi = np.ones(qi.shape)
        fzj = np.ones(zj.shape)
        bij = flows / flows.sum()
        fqi[fqi == 0] = 0.00000001
        fzj[fzj == 0] = 0.00000001
        bij[bij == 0] = 0.00000001
        while (abs(max_rel_err) > tol) and (c <= max_iter):
            c += 1
            new_fqi = np.broadcast_to(qi, fqi.shape) / (bij * fzj).sum(axis=1)
            new_fzj = np.broadcast_to(zj, fzj.shape) / (bij * new_fqi).sum(axis=1)
            new_fzj = zj / (bij * np.broadcast_to(new_fqi, bij.shape).transpose()).sum(
                axis=0
            )
            flows = bij * new_fqi.reshape(-1, 1) * new_fzj
            qi_bal = flows.sum(axis=1)
            zj_bal = flows.sum(axis=0)
            rel_err_q = (qi_bal - qi) / qi
            rel_err_z = (zj_bal - zj) / zj
            max_rel_err = max(
                np.max(np.abs(np.nan_to_num(rel_err_q))),
                np.max(np.abs(np.nan_to_num(rel_err_z))),
            )
            fqi = new_fqi
            fzj = new_fzj
        if verbose:
            print(f"n_iter: {c}")
            print(f"max relative error: {max_rel_err:.8f}")

        np.seterr(divide=np_err["divide"])
        np.seterr(invalid=np_err["invalid"])
        return flows, max_rel_err
