"""
The `rules` package contains the logic for handling user defined model rules.

The `gen` module is responsible for the generation of flows of commodities between
the model zones. This is done by creating production and attraction values for all
zones with multiple regression models for each NST group. The results are used as
input for a gravitational traffic model.

The `parcelgen` module handles the distribution logic for parcels. Distribution areas
can be defined as zone attributes for hubs of multiple parcel services.

The `means` module handles the means assignment rules, which can also include
directives for flow splitting at hubs or other locations.

The `vehicle` module is responsible for the calculation of the sums of individual
vehicles.

All modules use a mechanism of registering user defined rule classes to a rule set
class. The rule set class is used to execute all registered rules in order of
their respective registration. Rule objects are created as instances of their classes
and with a global context as one of their attributes. This context can also be modified
during rule execution.
"""

from .gen import GumoreGen, GumoreGenSet
from .means import GumoreRule, GumoreRuleSet
from .vehicle import Vehicle, VehicleSet
from .parcelgen import ParcelGen, ParcelGenSet
