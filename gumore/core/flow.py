from typing import List

from gumore.core.entities import TransportMeans
from gumore.core.findzone import ZoneRatio
from gumore.util.database import get_db_connection, create_cursor
from gumore.util.log import *


initialize_logger()
logger = logging.getLogger(__name__)


class Flow:
    """ the flow is generated and
        then split up in children during means of transport calculation.
        If a flow has children, then itself is not valid any more.
    """

    def __init__(
        self,
        f,
        t,
        commodity,
        qty,
        means=None,
        vessel=None,
        source=None,
        sink=None,
        p_company=None,
        p_hub_name=None,
        p_type=None,
    ):
        self.f = f
        self.t = t
        if source:
            self.source = source
        else:
            self.source = f
        if sink:
            self.sink = sink
        else:
            self.sink = t
        self.commodity: str = commodity
        self.qty = qty
        self.means = means
        self.vessel = vessel
        self.p_company = p_company
        self.p_hub_name = p_hub_name
        self.p_type = p_type
        self.children = []

    def __repr__(self):
        return (
            f"Flow({self.f}, {self.t}, {self.commodity}, {self.qty}, {self.means}, {self.vessel}, "
            f"{self.source}, {self.sink})"
        )

    def __str__(self):
        leaves = self.get_leaves(self)

        out_str = f"{len(leaves)} leaves:\n"
        for leave in leaves:
            out_str += f"   {leave.__repr__()}\n"
        return out_str

    # @staticmethod
    # def _get_children(flow):
    #     print(flow.children)
    #     if len(flow.children) == 0:
    #         return [flow]
    #     return Flow._get_children(flow)

    def as_tuple(self):
        return self.f, self.t, self.commodity, self.qty, self.means, self.vessel

    def add_child(self, source=None, sink=None):
        """ clone the flow as its own child and
            return the new child
        """
        child = Flow(
            self.f,
            self.t,
            self.commodity,
            self.qty,
            self.means,
            source=source,
            sink=sink,
            p_company=self.p_company,
            p_hub_name=self.p_hub_name,
            p_type=self.p_type,
        )
        self.children.append(child)
        return child

    def setmeans(self, means, ratios=None):
        if isinstance(means, TransportMeans):
            self.means = means
        else:
            assert sum(ratios) == 1.0, (
                f"Sum of ratios is {sum(ratios)}, but " f"needs to be 1."
            )
            for m, ratio in zip(means, ratios):
                child = self.add_child(source=self.source, sink=self.sink)
                child.means = m
                child.qty = ratio * self.qty

    def split(self, cells_ratios):
        """
        split the flow at a Cell

        Args:
            cells_ratios (list): a list of 2-tuples (cells,ratios) where

                * cells: a Cell, where to split
                * ratios: a ratio for the Cell

        Returns: the resulting flows
        """
        for cell, ratio in cells_ratios:
            leg1 = self.add_child()
            leg2 = self.add_child()
            leg1.t = cell
            leg2.f = cell
            leg1.source = self.f
            leg1.sink = self.t
            leg2.source = self.f
            leg2.sink = self.t
            qty = (ratio) * self.qty
            leg1.qty = qty
            leg2.qty = qty
        return self.children

    def reload(self, cells_ratios: List[ZoneRatio], means_list):
        assert len(cells_ratios) + 1 == len(means_list), (
            f"Could not reload flow {self}. Number of means "
            f"({len(means_list)}) does not match number of split cells "
            f"({len(cells_ratios)}). Number of means "
            f"needs to be number of split cells + 1."
        )
        last_t = self.f
        legs = []
        for cells_ratio, means in zip(cells_ratios, means_list):
            cell_id, _ = cells_ratio
            if last_t != cell_id:
                leg = self.add_child(source=self.f, sink=self.t)
                leg.f = last_t
                leg.t = cell_id
                leg.qty = self.qty
                leg.means = means
                legs.append(leg)
                last_t = cell_id
        if last_t != self.t:
            last_leg = self.add_child(source=self.f, sink=self.t)
            last_leg.f = last_t
            last_leg.t = self.t
            last_leg.qty = self.qty
            last_leg.means = means_list[-1]
            legs.append(last_leg)
        return legs  # self.children

    def reload_parallel(self, cells_ratios: List[ZoneRatio], means_pre, means_post):
        legs = []
        for cells_ratio in cells_ratios:
            if cells_ratio.id == self.f:
                leg = self.add_child(source=self.f, sink=self.t)
                leg.qty = cells_ratio.ratio * self.qty
                leg.means = means_post
                legs.append(leg)
            elif cells_ratio.id == self.t:
                leg = self.add_child(source=self.f, sink=self.t)
                leg.qty = cells_ratio.ratio * self.qty
                leg.means = means_pre
                legs.append(leg)
            else:
                leg = self.add_child(source=self.f, sink=self.t)
                leg.f = self.f
                leg.t = cells_ratio.id
                leg.qty = cells_ratio.ratio * self.qty
                leg.means = means_pre
                legs.append(leg)
                leg = self.add_child(source=self.f, sink=self.t)
                leg.f = cells_ratio.id
                leg.t = self.t
                leg.qty = cells_ratio.ratio * self.qty
                leg.means = means_post
                legs.append(leg)
        return legs

        # last_t = self.f
        # legs = []
        # for cells_ratio, means in zip(cells_ratios, means_list):
        #     cell_id, _ = cells_ratio
        #     if last_t != cell_id:
        #         leg = self.add_child(source=self.f, sink=self.t)
        #         leg.f = last_t
        #         leg.t = cell_id
        #         leg.qty = self.qty
        #         leg.means = means
        #         legs.append(leg)
        #         last_t = cell_id
        # if last_t != self.t:
        #     last_leg = self.add_child(source=self.f, sink=self.t)
        #     last_leg.f = last_t
        #     last_leg.t = self.t
        #     last_leg.qty = self.qty
        #     last_leg.means = means_list[-1]
        #     legs.append(last_leg)
        # return legs  # self.children

    def reload_if_possible(
        self,
        zones_ratios: List[ZoneRatio],
        means_pre: TransportMeans,
        means_main: TransportMeans,
        means_post: TransportMeans,
    ):

        assert len(zones_ratios) in (0, 1, 2)
        # If there are less then 2 freight yards:
        # go by means_pre
        if len(zones_ratios) in (0, 1):
            self.setmeans(means_pre)
        # If source freight yard is in target zone:
        # go by means_pre
        elif zones_ratios[0].id == self.t:
            self.setmeans(means_pre)
        # Check if source freight yard is the same as target freight yard:
        # then go by means_pre
        elif zones_ratios[0].id == zones_ratios[1].id:
            self.setmeans(means_pre)
            # print(1)
        # if flow origin has freight yard, but flow target not:
        # then go from origin to freight yard by means_main and
        # from freight yard to target by means_post
        elif zones_ratios[0].id == self.f and zones_ratios[1].id != self.t:
            self.reload([zones_ratios[1]], [means_main, means_post])
            # print(2)
        # if flow target has freight yard, but flow origin not:
        # then go from origin to freight yard by means_pre and
        # from freight yard to target by means_main
        elif zones_ratios[1].id == self.t and zones_ratios[0].id != self.f:
            self.reload([zones_ratios[0]], [means_pre, means_main])
            # print(3)
        # if origin and target both don't have freight yards and they
        # don't share the same freight yard:
        # then go from origin to freight yard 1 by means_pre,
        # from freight yard 1 to freight yard 2 by means_main, and
        # from freight yard 2 to target by means_post
        else:
            self.reload(zones_ratios, [means_pre, means_main, means_post])
            # print(4)

    @staticmethod
    def get_leaves(flow):
        leaves = []
        if not flow.children:
            leaves.append(flow.to_tuple())
        for child in flow.children:
            leaves += flow.get_leaves(child)
        return leaves

    def to_tuple(self):
        return (
            self.f,
            self.t,
            self.commodity,
            self.qty,
            self.means.code if self.means else None,
            self.source,
            self.sink,
            self.p_company,
            self.p_hub_name,
            self.p_type,
        )
