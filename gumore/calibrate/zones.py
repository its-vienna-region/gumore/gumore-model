# https://towardsdatascience.com/machine-learning-bit-by-bit-multivariate-gradient-descent-e198fdd0df85

import numpy as np
import pandas as pd


# import gumore.model.model as mod
# import gumore.core.base
import gumore.core.entities as en

# import gumore.core.graph as graph

# app=gumore.core.base.gumore_app


class ZoneCalibrator:
    def __init__(self, context, counts, alpha=0.5, threshold=0.00000001, max_iter=5):
        self.alpha = alpha
        self.threshold = threshold
        self.max_iter = max_iter
        self.single_param_mode = True
        self.last_loss = 0
        self.n_iter = 0
        self.active_param = 0
        self.collect_losses = True

        self.params = [
            (context.rules.rules[0], "mindist"),
            (context.rules.rules[0], "maxdist"),
        ]
        self.n_params = len(self.params)
        self.param_values = np.zeros((self.max_iter, self.n_params), dtype=np.float)
        self.param_values[0, :] = [getattr(x[0], x[1]) for x in self.params]

        self.losses = np.zeros((self.max_iter, self.n_params), dtype=np.float)

        self.counts = pd.DataFrame(
            data=counts, columns=["zone_id", "direction", "means", "counted"]
        )

    def set_params(self):
        print("=" * 100)
        print(
            "Starting iteration {}, parameter to change: {} ({})".format(
                self.n_iter, self.active_param, self.params[self.active_param]
            )
        )
        print("-" * 100)
        for i in range(self.n_params):
            self._set_param(i, self.param_values[self.n_iter, i])
        for param in self.params:
            print(param[1], getattr(param[0], param[1]))

    def calibrate(self, vessel_results):
        print("=" * 100)
        print(
            "Starting iteration {}, parameter to change: {}".format(
                self.n_iter, self.active_param
            )
        )
        print("-" * 100)
        for param in self.params:
            print(param[1], getattr(param[0], param[1]))

        if self.n_iter == self.max_iter:
            self._print_result()
            return True

        if self.active_param == 0:
            self.losses[self.n_iter, :] = self._calc_loss(vessel_results)
            self.old_values = self.param_values[self.n_iter, :]

        if self.active_param > 0:
            self.losses[self.n_iter + 1, self.active_param - 1] = self._calc_loss(
                vessel_results
            )

        if self.active_param == -1:
            self.losses[self.n_iter + 1, self.n_params - 1] = self._calc_loss(
                vessel_results
            )

        if self.active_param == -1:
            print("change all")
            print(self.losses)
            self.n_iter += 1
            self.active_param = 0
            return False

        if self.n_iter == 0:
            grad = self.losses[0, self.active_param]
        else:
            grad = (
                self.losses[self.n_iter, self.active_param]
                - self.losses[self.n_iter - 1, self.active_param]
            )

        old_value = self.old_values[self.active_param]
        new_value = old_value + old_value * self.alpha * grad
        print(old_value, grad, new_value)
        setattr(
            self.params[self.active_param][0],
            self.params[self.active_param][1],
            new_value,
        )

        self.active_param += 1

        if self.active_param == self.n_params:
            self.active_param = -1

        self._print_result()

    def _calc_new_value(self, i_param):
        new_value = (
            old_value + old_value * self.alpha * self.gradients[self.n_iter, i_param]
        )
        return new_value

    def _set_param(self, i_param, value):
        setattr(self.params[i_param][0], self.params[i_param][1], value)

    def _get_gradient(self, i_iter, i_param):
        grad = self.gradient[i_iter, i_param]
        return grad

    def _store_att_value(self, i_iter, i_param):
        self.param_values[i_iter, i_param] = getattr(
            self.params[i_param][0], self.params[i_param][1]
        )

    def _calc_loss(self, vessel_results):
        # print(vessel_results)
        self.counts = self.counts.loc[:, ["zone_id", "direction", "means", "counted"]]
        vessel_results_to = vessel_results.groupby(
            by=["t", "means"], as_index=False
        ).sum()
        vessel_results_from = vessel_results.groupby(
            by=["f", "means"], as_index=False
        ).sum()
        counts_to = (
            self.counts[self.counts.direction == "t"]
            .merge(
                vessel_results_to,
                how="left",
                left_on=["zone_id", "means"],
                right_on=["t", "means"],
            )
            .loc[:, ["zone_id", "direction", "means", "count", "counted"]]
        )
        counts_from = (
            self.counts[self.counts.direction == "f"]
            .merge(
                vessel_results_from,
                how="left",
                left_on=["zone_id", "means"],
                right_on=["f", "means"],
            )
            .loc[:, ["zone_id", "direction", "means", "count", "counted"]]
        )
        self.counts = counts_to.append(counts_from)
        self.counts["loss"] = self.counts["counted"] / self.counts["count"]
        print(self.counts)
        return self.counts.loss.sum()

    def _print_result(self):
        print(self.losses)
        # print(self.gradients)
        print(self.param_values)


if __name__ == "__main__":
    g = graph.Graph()
    print(g.zones.df)

    zc = ZoneCalibrator()
    print(zc.counts)

    print(zc.variables)

    print(mod.SplitNST01)

    print(app.rules)
    print(getattr(zc.variables[0][0], zc.variables[0][1]))
    setattr(zc.variables[0][0], zc.variables[0][1], 50)
    print(getattr(zc.variables[0][0], zc.variables[0][1]))
