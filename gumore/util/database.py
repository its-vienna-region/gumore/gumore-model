import configparser

import psycopg2
import psycopg2.extras
import sqlalchemy
from sqlalchemy import create_engine

from gumore.util import global_vars
import gumore.util.conf
from gumore.util.conf import (
    get_active_config_as_dict,
    get_config,
    is_scenario,
    scenario_name,
)
from gumore.core.entities import commodities
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


def add_scenario_schema_if_applicable(x: str) -> str:
    if is_scenario():
        return f"{scenario_name()}.{x}"
    else:
        return f"baseline.{x}"


def get_active_schema():
    if is_scenario():
        return scenario_name()
    else:
        return "baseline"


TABLES = {
    x: add_scenario_schema_if_applicable(x)
    for x in [
        "prod",
        "att",
        "prod_cal",
        "att_cal",
        "rawflows",
        "meansflows",
        "parcelflows",
        "trips",
        "assignment",
        "assignment_qty",
        "geoview__assignment",
        "geoview__assignment_qty",
        "counts_vs_assignment",
        "count_vs_assignment_geoview",
    ]
}


def get_test_db_connection():
    conf = gumore.util.conf.get_config()
    db_conf_name = "database." + conf.get("database", "test_db_conf_name")
    host = conf.get(db_conf_name, "host")
    port = conf.getint(db_conf_name, "port")
    dbname = conf.get(db_conf_name, "name")
    user = conf.get(db_conf_name, "user")
    password = conf.get(db_conf_name, "password")
    con = psycopg2.connect(
        dbname=dbname, user=user, host=host, password=password, port=port
    )
    return con


def get_db_connection() -> psycopg2._psycopg.connection:
    """
    Creates new database connection.

    Connection information is read from active config.

    Supported database type: PostgreSQL.

    Returns:
        psycopg2.extensions.connection: The connection.

    """
    try:
        conf = gumore.util.conf.get_config()
        db_conf_name = "database." + conf.get("database", "active_db_conf_name")         
        connection = psycopg2.connect(
            host=conf.get(db_conf_name, "host"),
            port=conf.getint(db_conf_name, "port"),
            dbname=conf.get(db_conf_name, "name"),
            user=conf.get(db_conf_name, "user"),
            password=conf.get(db_conf_name, "password")
        )
        return connection

    except Exception:
        logger.exception("Fatal Error")
        raise


def create_cursor(connection: psycopg2._psycopg.connection) -> psycopg2._psycopg.cursor:
    """Creates a database cursor for an existing connection.

    Args:
        connection (psycopg2._psycopg.connection): The connection

    Returns:
        psycopg2._psycopg.cursor: The cursor
    """
    return connection.cursor(cursor_factory=psycopg2.extras.DictCursor)


def get_engine() -> sqlalchemy.engine.Engine:
    """Creates and returns an `sqlalchemy` database engine.

    The connection parameters are read from the config.

    Returns:
        sqlalchemy.engine.Engine: The created engine.
    """
    conf = gumore.util.conf.get_config()

    db_conf_name = "database." + conf.get("database", "active_db_conf_name")
    url = "postgresql://{user}:{password}@{host}:{port}/{dbname}".format(
        host=conf.get(db_conf_name, "host"),
        port=conf.getint(db_conf_name, "port"),
        dbname=conf.get(db_conf_name, "name"),
        user=conf.get(db_conf_name, "user"),
        password=conf.get(db_conf_name, "password"),
    )
    engine = create_engine(url)
    return engine


def init_database() -> None:
    """Creates the following PostgreSQL extensions, if not yet existing: 
    `postgis`, `pgrouting`, `tablefunc`.
    """
    conn = get_db_connection()
    c = create_cursor(conn)
    c.execute("CREATE EXTENSION IF NOT EXISTS postgis")
    c.execute("CREATE EXTENSION IF NOT EXISTS pgrouting")
    c.execute("CREATE EXTENSION IF NOT EXISTS tablefunc")
    conn.commit()


def init_tables() -> None:
    conn = get_db_connection()
    c = create_cursor(conn)
    c.execute("CREATE SCHEMA IF NOT EXISTS baseline")

    if is_scenario():
        if get_config().getboolean(
            scenario_name(), "scale_flows_with_empirical_baseline_data"
        ):
            c.execute(
                """
                SELECT EXISTS (
                SELECT FROM information_schema.tables 
                WHERE  table_schema = 'baseline'
                AND    table_name   = 'comp_reg_emp_bund_baseline'
                );
            """
            )
            baseline_scaling_factors_exist = c.fetchone()[0]
            if not baseline_scaling_factors_exist:
                raise ValueError(
                    "Running in scenario prognosis mode, but no baseline has been "
                    "calculated yet (Table 'baseline.comp_reg_emp_bund_baseline' not "
                    "in database ). You need to run in baseline mode first. "
                    "See config section 'model_run', option "
                    "'baseline_or_scenario'."
                )
        else:
            c.execute(
                f"DROP TABLE IF EXISTS {scenario_name()}.comp_reg_emp_bund_baseline"
            )
        c.execute(f"CREATE SCHEMA IF NOT EXISTS {scenario_name()}")
    else:
        c.execute("DROP TABLE IF EXISTS baseline.comp_reg_emp_bund_baseline")

    config_str = f"{get_active_config_as_dict()}".replace("'", '"')
    c.execute(f"COMMENT ON SCHEMA {get_active_schema()} IS '{config_str}'")

    tables_to_drop = [
        "prod",
        "att",
        "prod_cal",
        "att_cal",
        "rawflows",
        "meansflows",
        "parcelflows",
        "trips",
    ]
    for table_name_without_schema in tables_to_drop:
        c.execute(f"DROP TABLE IF EXISTS {TABLES[table_name_without_schema]}")
    c.execute(f"DROP TABLE IF EXISTS {TABLES['assignment']} CASCADE")
    c.execute(f"DROP TABLE IF EXISTS {TABLES['assignment_qty']} CASCADE")

    c.execute(
        f"""CREATE TABLE {TABLES['rawflows']} (
                f integer, 
                t integer, 
                commodity text, 
                qty real,
                PRIMARY KEY (f, t, commodity)
            )"""
    )
    c.execute(
        f"""CREATE TABLE {TABLES['meansflows']}
         (f integer, t integer, commodity text, qty real,means text, source integer, sink integer)"""
    )
    c.execute(
        f"""CREATE TABLE {TABLES['parcelflows']} (
            commodity text,
            company text,
            hub_name text,
            ptype text,
            means text,
            hub_zone_id integer,
            target_zone integer,
            source integer,
            sink integer,
            parcels integer)"""
    )

    c.execute(
        f"""CREATE TABLE {TABLES['trips']}
                 (f integer,
                 t integer,
                 means text,
                 vessel text,
                 commodities text,
                 count integer)"""
    )
    c.execute(f"CREATE INDEX idx_trips_f ON {TABLES['trips']} (f)")
    c.execute(f"CREATE INDEX idx_trips_t ON {TABLES['trips']} (t)")
    c.execute(f"CREATE INDEX idx_trips_means ON {TABLES['trips']} (means)")

    graph_table_name = gumore.util.conf.get_graph_table_name()
    c.execute(
        f"""
        CREATE TABLE {TABLES['assignment']} (
            id serial PRIMARY KEY DEFERRABLE INITIALLY DEFERRED,
            ways_id integer REFERENCES {graph_table_name} (id),
            means text,
            count integer)"""
    )
    c.execute(
        f"""
        CREATE TABLE {TABLES['assignment_qty']} (
            id serial PRIMARY KEY DEFERRABLE INITIALLY DEFERRED,
            ways_id integer REFERENCES {graph_table_name} (id),
            commodity text,
            qty real)"""
        )
    print(f"""
        CREATE TABLE {TABLES['assignment_qty']} (
            id serial PRIMARY KEY DEFERRABLE INITIALLY DEFERRED,
            ways_id integer REFERENCES {graph_table_name} (id),
            commodity text,
            qty real)""")

    conn.commit()
    c.close()




if __name__ == "__main__":
    conn = get_db_connection()
    print(conn)
    print(create_cursor(conn))
    print(type(conn))
