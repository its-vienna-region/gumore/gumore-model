from functools import partial
import os
from typing import Dict, Iterable, List, Tuple

from gumore.util import global_vars
import gumore.util.conf
from gumore.util.log import *

import numpy as np
import pandas as pd

initialize_logger()
logger = logging.getLogger(__name__)


def _get_flows_emp_file_paths(
    basepath_empirical_flows: str, prefix: str, nst_ids: List[int], suffix: str
) -> List[str]:
    file_paths = [
        os.path.join(basepath_empirical_flows, prefix + str(nst_id) + suffix)
        for nst_id in nst_ids
    ]
    return file_paths


def _load_csvs_into_dataframes(
    file_paths: Iterable[str], sep: str, decimal: str
) -> List[pd.DataFrame]:
    return list(map(partial(pd.read_csv, sep=sep, decimal=decimal), file_paths))


def load_regional_sums_for_structural_data(level: str = "nuts2") -> pd.DataFrame:
    conf = gumore.util.conf.get_config()
    data_path = gumore.util.conf.get_absolute_path(conf, "paths", "data")
    basepath_regional_sums = os.path.join(
        data_path, conf.get("paths", "data_structural_regional_sums")
    )
    if level == "nuts2":
        filename = gumore.util.conf.add_scenario_name_if_applicable("regional_sums.csv")
        index_col = conf.get("graph", "zones_region_column_name")
    elif level == "nuts3":
        filename = gumore.util.conf.add_scenario_name_if_applicable(
            "regional_sums_nuts3_DUMMY_DATA.csv"
        )
        index_col = "nuts3_code"
    filepath = os.path.join(data_path, basepath_regional_sums, filename)
    regional_sums = pd.read_csv(filepath, sep=";", index_col=index_col)
    if level == "nuts2":
        assert set(conf.get("filedata", "flows_emp_cell_names").split(",")).issubset(
            set(regional_sums.index)
        )

    return regional_sums


def load_empirical_flow_data(
    internal_or_external: str
) -> Tuple[Dict[int, pd.DataFrame], Dict[int, pd.DataFrame]]:
    config_section_prefix = (
        "flows_emp_" if internal_or_external == "internal" else "external_flows_"
    )
    data_path_flows = (
        "data_emp_flows"
        if internal_or_external == "internal"
        else "data_external_flows"
    )
    conf = gumore.util.conf.get_config()
    data_path = gumore.util.conf.get_absolute_path(conf, "paths", "data")
    basepath_empirical_flows = os.path.join(
        data_path, conf.get("paths", data_path_flows)
    )
    prefix = conf.get("filedata", f"{config_section_prefix}prefix")
    nst_ids = [
        int(x)
        for x in conf.get("filedata", f"{config_section_prefix}nst_ids").split(",")
    ]
    suffix = conf.get("filedata", f"{config_section_prefix}suffix")
    cell_names = conf.get("filedata", f"{config_section_prefix}cell_names").split(",")
    sep = conf.get("filedata", f"{config_section_prefix}csv_sep")
    decimal = conf.get("filedata", f"{config_section_prefix}csv_decimal")

    file_paths = _get_flows_emp_file_paths(
        basepath_empirical_flows, prefix, nst_ids, suffix
    )
    empirical_flow_data = _load_csvs_into_dataframes(file_paths, sep, decimal)

    flows_nsts_emp = dict()
    flows_nsts_emp_0_diagonal = dict()
    for i, d in enumerate(empirical_flow_data):
        df = (
            d.set_index(d.columns[0])
            .rename_axis("from_cell")
            .rename_axis("to_cell", axis=1)
        )
        if cell_names != [""]:
            df = df.loc[cell_names, cell_names]
        df_0_diagonal = df.copy()
        df_0_diagonal.values[tuple([np.arange(df_0_diagonal.shape[0])]) * 2] = 0
        flows_nsts_emp[nst_ids[i]] = df
        flows_nsts_emp_0_diagonal[nst_ids[i]] = df_0_diagonal
    return flows_nsts_emp, flows_nsts_emp_0_diagonal


def load_cordon_relations() -> Tuple[pd.DataFrame, pd.DataFrame]:
    conf = gumore.util.conf.get_config()
    data_path = gumore.util.conf.get_absolute_path(conf, "paths", "data")
    basepath_cordon_relations = os.path.join(
        data_path, conf.get("paths", "data_external_flows")
    )
    sep = conf.get("filedata", "cordon_relations_csv_sep")
    decimal = conf.get("filedata", "cordon_relations_csv_decimal")
    filename_input = conf.get("filedata", "cordon_relations_input_filename")
    filename_output = conf.get("filedata", "cordon_relations_output_filename")
    df_input: pd.DataFrame = pd.read_csv(
        os.path.join(basepath_cordon_relations, filename_input),
        sep=sep,
        decimal=decimal,
    )
    df_input = (
        df_input.set_index(df_input.columns[0])
        .rename_axis("from_cell")
        .rename_axis("to_cell", axis=1)
    )
    df_output: pd.DataFrame = pd.read_csv(
        os.path.join(basepath_cordon_relations, filename_output),
        sep=sep,
        decimal=decimal,
    )
    df_output = (
        df_output.set_index(df_output.columns[0])
        .rename_axis("from_cell")
        .rename_axis("to_cell", axis=1)
    )

    return df_input, df_output


if __name__ == "__main__":
    print(load_empirical_flow_data()[0])
    print(load_empirical_flow_data()[1])
