"""
This module reads configuration from files and provides access to it by passing `configparser.ConfigParser`-objects
and by writing this object to `gumore.util.global_vars`.

The configuration information is combined from two sources: an INI-File with system-level information
(logging, paths, databases, ...) and a spreadsheet file with model configuration.

Other modules may not work properly without running `get_config()` at least one time.
"""

import configparser
import os
from typing import Tuple, Dict

import pandas as pd

# import append_sys_path
from gumore.util import global_vars

if os.environ.get("GUMORE_CREATE_SPHINX_DOC") == "True" and not global_vars.proj_dir:
    import shutil
    import tempfile
    temp_dir = tempfile.TemporaryDirectory()
    conf_dir = os.path.join(os.path.dirname(__file__), "..", "config")
    shutil.copytree(conf_dir, os.path.join(temp_dir.name, "config"))
    global_vars.proj_dir = temp_dir.name


def _get_active_config_name():
    """
    Reads name of active config from config/active_config.txt.

    Returns:
        str: filename of active config

    """
    # file_path = os.path.abspath(
    #     os.path.join(
    #         os.path.dirname(__file__), "..", "..", "config", "active_config.txt"
    #     )
    # )
    try:
        file_path = os.path.join(global_vars.proj_dir, "config", "active_config.txt")
    except TypeError:
        return None
    try:
        with open(file_path, "r") as f:
            active_config_name = f.readline()
        return active_config_name
    except FileNotFoundError:
        return None


def _add_model_config_from_spreadsheet(
    config: configparser.ConfigParser, sheet_path: str
) -> Tuple[configparser.ConfigParser, Dict]:
    """
    Adds additional configuration from spreadsheet file to `config`.

    The spreadsheet file needs to be correctly structured. Ask Roland for more info.

    Args:
        config (`configparser.ConfigParser`): Config object to which additional configuration shall be added.
        sheet_path (str): Path to spreadsheet file.

    Returns:
        `configparser.ConfigParser`: amended config
        dict: Dictionary of matrices (key=matrix name, value=`pandas.DataFrame`)

    """

    config_key_prefix_for_tables = {
        "table_modes": "mode_obj_",
        "table_means": "means_obj_",
        "table_commodities": "commodity_obj_",
    }

    df_meta = pd.read_excel(sheet_path, sheet_name="META")
    meta = df_meta.to_dict()
    for key, value in zip(meta["key"].values(), meta["value"].values()):
        config["model_config"][key] = str(value)
    header = config.getint("model_config", "header_num_lines")

    for table in [x for x in config["model_config"].keys() if x.startswith("table_")]:
        sheet_name = config.get("model_config", table)
        df = pd.read_excel(sheet_path, sheet_name, header=header, dtype=str)
        df = df.loc[:, ~df.columns.str.contains("^Unnamed")]

        prefix = config_key_prefix_for_tables[table]
        df_dict = df.to_dict()
        cols = (tuple(x[1].values()) for x in list(df_dict.items()))
        records = tuple(zip(*cols))
        for i, record in enumerate(records):
            record_str = "||".join(record)
            key_name = prefix + str(i)
            config["model_config"][key_name] = record_str

    matrices = {}
    for matrix in [x for x in config["model_config"].keys() if x.startswith("matrix_")]:
        sheet_name = config.get("model_config", matrix)
        col_index = config.getint("model_config", "col_index_" + matrix)
        df = pd.read_excel(
            sheet_path, sheet_name, header=(header, header + col_index - 1)
        )
        # df = df.loc[:, ~df.columns.str.contains('^Unnamed')]
        df.set_index(df.columns[0], inplace=True)
        df.index.name = df.index.name[col_index - 1]
        matrices[matrix] = df

    return config, matrices


def get_config():
    """
    Reads config file and model config spreadsheet. Active config file can be set via "config/active_config.txt".

    The spreadsheet config is appended to the config object as new entries.
    The config is returned and written to `gumore.util.global_vars.config`
    The matrices from the spreadsheet are written to `gumore.util.global_vars.matrices_model_config`

    If config and matrices from model config spreadsheet are already in memory, they are not read again from the files.

    Returns:
        configparser.ConfigParser: config

    """
    if not global_vars.config and len(global_vars.matrices_model_config) == 0:

        # interpolation is deactivated for reading datefmt from config
        config = configparser.ConfigParser(interpolation=None)

        active_config_name = _get_active_config_name()

        if active_config_name:

            path = os.path.join(global_vars.proj_dir, "config", active_config_name)
            config.read(path, encoding="utf-8")

            # run sanity checks (before modification)
            _run_config_sanity_checks_pre(config)

            # add basepath
            config["paths"]["basepath"] = global_vars.proj_dir

            # add database password for active database from local password files
            active_db_conf_name = "database." + config.get(
                "database", "active_db_conf_name"
            )
            db_conf_file_path = os.path.join(
                config.get("paths", "basepath"),
                "config",
                config.get(active_db_conf_name, "password_file"),
            )
            config.read(db_conf_file_path, encoding="utf-8")

            # database password for test database from local password files, if possible
            try:
                test_db_conf_name = "database." + config.get(
                    "database", "test_db_conf_name"
                )
                test_db_conf_file_path = os.path.join(
                    config.get("paths", "basepath"),
                    "config",
                    config.get(test_db_conf_name, "password_file"),
                )
                config.read(test_db_conf_file_path, encoding="utf-8")
            except configparser.NoSectionError:
                print("No Section for test database found in config.")

            # get model config (including matrices) from spreadsheet
            active_scenario_name = config.get("model_run", "active_scenario_name")
            sheet_path_section = (
                active_scenario_name
                if config.get("model_run", "baseline_or_scenario") == "scenario"
                and config.get(active_scenario_name, "sheet_path")
                else "model_config"
            )
            sheet_path = get_absolute_path(config, sheet_path_section, "sheet_path")
            config, matrices = _add_model_config_from_spreadsheet(config, sheet_path)

            # write config and matrices to global_vars
            global_vars.config = config
            global_vars.matrices_model_config = matrices
        else:
            return None
    else:
        config = global_vars.config

    return config


def get_string_resource(name: str) -> str:
    return get_config().get("string_resources", name)


def _run_config_sanity_checks_pre(config: configparser.ConfigParser):
    get = config.get
    # model_run
    assert get("model_run", "baseline_or_scenario") in ("baseline", "scenario")
    if get("model_run", "baseline_or_scenario") == "scenario":
        active_scenario_name = get("model_run", "active_scenario_name")
        assert active_scenario_name is not ""
        assert active_scenario_name in config.sections()
    assert get("model_run", "vessel_remainder_trips_generation_mode") in (
        "simple",
        "detailed",
    )
    for option in (
        "perform_trips_assignment",
        "perform_parcel_simulation",
        "perform_result_sanity_check",
        "perform_count_value_comparison",
    ):
        assert get("model_run", option) in ("True", "False")

    # active scenario
    if get("model_run", "baseline_or_scenario") == "scenario":
        get(active_scenario_name, "scale_flows_with_empirical_baseline_data") in (
            "True",
            "False",
        )
        get(active_scenario_name, "use_scenario_rules_module") in ("True", "False")


def get_absolute_path(conf, section, path_key):
    """
    Reads path from config and adds basepath if path is relative.

    This enables both relative and absolute paths in config.

    Args:
        conf (ConfigParser object): config
        section (str): section in config
        path_key (str): key in config which contains the path

    Returns:
        str: absolute path

    """
    path = conf[section][path_key]
    if not os.path.isabs(path):
        path = os.path.join(conf["paths"]["basepath"], path)
    return path


def get_activate_db_conf_name():
    """
    Reads name of active database config section from config.

    Returns:
        str: name of active database config section
    """
    active_db_conf_name = "database." + get_config().get(
        "database", "active_db_conf_name"
    )
    return active_db_conf_name


def get_graph_table_name() -> str:
    if is_scenario() and get_config().get(scenario_name(), "graph_table_name"):
        return get_config().get(scenario_name(), "graph_table_name")
    else:
        return get_config().get(get_activate_db_conf_name(), "graph_table_name")


def get_zones_table_name() -> str:
    if is_scenario() and get_config().get(scenario_name(), "zones_table_name"):
        return get_config().get(scenario_name(), "zones_table_name")
    else:
        return get_config().get(get_activate_db_conf_name(), "zones_table_name")


def get_path_table_name(mode: str) -> str:
    if is_scenario() and get_config().get(scenario_name(), "path_table_name"):
        return get_config().get(scenario_name(), "path_table_name")
    graph_table_name = get_graph_table_name()
    zones_table_name = get_zones_table_name()
    return f"path__{zones_table_name}__{graph_table_name}__{mode}"


def check_db_config():
    conf = get_config()
    db_conf_file_path = os.path.join(
        conf.get("paths", "basepath"),
        "config",
        conf.get(get_activate_db_conf_name(), "password_file"),
    )
    options = ("host", "port", "name", "user", "password")
    missing_options = []
    for option in options:
        if not conf.get(get_activate_db_conf_name(), option):
            missing_options.append(option)
    if len(missing_options) > 0:
        raise ValueError(
            f"Check your database config file ({db_conf_file_path}). "
            f"Option(s) '{missing_options}' not set."
        )


def add_scenario_name_if_applicable(x: str) -> str:
    if is_scenario():
        if "." in x:
            parts = x.split(".")
            assert len(parts) == 2
            return f"{parts[0]}_{scenario_name()}.{parts[1]}"
        else:
            return f"{x}_{scenario_name()}"
    else:
        return x


def is_scenario():
    return get_config().get("model_run", "baseline_or_scenario") == "scenario"


def scenario_name():
    return get_config().get("model_run", "active_scenario_name")


def package_name():
    use_scenario_rules_module = is_scenario() and get_config().getboolean(
        scenario_name(), "use_scenario_rules_module"
    )
    name = f"scenarios.{scenario_name()}" if use_scenario_rules_module else "mymodel"
    return name


def package_path():
    use_scenario_rules_module = is_scenario() and get_config().getboolean(
        scenario_name(), "use_scenario_rules_module"
    )
    name = (
        os.path.join("scenarios", scenario_name())
        if use_scenario_rules_module
        else "mymodel"
    )
    return name


def get_distmat_table_name(internal_name: str) -> str:
    if is_scenario() and get_config().get(scenario_name(), internal_name):
        return get_config().get(scenario_name(), internal_name)
    else:
        return get_config().get("graph", internal_name)


def get_active_config_as_dict() -> dict:
    return {
        s.upper(): {
            k: v if k not in "password" else "*****" for k, v in get_config().items(s)
        }
        for s in get_config().sections()
    }


if __name__ == "__main__":
    conf = get_config()

    print()
    print("=" * 80)
    print("PRINTING ALL CONFIG SECTIONS")
    print("=" * 80, "\n")
    import pprint

    for key in conf.keys():
        print(key)
        print("-" * len(key))
        pprint.pprint(dict(conf[key]))
        print()

    print()
    print("=" * 80)
    print("PRINTING ALL GLOBAL VARIABLES")
    print("=" * 80, "\n")
    gvars = [x for x in dir(global_vars) if not x.startswith("__")]
    for var in gvars:
        print(var)
        print("-" * len(var))
        pprint.pprint(global_vars.__dict__[var])
        print()
