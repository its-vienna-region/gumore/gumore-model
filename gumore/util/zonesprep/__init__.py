"""
Utility functions for preparation of gumore traffic zones tables:

    * Import zones shapefile into database
    * Convert zones attributes to jsonb
    * Add centroids as preparation for connector calculation
    * Create connector tables
    * Create shortest path table

SYNOPSIS

use individual functions::

    import_zones_shapefile_into_database("vb_merged.shp", "vb", source_encoding="LATIN1")
    import_zones_shapefile_into_database("Regionen_NUTS2_NUTS0_mainzone.SHP", "vb_grob", source_encoding="LATIN1")

    convert_zones_attributes_to_jsonb_in_database("vb", ["inhabitants", "employees"], "gid")

    add_centroids_for_zones_in_database("vb_grob", "gid")
    add_centroids_for_zones_in_database("vb", "gid")

    create_connector_tables_in_database("vb_grob", ["hgv", "rail"], "ad", "gid")
    create_connector_tables_in_database("vb", ["hgv"], "eu", "gid")

    create_shortest_path_table_in_database("vb", ["hgv"], "eu_dense", "gid")
"""
# import append_sys_path
from gumore.util.log import *
from gumore.util.zonesprep.attributes import (
    convert_zones_attributes_to_jsonb_in_database,
    create_dummy_attributes_for_zones_in_database,
)
from gumore.util.zonesprep.connectors import create_connector_tables_in_database
from gumore.util.zonesprep.geometry import (
    add_centroids_for_zones_in_database,
    import_zones_shapefile_into_database,
)
from gumore.util.zonesprep.shortestpath import create_shortest_path_table_in_database

initialize_logger()
logger = logging.getLogger(__name__)

__all__ = [
    "convert_zones_attributes_to_jsonb_in_database",
    "create_connector_tables_in_database",
    "create_dummy_attributes_for_zones_in_database",
    "create_shortest_path_table_in_database",
    "add_centroids_for_zones_in_database",
    "import_zones_shapefile_into_database",
]

if __name__ == "__main__":
    # convert_zones_attributes_to_jsonb_in_database("vb__with_att", list("abcdefghijklmnopqrs") + ["ew"], "gid")
    # SYNOPSIS: use individual functions
    # import_zones_shapefile_into_database("vb_merged.shp", "vb", source_encoding="LATIN1")
    # import_zones_shapefile_into_database("Regionen_NUTS2_NUTS0_mainzone.SHP", "vb_grob", source_encoding="LATIN1")
    # import_zones_shapefile_into_database("oesterreich_bundeslaender.shp", "vb_at_nuts2", source_encoding="LATIN1")

    # create_dummy_attributes_for_zones_in_database("vb_at_nuts2",
    #                                               integer_columns=["einwohner", "besch_a", "besch_b"],
    #                                               float_columns=["attraktivitaet_1"],
    #                                               boolean_columns=["grosse_schottergrube",
    #                                                                "verladestelle",
    #                                                                "gvz",
    #                                                                "hafen",
    #                                                                "flughafen"],
    #                                               categorical_columns=["kategorie_1"])
    #
    # convert_zones_attributes_to_jsonb_in_database("vb_at_nuts2",
    #                                               ["einwohner", "besch_a", "besch_b",
    #                                                "attraktivitaet_1", "grosse_schottergrube",
    #                                                "verladestelle", "gvz",
    #                                                "hafen", "flughafen", "kategorie_1"],
    #                                               "gid")

    # add_centroids_for_zones_in_database("vb_grob", "gid")
    # add_centroids_for_zones_in_database("vb", "gid")
    # add_centroids_for_zones_in_database("vb_at_nuts2", "gid")

    # create_connector_tables_in_database("vb_grob", ["hgv", "rail"], "ad", "gid")
    # create_connector_tables_in_database("vb", ["hgv"], "eu_dense", "gid")
    # create_connector_tables_in_database("vb_at_nuts2", ["hgv"], "eu_dense", "gid")
    # create_connector_tables_in_database("vb_grob", ["hgv"], "eu", "gid")

    # create_shortest_path_table_in_database("vb__with_att", ["hgv"], "eu_dense", "gid")

    # create_shortest_path_table_in_database(
    #     "vb__with_att",
    #     ["hgv"],
    #     "eu_dense",
    #     "gid",
    #     zones_where_clause="WHERE gid in (1349, 1351, 1350, 1417, 1425, 333, 325)",
    #     repair_zones_with_identical_connectors=True,
    #     debug=True
    # )

    # SYNOPSIS: use wrapper function

    pass
