from itertools import permutations
from datetime import datetime
import logging
from typing import Iterable, Tuple

import click
import psycopg2.extensions

# import append_sys_path
from gumore.util.database import get_db_connection, create_cursor
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


# @cli.command(name="paths")
# @click.option(
#     "-z",
#     "--zones-table-name",
#     required=True,
#     help="Name of zones table, e.g. 'vb__with_att'",
# )
# @click.option(
#     "-m",
#     "--mode-of-transport",
#     "modes_of_transport",
#     required=True,
#     multiple=True,
#     help="Name of the mode of transport, e.g. 'hgv'. Can be repeated.",
# )
# @click.option(
#     "-g",
#     "--graph-table-prefix",
#     required=True,
#     help="Prefix of graph table, e.g. 'eu_dense'",
# )
# @click.option(
#     "-pk",
#     "--zones-primary-key-column",
#     required=True,
#     help="Primary key column name of zones table, e.g. 'gid'",
# )
# @click.option(
#     "-bb",
#     "--bbox",
#     type=(float, float, float, float),
#     default=(9.47, 46.43, 17.0, 49.04),
#     show_default=True,
#     help="Bounding box for clipping the graph in the form: SW-LONG SW-LAT NE-LONG NE-LAT. Defaults to bbox of Austria.",
# )
# @click.option(
#     "-bs",
#     "--batch-size",
#     type=int,
#     default=10,
#     show_default=True,
#     help="Number of source cells in one batch run of the `pgr_astar` algorithm. "
#     "This will highly impact memory usage during calculation.",
# )
# @click.option(
#     "-w",
#     "--zones-where-clause",
#     default="WHERE land = 'AT'",
#     show_default=True,
#     help="SQL clause to select zones for which the shortest paths shall be calculated.",
# )
# @click.option(
#     "-r",
#     "--repair-zones-with-identical-connectors",
#     type=bool,
#     default=True,
#     show_default=True,
#     help="Needs to be TRUE if you have zones sharing identical connectors.",
# )
# @click.option(
#     "-d",
#     "--debug",
#     type=bool,
#     default=False,
#     show_default=True,
#     help="Adds the prefix 'debug__' to the created shortest path table(s) to avoid overriding existing tables.",
# )
def create_shortest_path_table_in_database(
    zones_table_name: str,
    modes_of_transport: Iterable[str],
    graph_table_prefix: str,
    zones_primary_key_column: str,
    bbox: Tuple[float, float, float, float] = (9.47, 46.43, 17.2, 49.04),
    batch_size: int = 10,
    zones_where_clause: str = "",
    repair_zones_with_identical_connectors: bool = True,
    debug: bool = False,
):
    """
    (Re-)creates a table with all shortest paths between all zones for each mode.

    The shortest paths are calculated using `pgrouting`'s `pgr_astar` function. The
    graph is clipped to the extent of the bounding box to speed up calculation. The
    calculation is split up into batches of source cells to limit the memory usage.
    If you are running out of memory, try to reduce `batch_size` (which will increase
    running time).

    The created table(s) will start with the prefix 'path__' or 'debug__path__'.

    Example script usage:

        python zonesprep.py paths -z vb__with_att -m hgv -g eu_dense -pk gid  -w "WHERE gid in (1349, 1351, 1350, 1417, 1425, 333, 325)" -d True
    \f

    Args:

        zones_table_name (`str`): Name of the zones table.
        modes_of_transport (`Iterable[str]`): Names of the modes.
        graph_table_prefix (`str`): Name of the graph table
        zones_primary_key_column (`str`): Name of the zones table's primary key column.
        bbox (`Tuple[float, float, float, float]`): Bounding box to which the search
            for the shortest paths will be restricted, in the form: (SW-LONG,SW-LAT,
            NE-LONG,NE-LAT). Only links within the bounding box will be considered.
        batch_size (`int`, defaults to 10): Number of source cells in one batch run
            of the `pgr_astar` algorithm.
        zones_where_clause (`str`, defaults to "WHERE land = 'AT'"): SQL clause to
            select zones for which the shortest paths shall be calculated.
        repair_zones_with_identical_connectors (`bool`, defaults to `True`): Needs to
            be TRUE if you have zones sharing identical connectors.
        debug (`bool`, defaults to `True`): Adds the prefix 'debug__' to the created
            shortest path table(s) to avoid overriding existing tables.

    Examples:

        create_shortest_path_table_in_database("vb", ["hgv"], "eu_dense", "gid")

    Warnings:

        Expect the created table and indices to use a lot of disk space!
    """

    graph_table_name = f"{graph_table_prefix}_2po_4pgr"
    path_table_name_base = f"path__{zones_table_name}__{graph_table_name}__"
    if debug:
        path_table_name_base = "debug__" + path_table_name_base
    conn = get_db_connection()
    c = create_cursor(conn)

    sql = f"""
        SELECT {zones_primary_key_column} 
        FROM {zones_table_name} 
        {zones_where_clause}"""
    c.execute(sql)
    zone_ids = [x[0] for x in c.fetchall()]

    n_relations = len(list(permutations(zone_ids, 2)))
    logger.info(
        f"Starting creation of shortest path table for {n_relations} zone " f"relations"
    )

    for mode in modes_of_transport:
        connector_table_name = (
            f"connect__{zones_table_name}__to__{graph_table_prefix}_2po_4pgr__{mode}"
        )
        path_table_name = path_table_name_base + mode

        sql = f"""
            drop table if exists {path_table_name} CASCADE
            """
        c.execute(sql)

        sql = f"""
            CREATE TABLE {path_table_name} (
                id serial PRIMARY KEY,
                vb_from integer NOT NULL,
                vb_to integer NOT NULL,
                ways_id integer REFERENCES {graph_table_name}(id) ON DELETE RESTRICT)
            """
        c.execute(sql)

        sql = f"select ways_source from {connector_table_name} where gid in {str(tuple(zone_ids))}"
        c.execute(sql)
        ways_sources = [x[0] for x in c.fetchall()]
        sql = f"select ways_target from {connector_table_name} where gid in {str(tuple(zone_ids))}"
        c.execute(sql)
        ways_targets = [x[0] for x in c.fetchall()]

        sql = f"""
            CREATE OR REPLACE TEMPORARY VIEW {graph_table_name}__temp AS SELECT * FROM {graph_table_name} WHERE {mode} = true"""
        c.execute(sql)

        def create_batches(iterable, l_batches=None, n_batches=None):
            assert not (l_batches and n_batches)
            assert l_batches or n_batches
            if n_batches:
                n = len(iterable) // n_batches
            elif l_batches:
                n = l_batches
            l = len(iterable)
            for ndx in range(0, l, n):
                yield iterable[ndx : min(ndx + n, l)]

        ways_sources_batches = list(create_batches(ways_sources, l_batches=batch_size))
        start_time = datetime.now()
        n_batches = len(ways_sources_batches)

        for i, batch in enumerate(ways_sources_batches):
            logger.info(f"Batch {i+1} of {n_batches}: {batch}")

            sql = f"""
                INSERT INTO {path_table_name} (vb_from, vb_to, ways_id)
                SELECT
                   (select c.gid from {connector_table_name} AS c where c.ways_source = p.start_vid limit 1),
                   (select c.gid from {connector_table_name} AS c where c.ways_target = p.end_vid limit 1),
                   n.id
                FROM
                   {graph_table_name} n,
                    (SELECT start_vid, end_vid, edge, cost from pgr_astar(
                   'SELECT id, source, target, cost, reverse_cost, x1, y1, x2, y2 FROM {graph_table_name}__temp AS r
                    WHERE r.geom_way && ST_MakeEnvelope{str(bbox)}',
                    ARRAY{str(batch)},
                    ARRAY{str(ways_targets)},
                   heuristic := 2)) AS p
                WHERE
                   n.id = p.edge;
                """
            logger.debug(f"SQL-statement:\n{sql}")
            c.execute(sql)

        # Add missing relations for zones sharing identical connectors
        if repair_zones_with_identical_connectors:
            _repair_identical_connectors(
                c, connector_table_name, path_table_name, zone_ids, logger
            )

        # Create geoview
        sql = f"""
            CREATE OR REPLACE VIEW geoview__{path_table_name} AS
            SELECT p.*, g.geom_way
            FROM
                {path_table_name} p,
                {graph_table_name} g
            WHERE
                p.ways_id = g.id"""
        c.execute(sql)

        # Create indexes
        sql = f"""
            CREATE INDEX idx_{path_table_name}_ways_id
            ON {path_table_name}(ways_id)"""
        c.execute(sql)
        sql = f"""
            CREATE INDEX idx_{path_table_name}_vb_from
            ON {path_table_name}(vb_from)"""
        c.execute(sql)
        sql = f"""
            CREATE INDEX idx_{path_table_name}_vb_to
            ON {path_table_name}(vb_to)"""
        c.execute(sql)

        conn.commit()

        logger.info(
            f"Finished creation of shortest path table after "
            f"{datetime.now() - start_time} hours"
        )


def _repair_identical_connectors(
    c: psycopg2.extensions.cursor,
    connector_table_name: str,
    path_table_name: str,
    zone_ids: Iterable[int],
    logger: logging.Logger,
) -> None:
    sql = f"""
        select string_agg(gid::text, '||')
        from {connector_table_name}
        group by ways_id
        having count(*) > 1"""
    c.execute(sql)
    records = c.fetchall()
    logger.info(f"Starting to repair shortest paths for the following zones: {records}")
    for record in records:  # e.g. ("116||120||119||121")
        correct_zone_id = None
        zone_ids_to_repair = [int(x) for x in record[0].split("||")]
        for zone_id in zone_ids_to_repair:
            if zone_id in zone_ids:
                sql = f"""
                    SELECT COUNT(*) 
                    FROM {path_table_name} 
                    WHERE vb_from = {zone_id}"""
                c.execute(sql)
                count = c.fetchone()[0]
                if count > 0:
                    correct_zone_id = zone_id
        for zone_id in zone_ids_to_repair:
            if (zone_id in zone_ids) and (zone_id != correct_zone_id):
                sql = f"""
                    INSERT INTO {path_table_name} (vb_from, vb_to, ways_id)
                    SELECT {zone_id}, vb_to, ways_id
                    FROM {path_table_name}
                    WHERE vb_from = {correct_zone_id}"""
                c.execute(sql)
                sql = f"""
                    INSERT INTO {path_table_name} (vb_from, vb_to, ways_id)
                    SELECT vb_from, {zone_id}, ways_id
                    FROM {path_table_name}
                    WHERE vb_to = {correct_zone_id}"""
                c.execute(sql)
