import os
import logging

import click

# import append_sys_path
from gumore.util.conf import get_absolute_path, get_activate_db_conf_name, get_config
from gumore.util.database import get_db_connection, create_cursor, init_database
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


def import_zones_shapefile_into_database(
    filename: str, zones_table_name: str, source_encoding: str = "UTF-8"
):

    conf = get_config()
    data_path = get_absolute_path(conf, "paths", "data")
    zones_import_path = conf.get("paths", "data_zones_import")
    source_shape_file_path = os.path.join(data_path, zones_import_path, filename)

    init_database()

    logger.info(
        f"Starting import of zones shapefile {source_shape_file_path} "
        f"into table '{zones_table_name}' with source encoding {source_encoding}."
    )

    # create temporary shapefile with correct EPSG code
    temp_shape_file_path = os.path.join(
        data_path, zones_import_path, "_import_" + filename
    )
    command = (
        f"ogr2ogr "
        f'-f "ESRI Shapefile" '
        f"-t_srs EPSG:4326 "
        f"{temp_shape_file_path} "
        f"{source_shape_file_path} "
        f"-lco ENCODING=UTF-8"
    )
    os.system(command)

    # create temporary sql file with correct encoding
    temp_sql_file_name = "_import_" + filename.split(".")[0] + ".sql"
    temp_sql_file_path = os.path.join(data_path, zones_import_path, temp_sql_file_name)
    command = (
        f"shp2pgsql "
        f"-d "
        f"-s 4326 "
        f"-W {source_encoding} "
        f"{temp_shape_file_path} "
        f"{zones_table_name} "
        f"> {temp_sql_file_path}"
    )
    os.system(command)

    #  drop geoview
    conn = get_db_connection()
    c = create_cursor(conn)
    c.execute("DROP VIEW IF EXISTS geoview__zones_corrected")
    conn.commit()

    # import sql file into database
    user = conf.get(get_activate_db_conf_name(), "user")
    db_name = conf.get(get_activate_db_conf_name(), "name")
    db_host = conf.get(get_activate_db_conf_name(), "host")
    db_port = conf.get(get_activate_db_conf_name(), "port")
    command = (
        f"psql -U {user} -d {db_name} -h {db_host} -p {db_port} -q -f "
        f"{temp_sql_file_path}"
    )
    os.system(command)

    # Ensure stable gid numbering
    # Table will be sorted by column 'code' and gid will be an ascending integer serial
    # The original gid column created at import time will be renamed to 'gid_import'
    # This means, that only if the list of zones codes is changed, the gid will be
    # changed. This can happen when adding new zones, deleting old ones etc.
    # Keep in mind, to update other tables, if the gid changes (shortest paths,
    # distance matrix etc.)!
    c.execute(
        f"ALTER TABLE {zones_table_name} ADD CONSTRAINT {zones_table_name}_code_unique "
        f"UNIQUE (code)"
    )
    c.execute(f"ALTER TABLE {zones_table_name} ALTER COLUMN code set NOT NULL")
    c.execute(f"ALTER TABLE {zones_table_name} DROP CONSTRAINT {zones_table_name}_pkey")
    c.execute(f"ALTER TABLE {zones_table_name} RENAME COLUMN gid TO gid_import")
    c.execute(f"ALTER TABLE {zones_table_name} ADD COLUMN gid integer")
    sql = f"""
        UPDATE {zones_table_name}
        SET gid = w.gid
        FROM (  SELECT
                    code, 
                    rank() OVER (order by code ASC) AS gid
                FROM {zones_table_name}
                ) w
        WHERE w.code = {zones_table_name}.code;
        """
    c.execute(sql)
    c.execute(
        f"ALTER TABLE {zones_table_name} ADD CONSTRAINT {zones_table_name}_pkey "
        f"PRIMARY KEY (gid)"
    )
    conn.commit()

    # create spatial index
    sql = f"""
        create index IF NOT EXISTS idx_{zones_table_name}_geom on {zones_table_name} 
        using gist (geom)
        """
    c.execute(sql)
    conn.commit()

    # c.execute(f"vacuum analyze {zones_table_name}")

    # remove temporary files
    temp_files = [
        x
        for x in os.listdir(os.path.join(data_path, zones_import_path))
        if x.startswith("_import_")
    ]
    for temp_file in temp_files:
        os.remove(os.path.join(data_path, zones_import_path, temp_file))

    logger.info("Import done. Temporary files removed.")


def add_centroids_for_zones_in_database(zones_table_name: str, primary_key_column: str):
    logger.info(
        f"Starting centroid creation for {zones_table_name} with primary key "
        f"{primary_key_column}."
    )

    conn = get_db_connection()
    c = create_cursor(conn)

    sql = f"""
        alter table {zones_table_name}
        add column if not exists geom_point geometry(Point, 4326)
        """
    c.execute(sql)

    sql = f"""
        update {zones_table_name}
        set geom_point=sub.geom_point
        from (
            select	
                {primary_key_column}, st_centroid(geom) as geom_point
            from
                {zones_table_name}) as sub
        where {zones_table_name}.{primary_key_column} = sub.{primary_key_column}
        """
    c.execute(sql)

    conn.commit()

    logger.info("Centroid creation finished.")
