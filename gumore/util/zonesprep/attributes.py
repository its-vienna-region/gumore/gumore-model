import logging
import os
from typing import Iterable, Optional

import pandas as pd

# import append_sys_path
from gumore.util.conf import get_absolute_path, get_activate_db_conf_name, get_config
from gumore.util.database import get_db_connection, get_engine, create_cursor
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


def create_dummy_attributes_for_zones_in_database(
    zones_table_name: str,
    integer_columns: Optional[Iterable[str]] = None,
    float_columns: Optional[Iterable[str]] = None,
    boolean_columns: Optional[Iterable[str]] = None,
    categorical_columns: Optional[Iterable[Iterable[str]]] = None,
    random_seed: Optional[float] = None,
    regional_sums_columns: Optional[Iterable[str]] = None,
    create_hotspots: Optional[bool] = False,
):
    assert any((integer_columns, float_columns, boolean_columns, categorical_columns))

    MEAN = 1000
    STD = 800

    conf = get_config()
    zones_region_column_name = conf.get("graph", "zones_region_column_name")

    conn = get_db_connection()
    c = create_cursor(conn)

    logger.info(
        f"Starting creation of dummy attributes in zones table '{zones_table_name}'."
    )

    sql = """
        create extension if not exists tablefunc
        """
    c.execute(sql)
    conn.commit()

    c.execute(f"SELECT setseed({random_seed})")

    if integer_columns:
        for column in integer_columns:
            logger.info(f"Creating column '{column}'")
            sql = f"""
                alter table {zones_table_name}
                add column if not exists {column} integer
                """
            c.execute(sql)

            sql = f"""
                update {zones_table_name} 
                set {column} = abs(sub.r * st_area(geom) * 10)
                from (
                    select 
                        {zones_table_name}.gid,
                        normal_rand(
                            (select count(*)::integer from {zones_table_name}),
                            {MEAN},
                            {STD}) as r
                    from
                        {zones_table_name}
                ) sub
                where 
                    {zones_table_name}.gid = sub.gid

                """
            c.execute(sql)

    if float_columns:
        for column in float_columns:
            logger.info(f"Creating column '{column}'")
            sql = f"""
                alter table {zones_table_name}
                add column if not exists {column} float
                """
            c.execute(sql)

            sql = f"""
                update {zones_table_name} 
                set {column} = abs(sub.r * st_area(geom) * 10)
                from (
                    select 
                        {zones_table_name}.gid,
                        normal_rand(
                            (select count(*)::integer from {zones_table_name}),
                            {MEAN},
                            {STD}) as r
                    from
                        {zones_table_name}
                ) sub
                where 
                    {zones_table_name}.gid = sub.gid

                """
            c.execute(sql)

    if boolean_columns:
        for column in boolean_columns:
            logger.info(f"Creating column '{column}'")
            sql = f"""
                alter table {zones_table_name}
                add column if not exists {column} boolean
                """
            c.execute(sql)

            sql = f"""
                update {zones_table_name} 
                set {column} = sub.r > 0
                from (
                    select 
                        {zones_table_name}.gid,
                        normal_rand(
                            (select count(*)::integer from {zones_table_name}),
                            {-1},
                            {1}) as r
                    from
                        {zones_table_name}
                ) sub
                where 
                    {zones_table_name}.gid = sub.gid

                """
            c.execute(sql)

    if categorical_columns:
        for column in categorical_columns:
            logger.info(f"Creating column '{column}'")
            sql = f"""
                alter table {zones_table_name}
                add column if not exists {column} text
                """
            c.execute(sql)

            sql = f"""
                update {zones_table_name} 
                set {column} = 
                    case 
                        when sub.r < -0.8 then 'A'
                        when sub.r < -0.3 then 'B'
                        when sub.r < 0.3 then 'C'
                        when sub.r < 0.8 then 'D'
                        else 'E'
                    end
                from (
                    select 
                        {zones_table_name}.gid,
                        normal_rand(
                            (select count(*)::integer from {zones_table_name}),
                            {0},
                            {1}) as r
                    from
                        {zones_table_name}
                ) sub
                where 
                    {zones_table_name}.gid = sub.gid

                """
            c.execute(sql)

    if create_hotspots:
        sql = f"""
            alter table {zones_table_name}
            add column if not exists hotspots text
        """
        c.execute(sql)
        sql = f"""
            update {zones_table_name} 
            set hotspots = 
                case 
                    when sub.r < 0.1 then '[[3, [4], [1, 4]], [1, [4], [1, 4]], [3, [10], [8, 10]], [1, [7], [7]], [3, [3], []]]'
                    when sub.r < 0.2 then '[[3, [6], [1, 6]], [3, [6], [1, 6]]]'
                    when sub.r < 0.3 then '[[3, [8], [8]]]'
                    else null
                end
            from (
                select 
                    {zones_table_name}.gid,
                    random() as r
                from
                    {zones_table_name}
            ) sub
            where 
                {zones_table_name}.gid = sub.gid

            """
        c.execute(sql)

    conn.commit()

    logger.info("Dummy attributes created.")

    if regional_sums_columns is not None:
        logger.info("Creating regional sums csv")
        regional_sums_colums_str = f"{regional_sums_columns}"[1:-1].replace("'", "")
        sql = f"""
            select {zones_region_column_name}, {regional_sums_colums_str}
            from {zones_table_name}
        """
        df = pd.read_sql(sql, get_engine())
        df = df.groupby("region").sum()

        data_path = get_absolute_path(conf, "paths", "data")
        data_structural_regional_sums_path = conf.get(
            "paths", "data_structural_regional_sums"
        )
        regional_sums_path = os.path.join(data_path, data_structural_regional_sums_path)
        try:
            os.mkdir(regional_sums_path)
        except FileExistsError:
            pass
        df.to_csv(os.path.join(regional_sums_path, "regional_sums.csv"), sep=";")
        logger.info(
            f"Created regional sums csv: {os.path.join(regional_sums_path, 'regional_sums.csv')}"
        )


def convert_zones_attributes_to_jsonb_in_database(
    zones_table_name: str, attribute_names: Iterable[str], primary_key: str
):
    logger.info(
        f"Starting conversion of zone attributes to jsonb on {attribute_names} "
        f"in table '{zones_table_name}' with primary key {primary_key}."
    )

    conn = get_db_connection()
    c = create_cursor(conn)

    sql = f"""
        alter table {zones_table_name}
        add column if not exists attributes jsonb
        """
    c.execute(sql)

    attributes_string = ", ".join(f"'{x}', {x}" for x in attribute_names)
    sql = f"""
        update {zones_table_name}
        set attributes=sub.attributes
        from (
            select
                {primary_key}, jsonb_build_object({attributes_string}) as attributes
            from {zones_table_name}) as sub
        where {zones_table_name}.{primary_key} = sub.{primary_key}
        """
    c.execute(sql)
    conn.commit()

    logger.info("Conversion finished.")
