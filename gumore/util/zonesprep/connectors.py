import logging
from typing import Iterable

# import append_sys_path
from gumore.util.database import get_db_connection, create_cursor
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


def create_connector_tables_in_database(
    zones_table_name: str,
    modes_of_transport: Iterable[str],
    graph_table_prefix: str,
    zones_primary_key_column: str,
):
    # TODO: Was passiert, wenn keine Schienen (Straßen) in Verkehrszelle liegen?
    # TODO: Bahnhöfe berücksichtigen (?)
    # TODO: Indizes auf id, source und target
    graph_table_name = f"{graph_table_prefix}_2po_4pgr"

    logger.info(
        f"Starting creation of connector tables for zones table '{zones_table_name}' "
        f"with primary key {zones_primary_key_column}, "
        f"for the following modes of transport {modes_of_transport}, "
        f"and this graph table: '{graph_table_name}'"
    )
    conn = get_db_connection()
    c = create_cursor(conn)

    for mode in modes_of_transport:
        connector_table_name = (
            f"connect__{zones_table_name}__to__{graph_table_prefix}_2po_4pgr__{mode}"
        )
        logger.info(
            f"Calculating connectors for mode {mode}. Results will be in table '{connector_table_name}'."
        )
        sql = f"""
            drop table if exists {connector_table_name}
            """
        c.execute(sql)

        sql = f"""
            create table {connector_table_name} as
            select
                {zones_table_name}.{zones_primary_key_column},
                ways.id as ways_id,
                ways.source as ways_source,
                ways.target as ways_target,
                ways.geom_way as geom_way
            from
                {zones_table_name}
                cross join lateral
                    (
                    select 
                        id, source, target, {mode}, geom_way
                    from 
                        {graph_table_name}
                    where 
                        {mode} = True
                    order by 
                        {zones_table_name}.geom_point <-> geom_way
                    limit 1
                    ) as ways
            """
        c.execute(sql)

        sql = f"""
            create index idx_{connector_table_name}_geom_way on {connector_table_name} using gist (geom_way)
            """
        c.execute(sql)

        sql = f"""
            select 
                ways_id
            from {connector_table_name}
            """
        c.execute(sql)

        ids = [x[0] for x in c.fetchall()]
        for id_ in ids:
            sql = f"""
                update
                    {graph_table_name}
                set
                    cost =
                        case when cost * reverse_cost < 0 then
                                  (select greatest(cost, reverse_cost) from {graph_table_name} where id = {id_})
                             else cost
                        end,
                    reverse_cost =
                        case when cost * reverse_cost < 0 then
                                  (select greatest(cost, reverse_cost) from {graph_table_name} where id = {id_})
                             else reverse_cost
                        end
                where
                    id = {id_}
                """
            c.execute(sql)

    conn.commit()

    logger.info("Creation of connector tables finished.")
