from typing import Dict, Set

import pandas as pd


from gumore.util import global_vars

if not global_vars.proj_dir:
    import os

    global_vars.proj_dir = os.path.join(global_vars.projects_path, "default")

from gumore.util.conf import get_config, get_graph_table_name
from gumore.util.database import (
    get_active_schema,
    get_db_connection,
    get_engine,
    create_cursor,
    TABLES,
)
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


def load_count_vs_assignment_to_database():
    logger.info("[ ] Starting count value comparison")

    df = add_assignment_to_counts()
    conn = get_db_connection()
    c = create_cursor(conn)
    c.execute(f"DROP TABLE IF EXISTS {TABLES['counts_vs_assignment']} CASCADE")
    conn.commit()
    df.to_sql("counts_vs_assignment", get_engine(), schema=get_active_schema())

    _create_count_vs_assignment_geoview()

    logger.info("[X] Finished count value comparison")

    return df


def add_assignment_to_counts() -> pd.DataFrame:
    df_count = _get_count_values()
    ways_ids = _get_ways_ids_of_counts(df_count)
    df_assign = _get_assignment_values_for_count_locations(ways_ids)
    mapping = _map_count_vehicle_classes_to_model_classes()
    df_result = _merge_assignment_to_counts(df_count, df_assign, mapping)
    return df_result


def _map_count_vehicle_classes_to_model_classes() -> Dict[str, Dict[str, float]]:
    count_classes = get_config().get("count_values", "count_vehicle_classes").split("|")
    config_options = [
        "count_vehicle_class_to_model_classes_" + str(i + 1)
        for i, _ in enumerate(count_classes)
    ]
    mapping = {}
    for cl, opt in zip(count_classes, config_options):
        weights = [
            x.split(":") for x in get_config().get("count_values", opt).split(",")
        ]
        mapping = {
            **mapping,
            **{cl: {model_class: float(factor) for model_class, factor in weights}},
        }
    return mapping


def _get_count_values() -> pd.DataFrame:
    conf_count = get_config()["count_values"]
    count_table = conf_count.get("table_name_counts_connected")
    columns = [
        conf_count.get("attribute_name_unique_id"),
        conf_count.get("attribute_name_ways_id"),
        conf_count.get("attribute_name_ways_source"),
        conf_count.get("attribute_name_ways_target"),
        conf_count.get("attribute_name_vehicle_class"),
        conf_count.get("attribute_name_count_value"),
    ] + conf_count.get("attribute_names_others").split(",")
    df = pd.read_sql(
        f"select {','.join(columns)} from baseline.{count_table}", get_engine()
    )
    df_col_names = [
        "id",
        "ways_id",
        "ways_source",
        "ways_target",
        "vehicle_class",
        "count_value",
    ] + conf_count.get("attribute_names_others").split(",")
    df.columns = df_col_names

    classes = conf_count.get("count_vehicle_classes").split("|")
    df = df[df.vehicle_class.isin(classes)]
    # TODO: In Config ziehen
    df = df.groupby(["ways_id", "ways_source", "ways_target", "ways_reverse_cost", "vehicle_class"], as_index=False).agg({
        "id": "first",
        "count_value": "sum",
        "ptvzaehlstname": lambda x: "|".join(x.fillna("")),
        "hersteller":  lambda x: "|".join(x.fillna("")),
        "nb_from_ptvzaehlstname": lambda x: "|".join(x.fillna("")),
        "dir_from_ptvzaehlstname": lambda x: "|".join(x.fillna("")),
        "direction_name": lambda x: "|".join(x.fillna("")),
        "richtung": lambda x: "|".join(x.fillna("")),
        "count_dtvms_lkw": "sum",
        "count_dtvms_kfz": "sum",
        "count_dtvms_pab": lambda x: x.sum() if x.notna().all() else None,
        "count_dtvms_servicefahrten": lambda x: x.sum() if x.notna().all() else None,
    })
    return df


def _get_ways_ids_of_counts(df: pd.DataFrame) -> Set[int]:
    return set(df.ways_id.to_list())


def _get_assignment_values_for_count_locations(ways_ids: Set[int]) -> pd.DataFrame:
    sql = f"""
        SELECT ways_id, means, count
        FROM {TABLES['assignment']}
        WHERE ways_id in ({','.join(str(x) for x in ways_ids)})
    """
    df = pd.read_sql(sql, get_engine())
    return df


def _merge_assignment_to_counts(
    df_count: pd.DataFrame,
    df_assign: pd.DataFrame,
    mapping: Dict[str, Dict[str, float]],
) -> pd.DataFrame:
    df_count = df_count.copy()
    df_count["mapping"] = df_count.vehicle_class.apply(lambda x: mapping[x])
    assignment_values = []
    for row in df_count.itertuples():
        df = df_assign[
            (df_assign.ways_id == row.ways_id)
            & (df_assign.means.isin(row.mapping.keys()))
        ]
        if len(df):
            count_for_vehicle_class = df.apply(
                lambda x: x["count"] * row.mapping[x.means], axis=1
            )
            assignment_values.append(count_for_vehicle_class.sum())
        else:
            assignment_values.append(0.0)
    df_count["assignment"] = assignment_values
    df_count["err_rel"] = df_count.assignment / df_count.count_value * 100
    df_count["err_abs"] = df_count.assignment - df_count.count_value
    del df_count["mapping"]
    return df_count


def _create_count_vs_assignment_geoview():
    table_counts_connected = get_config().get("count_values", "table_name_counts_connected")
    id_column = get_config().get("count_values", "attribute_name_unique_id")
    sql = f"""
        CREATE VIEW {TABLES['count_vs_assignment_geoview']} AS
        SELECT v.*, g.geom_way
        FROM 
            -- baseline.{table_counts_connected} c, 
            {TABLES['counts_vs_assignment']} v,
            {get_graph_table_name()} g


        WHERE
            v.ways_id = g.id
    """
    conn = get_db_connection()
    c = create_cursor(conn)
    c.execute(sql)
    conn.commit()


if __name__ == "__main__":

    load_count_vs_assignment_to_database()
