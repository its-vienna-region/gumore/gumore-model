import configparser
import os
import shutil

from pkg_resources import resource_filename

from gumore.util.global_vars import projects_path


def get_proj_path(proj_name) -> str:
    proj_path = os.path.join(projects_path, proj_name)
    return proj_path


def init_project(proj_name: str):
    proj_path = os.path.join(projects_path, proj_name)
    conf_path = os.path.join(proj_path, "config", "gumore.conf")
    data_path = os.path.join(proj_path, "data")
    mymodel_path = os.path.join(proj_path, "mymodel")
    osm2po_path = os.path.join(proj_path, "osm2po")
    _create_dir(proj_path)
    _copy_config_files(proj_path)
    _set_config_options(conf_path, data_path)
    _create_data_dir(conf_path, data_path)
    _create_mymodel_dir(mymodel_path)
    _create_osm2po_dir(osm2po_path)


def _create_dir(proj_path: str):
    if not os.path.exists(projects_path):
        os.mkdir(projects_path)
    if os.path.exists(proj_path):
        raise ValueError(
            f"Project path {proj_path} already exists! Please delete it manually."
        )
    os.mkdir(proj_path)


def _copy_config_files(proj_path):
    FILES_AND_DESTS = [
        ["config/active_config.txt", "config"],
        ["config/gumore.conf", "config"],
        ["config/model_config.xlsx", "config"],
        ["config/osm2po.config", "config"],
        ["config/pw_postgresql.conf", "config"],
    ]

    for fd in FILES_AND_DESTS:
        source_path = resource_filename("gumore", fd[0])
        dest_dir = os.path.join(proj_path, fd[1])
        if not os.path.exists(dest_dir):
            os.mkdir(dest_dir)
        shutil.copy(source_path, dest_dir)


def _set_config_options(conf_path: str, data_path):
    conf = configparser.ConfigParser()
    conf.read(conf_path)
    conf["paths"]["data"] = data_path
    with open(conf_path, "w") as f:
        conf.write(f)


def _create_data_dir(conf_path: str, data_path: str):
    conf = configparser.ConfigParser()
    conf.read(conf_path)
    shutil.copytree(
        os.path.dirname(
            resource_filename(
                "gumore", "data/geo/is50v_mork_umdaemi_svaedi_24122019.shp"
            )
        ),
        os.path.join(data_path, conf.get("paths", "data_zones_import")),
    )
    shutil.copytree(
        os.path.dirname(resource_filename("gumore", "data/flows_emp/is_nst3_tons.csv")),
        os.path.join(data_path, conf.get("paths", "data_emp_flows")),
    )


def _create_mymodel_dir(mymodel_path: str):
    shutil.copytree(
        os.path.dirname(resource_filename("gumore", "data/mymodel/__init__.py")),
        mymodel_path,
    )


def _create_osm2po_dir(osm2po_path: str):
    shutil.copytree(
        os.path.dirname(
            resource_filename("gumore", "data/osm2po/osm2po-core-5.2.43-signed.jar")
        ),
        osm2po_path,
    )


if __name__ == "__main__":
    init_project("test")
