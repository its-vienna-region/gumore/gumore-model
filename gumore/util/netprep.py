"""
Utility functions for preparation of the gumore graph table:

    * Download OSM data
    * Convert it to format which is routable in PostgreSQL
    * Import the data into PostgreSQL
    * Create flags for driving allowances
    * Correct reverse cost (set from 1000000 to -1)
    * Create spatial index

SYNOPSIS

use individual functions::

    download_osm_data("andorra/europe-latest", ".osm.pbf")
    download_osm_data("europe-latest", ".osm.pbf")

    prepare_sql_file_for_net_import("andorra-latest.osm.pbf", "ad")
    prepare_sql_file_for_net_import("europe-latest.osm.pbf", "eu")

    import_osm2po_output_into_database("eu")

    add_boolean_way_resolver_flags_to_graph_table("ad")

    import_zones_shapefile_into_database("vb_merged.shp", "vb", source_encoding="LATIN1")
    import_zones_shapefile_into_database("Regionen_NUTS2_NUTS0_mainzone.SHP", "vb_grob", source_encoding="LATIN1")

    convert_zones_attributes_to_jsonb_in_database("vb", ["inhabitants", "employees"], "gid")

    add_centroids_for_zones_in_database("vb_grob", "gid")

use wrapper function::

    prepare_osm_net("europe/andorra-latest", ".osm.pbf", "ad", ("hgv", "rail"))

"""

import os
import sys
from typing import Iterable

import requests

# import append_sys_path
# from gumore.util import global_vars
#
# if len(sys.argv) == 2:
#     global_vars.proj_dir = sys.argv[1]
# else:
#     global_vars.proj_dir = os.path.join(os.path.split(__file__)[0], "..")
from gumore.util.conf import get_absolute_path, get_activate_db_conf_name, get_config
from gumore.util.database import (
    get_db_connection,
    get_test_db_connection,
    init_database,
    create_cursor,
)
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


@log_tracebacks()
def download_osm_data(region: str, fmt: str) -> str:
    """
    Downloads OSM data from geofabrik.de for the given region in the given format.

    If the file already exists, download is skipped

    The following base url is used: http://download.geofabrik.de/

    Relevant configuration sections::

        [paths]
        data=<PATH>
        data_osm=<PATH>

    Args:
        region (str): region part of the url, e.g. 'europe-latest' or 'europe/andorra-latest'
        fmt (str): format part of the url, e.g. '.osm.pbf'

    Returns:
        filename (str): Name of downloaded file
    """
    base_url = "http://download.geofabrik.de/"
    url = base_url + region + fmt
    conf = get_config()
    data_path = get_absolute_path(conf, "paths", "data")
    data_osm_path = conf.get("paths", "data_osm")
    download_path = os.path.join(data_path, data_osm_path)
    if not os.path.exists(download_path):
        os.mkdir(download_path)
    filename = url.split("/")[-1]
    download_filepath = os.path.join(download_path, filename)

    if os.path.exists(download_filepath):
        logger.warning(
            f"{download_filepath} already exists. Skipping download. Remove or rename file if you want "
            f"to download the latest osm data."
        )
    else:
        logger.info(
            f"Starting download from {url}. File will be written to {download_filepath}."
        )
        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            with open(download_filepath, "wb") as f:
                for chunk in r.iter_content(chunk_size=16384):
                    if chunk:
                        f.write(chunk)
        logger.info("Download complete.")

    return filename


@log_tracebacks()
def prepare_sql_file_for_net_import(pbf_file_name: str, prefix: str):
    """
    Converts an OSM pbf-file into a format, which is interpretable by pgrouting and store it in an sql file.

    The created sql file can be imported into PostgreSQL, e.g. by using psql.

    Depends on osm2po (https://osm2po.de/), which needs to be correctly installed in advance.

    Relevant configuration sections::

        [paths]
        data=<DIRECTORY_PATH>
        data_osm=<DIRECTORY_PATH>
        data_osm2po=<DIRECTORY_PATH>
        osm2po_jar=<PATH_TO_JAR_FILE>

        [model_config]
        osm2po_path=<PATH_TO_CONFIG_FILE>

    osm2po is configured in the config file, which is referenced in config section model_config/osm2po_path.

    Args:
        pbf_file_name (str): Name of the pbf-file to be converted
        prefix (str): Prefix used for creation of output directory

    Raises:
        IOError: if osm2po output path (including prefix) exists
    """
    conf = get_config()
    osm2po_jar_path = get_absolute_path(conf, "paths", "osm2po_jar")
    data_path = get_absolute_path(conf, "paths", "data")
    data_osm_path = conf.get("paths", "data_osm")
    data_osm2po_path = conf.get("paths", "data_osm2po")
    pbf_path = os.path.join(data_path, data_osm_path, pbf_file_name)
    osm2po_output_path = os.path.join(data_path, data_osm2po_path)
    output_path_with_prefix = os.path.join(osm2po_output_path, prefix)
    assert os.path.exists(pbf_path)
    if os.path.exists(output_path_with_prefix):
        raise IOError(
            f"{output_path_with_prefix} already exists. Please remove it manually!"
        )
    if not os.path.exists(osm2po_output_path):
        os.mkdir(osm2po_output_path)

    command = (
        f"java -Xmx1208m -jar {osm2po_jar_path} "
        f"cmd=tjspg "
        f"prefix={prefix} "
        f'workDir="{output_path_with_prefix}" '
        f"tileSize=15,15,1 "
        f'"{pbf_path}" '
        f"postp.0.class=de.cm.osm2po.plugins.postp.PgRoutingWriter"
    )
    logger.info(f"Running osm2po with command: {command}")
    os.system(command)

    sql_filename = prefix + "_2po_4pgr.sql"
    logger.info(
        f"osm2po processing finished. Output can be found here: {output_path_with_prefix}. The SQL file "
        f"for database import is called {sql_filename}"
    )


@log_tracebacks()
def import_osm2po_output_into_database(prefix: str, test_database: bool = False):
    """
    Imports osm2po output (an sql-file) into the active gumore database.

    Depends on psql.

    Relevant configuration sections::

        [paths]
        data=<PATH>
        data_osm2po=<PATH>

    Depends on PostgreSQL passfile (e.g. 'AppData/Roaming/postgresql/pgpass.conf').
    See https://www.postgresql.org/docs/10/libpq-pgpass.html

    Args:
        prefix (str): Prefix used by osm2po to create result directory to get input file. The prefix
            will also be used for creation of database table.
    """
    conf = get_config()
    data_path = get_absolute_path(conf, "paths", "data")
    data_osm2po_path = conf.get("paths", "data_osm2po")
    osm2po_output_path = os.path.join(data_path, data_osm2po_path)
    output_path_with_prefix = os.path.join(osm2po_output_path, prefix)
    sql_filename = prefix + "_2po_4pgr.sql"
    sql_filepath = os.path.join(output_path_with_prefix, sql_filename)

    init_database()

    user = conf.get(get_activate_db_conf_name(), "user")
    if not test_database:
        db_conf_name = get_activate_db_conf_name()
    else:
        db_conf_name = "database." + conf.get("database", "test_db_conf_name")
    db_name = conf.get(db_conf_name, "name")
    db_host = conf.get(db_conf_name, "host")
    db_port = conf.get(db_conf_name, "port")

    command = (
        f"psql -U {user} -d {db_name} -h {db_host} -p {db_port} -q -f {sql_filepath}"
    )
    logger.info(f"Importing into database with following command: {command}")
    os.system(command)
    logger.info("Import finished")


@log_tracebacks()
def add_boolean_way_resolver_flags_to_graph_table(
    prefix: str, indices: Iterable[str] = ("hgv", "rail"), test_database: bool = False
):
    """
    Adds boolean drive allowance flags to the gumore graph table to improve readability for humans.

    This is done by converting the bit-flags, which are created by osm2po.

    Args:
        prefix (str): Prefix of gumore graph table.
        indices (Iterable of str, default: ('hgv', 'rail')): Column names for which indices will be created.
    """
    if not test_database:
        conn = get_db_connection()
    else:
        conn = get_test_db_connection()
    c = create_cursor(conn)

    logger.info(
        f"Adding boolean way resolver tags to {prefix}_2po_4pgr for the following columns: {indices}"
    )
    sql = f"""
        alter table {prefix}_2po_4pgr
            add column car boolean,
            add column hgv boolean,
            add column rail boolean,
            add column ferry boolean,
            add column bike boolean,
            add column foot boolean,
            add column poly boolean
        """
    c.execute(sql)

    sql = f"""
        update {prefix}_2po_4pgr
        set
            car = substring(flags::bit(10),10,1)::int::boolean,
            hgv = substring(flags::bit(10),9,1)::int::boolean,
            rail = substring(flags::bit(10),8,1)::int::boolean,
            ferry = substring(flags::bit(10),7,1)::int::boolean,
            bike = substring(flags::bit(10),6,1)::int::boolean,
            foot = substring(flags::bit(10),5,1)::int::boolean,
            poly = substring(flags::bit(10),4,1)::int::boolean
        """
    c.execute(sql)

    for index in indices:
        sql = f"""
            create index idx_{prefix}_2po_4pgr_{index} on {prefix}_2po_4pgr ({index})
        """
        c.execute(sql)

    conn.commit()
    logger.info("Complete")


@log_tracebacks()
def correct_reverse_cost(prefix: str, test_database: bool = False):
    """
    Corrects the reverse costs in the graph table from 1000000 to -1.

    osm2po sets reverse costs for restricted links to 1000000. Pgrouting (pgr_dijkstraCost) excpects -1 for
    restricted links.

    Args:
        prefix (str): prefix of graph database table
    """
    if not test_database:
        conn = get_db_connection()
    else:
        conn = get_test_db_connection()
    c = create_cursor(conn)

    logger.info(
        f"Setting reverse costs of 1000000 to -1 in graph table '{prefix}_2po_4pgr'."
    )
    sql = f"""
        update {prefix}_2po_4pgr
        set reverse_cost = -1
        where reverse_cost = 1000000
        """
    c.execute(sql)
    rows_n = c.rowcount
    conn.commit()
    logger.info(f"Reverse cost correction for {rows_n} rows finished.")


@log_tracebacks()
def create_spatial_index(prefix: str, test_database: bool = False):
    if not test_database:
        conn = get_db_connection()
    else:
        conn = get_test_db_connection()
    c = create_cursor(conn)

    logger.info(
        f"Creating spatial index for graph table '{prefix}_2po_4pgr', geometry column 'geom_way'."
    )
    sql = f"""
        create index idx_{prefix}_2po_4pgr_geom_way on {prefix}_2po_4pgr using gist (geom_way)
        """
    c.execute(sql)
    conn.commit()

    sql = f"""
        vacuum analyze {prefix}_2po_4pgr
        """
    try:
        conn.autocommit = True
        c.execute(sql)
    except Exception as e:
        raise e
    finally:
        conn.autocommit = False

    logger.info("Spatial index created, table analyzed and vacuumized.")


@log_tracebacks()
def prepare_osm_net(
    region: str,
    fmt: str,
    prefix: str,
    indices: Iterable[str],
    test_database: bool = False,
):
    """
    Wrapper function for download, conversion, import, boolean flags creation, reverse cost correction,
    and spatial index creation.

    Relevant configuration sections::

        [paths]
        data=<DIRECTORY_PATH>
        data_osm=<DIRECTORY_PATH>
        data_osm2po=<DIRECTORY_PATH>
        osm2po_jar=<PATH_TO_JAR_FILE>

        [model_config]
        osm2po_path=<PATH_TO_CONFIG_FILE>

    Args:
        region (str): region part of the url, e.g. 'europe-latest' or 'europe/andorra-latest'
        fmt (str): format part of the url, e.g. '.osm.pbf'
        prefix (str): Prefix used by osm2po to create result directory to get input file. The prefix
            will also be used for creation of database table.
        indices (Iterable of str, default: ('hgv', 'rail')): Column names for which indices will be created.
    """
    logger.info("Starting prepare_osm_net wrapper function.")
    _check_paths(prefix)
    pbf_file_name = download_osm_data(region, fmt)
    prepare_sql_file_for_net_import(pbf_file_name, prefix)
    import_osm2po_output_into_database(prefix, test_database=test_database)
    add_boolean_way_resolver_flags_to_graph_table(
        prefix, indices, test_database=test_database
    )
    correct_reverse_cost(prefix, test_database=test_database)
    create_spatial_index(prefix, test_database=test_database)
    logger.info("prepare_osm_net wrapper function finished")


@log_tracebacks()
def _check_paths(prefix):
    """
    Creates osm2po output path if not yet done. Checks if osm2po output path (including prefix) already exists.

    Relevant configuration sections::

        [paths]
        data=<DIRECTORY_PATH>
        data_osm2po=<DIRECTORY_PATH>

    Args:
        prefix (str): Prefix used by osm2po to create result directory to get input file. The prefix
            will also be used for creation of database table.

    Raises:
        IOError: if osm2po output path (including prefix) exists
    """
    conf = get_config()
    data_path = get_absolute_path(conf, "paths", "data")
    data_osm2po_path = conf.get("paths", "data_osm2po")
    osm2po_output_path = os.path.join(data_path, data_osm2po_path)
    output_path_with_prefix = os.path.join(osm2po_output_path, prefix)
    if os.path.exists(output_path_with_prefix):
        raise IOError(
            f"{output_path_with_prefix} already exists. Please remove it manually!"
        )
    if not os.path.exists(osm2po_output_path):
        os.mkdir(osm2po_output_path)


if __name__ == "__main__":

    # pass
    # SYNOPSIS: use individual functions
    download_osm_data("europe/andorra-latest", ".osm.pbf")
    # download_osm_data("europe-latest", ".osm.pbf")

    prepare_sql_file_for_net_import("andorra-latest.osm.pbf", "ad")
    # prepare_sql_file_for_net_import("europe-latest.osm.pbf", "eu")

    # import_osm2po_output_into_database("eu")

    # add_boolean_way_resolver_flags_to_graph_table("ad")

    # correct_reverse_cost("eu")

    # create_spatial_index("eu_dense")

    # SYNOPSIS: use wrapper function
    # prepare_osm_net("europe/andorra-latest", ".osm.pbf", "ad", ("hgv", "rail"))
    # prepare_osm_net("europe-latest", ".osm.pbf", "eu_dense", ("hgv", "rail"))
