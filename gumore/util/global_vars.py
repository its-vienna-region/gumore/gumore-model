import os

parent_pid = False

# project directory
proj_dir = None

# config
config = None

# matrices from model config
# dictionary values shall be of type pandas.DataFrame
matrices_model_config = {}

# defaults
projects_path = os.path.join(os.path.expanduser("~"), "GumoreProjects")

# hotspots
size_to_factor = {}
