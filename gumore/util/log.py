from functools import wraps
import logging
import os.path

# import append_sys_path
import gumore.util.conf


def initialize_logger():
    """
    Initializes logger as defined in config. Adds handlers for different log message levels.

    Returns:
        None

    """
    conf = gumore.util.conf.get_config()

    if conf is not None:
        logger = logging.getLogger()
        logger.setLevel(conf["logging"]["level"])

        log_dir = gumore.util.conf.get_absolute_path(conf, "paths", "log")
        try:
            os.mkdir(log_dir)
        except FileExistsError:
            pass

        # add handlers if not yet done
        if not len(logger.handlers):

            # create console handler
            if conf["logging.console"]["active"] == "True":
                handler = logging.StreamHandler()
                handler.setLevel(conf["logging.console"]["level"])
                formatter = logging.Formatter(
                    conf["logging.console"]["format"],
                    datefmt=conf["logging.console"]["datefmt"],
                )
                handler.setFormatter(formatter)

                logger.addHandler(handler)

            log_prefix = gumore.util.conf.get_activate_db_conf_name().split(".")[-1]

            # create error file handler
            if conf["logging.error"]["active"] == "True":
                handler = logging.FileHandler(
                    os.path.join(
                        gumore.util.conf.get_absolute_path(conf, "paths", "log"),
                        log_prefix + "_" + conf["logging.error"]["filename"],
                    ),
                    "a",
                    encoding="UTF-8",
                    delay="true",
                )
                handler.setLevel(conf["logging.error"]["level"])
                formatter = logging.Formatter(
                    conf["logging.error"]["format"],
                    datefmt=conf["logging.error"]["datefmt"],
                )
                handler.setFormatter(formatter)
                logger.addHandler(handler)

            # create debug file handler
            if conf["logging.debug"]["active"] == "True":
                handler = logging.FileHandler(
                    os.path.join(
                        gumore.util.conf.get_absolute_path(conf, "paths", "log"),
                        log_prefix + "_" + conf["logging.debug"]["filename"],
                    ),
                    "a",
                    encoding="UTF-8",
                )
                handler.setLevel(conf["logging.debug"]["level"])
                formatter = logging.Formatter(
                    conf["logging.debug"]["format"],
                    datefmt=conf["logging.debug"]["datefmt"],
                )
                handler.setFormatter(formatter)
                logger.addHandler(handler)


def log_tracebacks():
    def log_tracebacks_decorator(func):
        @wraps(func)
        def func_wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception:
                logger = logging.getLogger(func.__qualname__)
                logger.exception("Fatal error")
                raise

        return func_wrapper

    return log_tracebacks_decorator
