import configparser
import psycopg2
from psycopg2 import sql

# import append_sys_path
from gumore.util.conf import get_absolute_path, get_activate_db_conf_name, get_config
from gumore.util.database import (
    get_db_connection,
    create_cursor,
    get_test_db_connection,
)
from gumore.util.netprep import prepare_osm_net
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


def create_test_database():
    conf = get_config()
    test_db_conf_name = "database." + conf.get("database", "test_db_conf_name")
    active_db_conf_name = "database." + conf.get("database", "active_db_conf_name")
    assert conf.get(test_db_conf_name, "name") != conf.get(active_db_conf_name, "name")

    try:
        host = conf.get(test_db_conf_name, "host")
        port = conf.getint(test_db_conf_name, "port")
        test_dbname = conf.get(test_db_conf_name, "name")
        user = conf.get(test_db_conf_name, "user")
        password = conf.get(test_db_conf_name, "password")
    except configparser.NoOptionError as e:
        pw_filename = conf.get(test_db_conf_name, "password_file")
        raise ValueError(
            f"No test database found. Add the password file '{pw_filename}'."
        )

    con = psycopg2.connect(
        dbname="postgres", user=user, host=host, password=password, port=port
    )
    con.autocommit = True
    cur = con.cursor()
    cur.execute(f"CREATE DATABASE {test_dbname}")
    cur.close()
    con.close()
    logger.info(f"Test database {test_dbname} successfully created.")


def create_db_extensions():
    con = get_test_db_connection()
    cur = con.cursor()
    cur.execute("CREATE EXTENSION IF NOT EXISTS postgis")
    cur.execute("CREATE EXTENSION IF NOT EXISTS pgrouting")
    cur.execute("CREATE EXTENSION IF NOT EXISTS tablefunc")
    con.commit()
    cur.close()
    con.close()


def create_net():
    prepare_osm_net(
        "europe/iceland-latest", ".osm.pbf", "is", ("hgv", "rail"), test_database=True
    )


if __name__ == "__main__":
    # create_test_database()
    # create_db_extensions()
    create_net()
