#!/usr/bin/env python3

""" This is the main program for the very(!) first prototype
implementation for the traffic generation of the project Guemore.
Call this file with no arguments for the test run. """

from multiprocessing import current_process
import os
import sys

import click

from gumore.util.conf import package_path
from gumore.util import global_vars

size_to_factor = {}

if len(sys.argv) == 2:
    if not global_vars.proj_dir:
        global_vars.proj_dir = os.path.join(
            os.path.expanduser("~"), "GumoreProjects", sys.argv[1]
        )
    from gumore.util.conf import check_db_config

    if __name__ == "__main__":
        global_vars.parent_pid = current_process().pid

    check_db_config()

    import gumore.core.rules.means
    import gumore.core.runner
    import gumore.util.database
    from gumore.util import global_vars
    from gumore.util.log import *

    initialize_logger()
    logger = logging.getLogger(__name__)

    # The following hack is needed for pickling objects as preparation for parallel
    # execution. This one does not work: from gumore.model.rules import *
    package_path = os.path.join(global_vars.proj_dir, package_path(), "rules.py")
    is_main_process = current_process().pid == global_vars.parent_pid
    if is_main_process:
        logger.info(f"Importing rules.py from package path {package_path}.")
    exec(compile(source=open(package_path, encoding="UTF-8").read(), filename="rules.py", mode="exec"))

@click.command(name="gm-run")
@click.argument("project-name")
def run(project_name):
    try:
        global_vars.proj_dir = project_name
        global_vars.size_to_factor = size_to_factor
        if os.environ.get("GUMORE_CREATE_SPHINX_DOC") != "True":
            gumore.util.database.init_tables()
        app = gumore.core.runner.gumore_app
        app.run()
    except Exception:
        logger.exception("Fatal error")


if __name__ == "__main__":
    run()
