"""

"""

import os
import sys

sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..")))
sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..", "gumore", "util")))

import argparse

from gumore.util.zonesprep import create_connector_tables_in_database

parser = argparse.ArgumentParser(
    description="Creates a new connector table, i.e. a mapping between zones and "
    "their connectors.",
    epilog="Example usage: python create_connector_tables.py vb_nuts1 gid "
    "eu_dense hgv rail",
)
parser.add_argument(
    "zones_table_name", help="Name of zones table (e.g. 'vb_nuts1').", type=str
)
parser.add_argument(
    "zones_primary_key_column",
    help="Name of column containing the " "primary key (e.g. 'gid').",
    type=str,
)
parser.add_argument(
    "graph_table_prefix",
    help="Prefix (without ending underscore) of graph table. This "
    "prefix is set when graph data is imported (e.g. 'eu_dense').",
)
parser.add_argument(
    "modes_of_transport",
    nargs="+",
    help="One or more mode(s) of transport. Use the codes created "
    "during graph import by osm2po (e.g. 'hgv rail')",
    type=str,
)
args = parser.parse_args()


def run():
    create_connector_tables_in_database(
        zones_table_name=args.zones_table_name,
        modes_of_transport=args.modes_of_transport,
        graph_table_prefix=args.graph_table_prefix,
        zones_primary_key_column=args.zones_primary_key_column,
    )


if __name__ == "__main__":
    run()
