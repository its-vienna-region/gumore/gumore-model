import os

# import sys

import click

# sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..")))
# sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..", "gumore", "util")))


@click.group()
def cli():
    pass


@cli.command(name="import")
@click.option(
    "-f",
    "--filename",
    required=True,
    help="Name of the Shapefile, e.g. 'is50v_mork_umdaemi_svaedi_24122019.shp'",
)
@click.option(
    "-z",
    "--zones-table-name",
    required=True,
    help="Name of the zones table which will be created in the database, e.g. 'zones'",
)
@click.option(
    "-e",
    "--source-encoding",
    default="UTF-8",
    show_default=True,
    help="Encoding of the Shapefile, e.g. 'LATIN-1'",
)
@click.argument("project-name")
def import_zones(filename, zones_table_name, source_encoding, project_name):
    """
    Import a Shapefile representing traffic zones into the database.

    Relevant configuration sections::

        \b
        [paths]
        data_zones_import=<DIRECTORY_PATH>

    Example usage::

        \b
        gm-zonesprep import -f is50v_mork_umdaemi_svaedi_24122019.shp -z zones demo

    """
    from gumore.util import global_vars

    global_vars.proj_dir = os.path.join(global_vars.projects_path, project_name)
    from gumore.util.zonesprep import import_zones_shapefile_into_database

    import_zones_shapefile_into_database(filename, zones_table_name, source_encoding)


@cli.command(name="centroids")
@click.option(
    "-z", "--zones-table-name", required=True, help="Name of zones table, e.g. 'zones'"
)
@click.option(
    "-p",
    "--primary-key-column",
    required=True,
    help="Zones table's primary key column name, e.g. 'gid'",
)
@click.argument("project-name")
def centroids(zones_table_name, primary_key_column, project_name):
    """
    Example Usage::

        gm-zonesprep centroids -z zones -p gid demo

    """
    from gumore.util import global_vars

    global_vars.proj_dir = os.path.join(global_vars.projects_path, project_name)
    from gumore.util.zonesprep import add_centroids_for_zones_in_database

    add_centroids_for_zones_in_database(zones_table_name, primary_key_column)


@cli.command(name="connectors")
@click.option(
    "-z", "--zones-table-name", required=True, help="Name of zones table, e.g. 'zones'"
)
@click.option(
    "-m",
    "--modes-of-transport",
    required=True,
    multiple=True,
    help="Name of the mode of transport, e.g. 'hgv'. Can be repeated.",
)
@click.option(
    "-g",
    "--graph-table-prefix",
    required=True,
    help="Prefix of the graph table, e.g. 'is'",
)
@click.option(
    "-p",
    "--zones-primary-key-column",
    required=True,
    help="Zones table's primary key column name, e.g. 'gid'",
)
@click.argument("project-name")
def connectors(
    zones_table_name,
    modes_of_transport,
    graph_table_prefix,
    zones_primary_key_column,
    project_name,
):
    """
    Set connector links for zones.

    Example usage::

        \b
        gm-zonesprep connectors -z zones -m hgv -g is -p gid demo

    """
    from gumore.util import global_vars

    global_vars.proj_dir = os.path.join(global_vars.projects_path, project_name)
    from gumore.util.zonesprep import create_connector_tables_in_database

    create_connector_tables_in_database(
        zones_table_name,
        modes_of_transport,
        graph_table_prefix,
        zones_primary_key_column,
    )


@cli.command(name="dummy-attributes")
@click.option(
    "-z", "--zones-table-name", required=True, help="Name of zones table, e.g. 'zones'"
)
@click.option(
    "-i",
    "--integer-column",
    "integer_columns",
    multiple=True,
    help="Name of integer column, e.g. 'population'. Can be repeated.",
)
@click.option(
    "-f",
    "--float-column",
    "float_columns",
    multiple=True,
    help="Name of float column, e.g. 'brp'. Can be repeated.",
)
@click.option(
    "-b",
    "--boolean-column",
    "boolean_columns",
    multiple=True,
    help="Name of boolean column, e.g. 'has_hub'. Can be repeated.",
)
@click.option(
    "-c",
    "--categorical-column",
    "categorical_columns",
    multiple=True,
    help="Name of categorical column, e.g. 'land_usage'. Can be repeated.",
)
@click.option(
    "-r",
    "--random-seed",
    type=click.IntRange(0, 10000),
    help="Random seed. Has to be in the range of 0 to 10000.",
)
@click.argument("project-name")
def dummy_arguments(
    zones_table_name,
    integer_columns,
    float_columns,
    boolean_columns,
    categorical_columns,
    random_seed,
    project_name,
):
    # TODO: Add new params
    """
    Create dummy attributes for zones table.

    Example usage::

        \b
        gm-zonesprep dummy-attributes -z zones -i population -i a -i b -i c -i d -b has_hub -r 0 demo

    """
    from gumore.util import global_vars

    global_vars.proj_dir = os.path.join(global_vars.projects_path, project_name)
    from gumore.util.zonesprep import create_dummy_attributes_for_zones_in_database

    random_seed = random_seed / 5000.0 - 1

    create_dummy_attributes_for_zones_in_database(
        zones_table_name,
        integer_columns,
        float_columns,
        boolean_columns,
        categorical_columns,
        random_seed,
    )


@cli.command(name="paths")
@click.option(
    "-z", "--zones-table-name", required=True, help="Name of zones table, e.g. 'zones'"
)
@click.option(
    "-m",
    "--mode-of-transport",
    "modes_of_transport",
    required=True,
    multiple=True,
    help="Name of the mode of transport, e.g. 'hgv'. Can be repeated.",
)
@click.option(
    "-g",
    "--graph-table-prefix",
    required=True,
    help="Prefix of graph table, e.g. 'eu_dense'",
)
@click.option(
    "-p",
    "--zones-primary-key-column",
    required=True,
    help="Primary key column name of zones table, e.g. 'gid'",
)
@click.option(
    "-bb",
    "--bbox",
    type=(float, float, float, float),
    default=(9.47, 46.43, 17.2, 49.04),
    show_default=True,
    help="Bounding box for clipping the graph in the form: SW-LONG SW-LAT NE-LONG NE-LAT. Defaults to bbox of Austria.",
)
@click.option(
    "-bs",
    "--batch-size",
    type=int,
    default=10,
    show_default=True,
    help="Number of source cells in one batch run of the `pgr_astar` algorithm. "
    "This will highly impact memory usage during calculation.",
)
@click.option(
    "-w",
    "--zones-where-clause",
    default="",
    show_default=True,
    help="SQL clause to select zones for which the shortest paths shall be calculated, e.g. \"WHERE land = 'AT'\".",
)
@click.option(
    "-r",
    "--repair-zones-with-identical-connectors",
    type=bool,
    default=True,
    show_default=True,
    help="Needs to be TRUE if you have zones sharing identical connectors.",
)
@click.option(
    "-d",
    "--debug",
    type=bool,
    default=False,
    show_default=True,
    help="Adds the prefix 'debug__' to the created shortest path table(s) to avoid overriding existing tables.",
)
@click.argument("project-name")
def paths(
    zones_table_name,
    modes_of_transport,
    graph_table_prefix,
    zones_primary_key_column,
    bbox,
    batch_size,
    zones_where_clause,
    repair_zones_with_identical_connectors,
    debug,
    project_name,
):
    """
    (Re-)creates a table with all shortest paths between all zones for each mode.

    The shortest paths are calculated using `pgrouting`'s `pgr_astar` function. The
    graph is clipped to the extent of the bounding box to speed up calculation. The
    calculation is split up into batches of source cells to limit the memory usage.
    If you are running out of memory, try to reduce `batch_size` (which will increase
    running time).

    The created table(s) will start with the prefix 'path__' or 'debug__path__'.

    Example usage::

        \b
        gm-zonesprep paths -z zones -m hgv -g is -p gid -bb -24.3 63.4 -13.7 66.6 demo
    """
    from gumore.util import global_vars

    global_vars.proj_dir = os.path.join(global_vars.projects_path, project_name)
    from gumore.util.zonesprep import create_shortest_path_table_in_database

    create_shortest_path_table_in_database(
        zones_table_name,
        modes_of_transport,
        graph_table_prefix,
        zones_primary_key_column,
        bbox,
        batch_size,
        zones_where_clause,
        repair_zones_with_identical_connectors,
        debug,
    )


@cli.command(name="all")
@click.option(
    "-f",
    "--filename",
    required=True,
    help="Name of the Shapefile, e.g. 'is50v_mork_umdaemi_svaedi_24122019.shp'",
)
@click.option(
    "-z",
    "--zones-table-name",
    required=True,
    help="Name of the zones table which will be created in the database, e.g. 'zones'",
)
@click.option(
    "-e",
    "--source-encoding",
    default="UTF-8",
    show_default=True,
    help="Encoding of the Shapefile, e.g. 'LATIN-1'",
)
@click.option(
    "-p",
    "--primary-key-column",
    required=True,
    help="Zones table's primary key column name, e.g. 'gid'",
)
@click.option(
    "-m",
    "--modes-of-transport",
    required=True,
    multiple=True,
    help="Name of the mode of transport, e.g. 'hgv'. Can be repeated.",
)
@click.option(
    "-g",
    "--graph-table-prefix",
    required=True,
    help="Prefix of the graph table, e.g. 'is'",
)
@click.option(
    "-bb",
    "--bbox",
    type=(float, float, float, float),
    default=(9.47, 46.43, 17.2, 49.04),
    show_default=True,
    help="Bounding box for clipping the graph in the form: SW-LONG SW-LAT NE-LONG NE-LAT. Defaults to bbox of Austria.",
)
@click.option(
    "-bs",
    "--batch-size",
    type=int,
    default=10,
    show_default=True,
    help="Number of source cells in one batch run of the `pgr_astar` algorithm. "
    "This will highly impact memory usage during calculation.",
)
@click.option(
    "-w",
    "--zones-where-clause",
    default="",
    show_default=True,
    help="SQL clause to select zones for which the shortest paths shall be calculated, e.g. \"WHERE land = 'AT'\".",
)
@click.option(
    "-r",
    "--repair-zones-with-identical-connectors",
    type=bool,
    default=True,
    show_default=True,
    help="Needs to be TRUE if you have zones sharing identical connectors.",
)
@click.option(
    "-d",
    "--add_dummy_attributes",
    type=bool,
    default=False,
    show_default=True,
    help="Add dummy structural data to zones table.",
)
@click.argument("project-name")
def prepare(
    filename,
    zones_table_name,
    source_encoding,
    primary_key_column,
    modes_of_transport,
    graph_table_prefix,
    bbox,
    batch_size,
    zones_where_clause,
    repair_zones_with_identical_connectors,
    add_dummy_attributes,
    project_name,
):
    """
    Wrapper function

    Example usage::

        \b
        gm-zonesprep all -f is50v_mork_umdaemi_svaedi_24122019.shp -z zones -p gid -m hgv -g is -bb -24.3 63.4 -13.7 66.6 -d True demo

    """
    from gumore.util import global_vars

    global_vars.proj_dir = os.path.join(global_vars.projects_path, project_name)
    from gumore.util.zonesprep import (
        add_centroids_for_zones_in_database,
        create_connector_tables_in_database,
        create_dummy_attributes_for_zones_in_database,
        create_shortest_path_table_in_database,
        import_zones_shapefile_into_database,
    )

    import_zones_shapefile_into_database(filename, zones_table_name, source_encoding)
    add_centroids_for_zones_in_database(zones_table_name, primary_key_column)
    create_connector_tables_in_database(
        zones_table_name, modes_of_transport, graph_table_prefix, primary_key_column
    )
    create_shortest_path_table_in_database(
        zones_table_name,
        modes_of_transport,
        graph_table_prefix,
        primary_key_column,
        bbox,
        batch_size,
        zones_where_clause,
        repair_zones_with_identical_connectors,
    )

    if add_dummy_attributes:
        create_dummy_attributes_for_zones_in_database(
            zones_table_name=zones_table_name,
            integer_columns=("population", "a", "b", "c", "d"),
            boolean_columns=("has_hub",),
            random_seed=0,
            regional_sums_columns=("population", "a", "b", "c", "d"),
            create_hotspots=True,
        )


@cli.command(name="distmat")
@click.option(
    "-n",
    "--new-distmat-table-name",
    required=True,
    help="Name of database table to create, e.g. 'cost_zones_is_hgv_km'",
)
@click.option(
    "-r",
    "--resistance-type",
    required=True,
    type=click.Choice(["travel_time", "distance"], case_sensitive=False),
    help="Type of resistance to calculate costs.",
)
@click.option(
    "-m",
    "--means-of-transport",
    required=True,
    type=click.Choice(["hgv", "train"], case_sensitive=False),
    help="Means of transport for which costs are calculated.",
)
@click.option(
    "-c",
    "--connector-table-name",
    required=True,
    help="Name of database table containing the mapping zone -> connector, e.g. "
    "'connect__zones__to__is_2po_4pgr__hgv'",
)
@click.option(
    "-d",
    "--directed",
    type=bool,
    default=True,
    show_default=True,
    help="True if graph is directed.",
)
@click.option(
    "-e",
    "--delete-existing-table",
    type=bool,
    default=False,
    show_default=True,
    help="If True, delete distance matrix table in database, if it already exists.",
)
@click.option(
    "-s",
    "--create-sample-table",
    type=bool,
    default=False,
    show_default=True,
    help="If True, create distance matrix only for a sample of all possible relations. "
    "Cannot be used, if --zones-where-clause is set.",
)
@click.option(
    "-w",
    "--zones-where-clause",
    default="",
    show_default=True,
    help="SQL clause to select zones for which the distances shall be calculated, "
    'e.g. "WHERE vb.gid IN (SELECT gid FROM zones WHERE ew > 20000)". Cannot '
    "be used if --create-sample-table is True.",
)
@click.argument("project-name")
def calc_distmat(
    new_distmat_table_name,
    resistance_type,
    means_of_transport,
    connector_table_name,
    directed,
    delete_existing_table,
    create_sample_table,
    zones_where_clause,
    project_name,
):
    """
    Calculates a new distance matrix with pg_routing (pgr_dijkstraCost) and stores it in the database.

    The name of the graph table needs to be defined in the database config. The table needs to have the
    following fields: id, source, target, cost, reverse_cost, km.

    The created distance matrix table has the following fields:
    vb_from_id, vb_to_id, start_vid, end_vid, dijk_cost

    Example usage::

        \b
        gm-zonesprep distmat -n cost_zones_is_hgv_hours -r travel_time -m hgv -c connect__zones__to__is_2po_4pgr__hgv demo

        gm-zonesprep distmat -n cost_zones_is_hgv_km -r distance -m hgv -c connect__zones__to__is_2po_4pgr__hgv demo

    """
    from gumore.util import global_vars

    global_vars.proj_dir = os.path.join(global_vars.projects_path, project_name)
    from gumore.core.graph import Distmat

    dm = Distmat()
    dm.create_in_db(
        new_distmat_table_name=new_distmat_table_name,
        resistance_type=resistance_type,
        means_of_transport=means_of_transport,
        connector_table_name=connector_table_name,
        directed=directed,
        delete_existing_table=delete_existing_table,
        create_sample_table=create_sample_table,
        zones_where_clause=zones_where_clause,
    )

    print("\nLoading result from database to check if everything is OK.")

    dm.reset()
    dm.read_from_db(distmat_table_name=new_distmat_table_name, recreate_pickle=True)

    print("\nCreated Distmat object")
    print("======================")
    print(dm)

    print("\nDistmat Dataframe")
    print("=================")
    print(dm.df)

    print("\nDataframe holds NA-values")
    print("=========================")
    print(dm.holds_na)

    if dm.holds_na:
        print("\nRelations with NA-values")
        print("========================")
        for key, value in dm.na_relations.items():
            if len(value) > 0:
                print(key, value)


if __name__ == "__main__":
    cli()
