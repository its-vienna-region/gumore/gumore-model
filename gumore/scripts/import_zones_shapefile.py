"""

"""

import os
import sys

sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..")))
sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..", "gumore", "util")))

import argparse

from gumore.util.zonesprep import import_zones_shapefile_into_database

parser = argparse.ArgumentParser(
    description="Import traffic zones shapefile into database",
    epilog="Example usage: python import_zones_shapefile.py "
    "NUTS1_Europamodell_mainzone.SHP vb_nuts1",
)
parser.add_argument(
    "path", help="Path to input file, relative to GÜMORE data basepath.", type=str
)
parser.add_argument(
    "zones_table_name",
    help="Name of zones table. This table will be "
    "created and filled with the imported data.",
    type=str,
)
parser.add_argument("-e", "--source_encoding", help="Text encoding of input file.")
args = parser.parse_args()


def run():
    if not args.source_encoding:
        import_zones_shapefile_into_database(
            filename=args.path, zones_table_name=args.zones_table_name
        )
    else:
        import_zones_shapefile_into_database(
            filename=args.path,
            zones_table_name=args.zones_table_name,
            source_encoding=args.source_encoding,
        )


if __name__ == "__main__":
    run()
