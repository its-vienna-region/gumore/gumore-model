"""
Deletes virtual links from graph table.

Relevant configuration section:

    [database.<DBNAME>]
    graph_table_name=<TABLE_NAME>
"""
import os
import sys

sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..")))
sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..", "gumore", "util")))

from gumore.core.graph import VirtualLinks


def run():
    links = VirtualLinks()
    links.delete_virtual_links_from_graph_table()


if __name__ == "__main__":
    run()
