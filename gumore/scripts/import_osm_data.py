"""
Imports osm data into database:

    * download
    * conversion
    * import
    * boolean flags creation
    * reverse cost correction
    * spatial index creation

Configuration is done directly in the source code of the script.
"""

from gumore.util.netprep import prepare_osm_net


def run():
    region = "europe-latest"
    fmt = ".osm.pbf"
    prefix = "eu_dense"
    indices = ("hgv", "rail")

    prepare_osm_net(region=region, fmt=fmt, prefix=prefix, indices=indices)


if __name__ == "__main__":
    run()
