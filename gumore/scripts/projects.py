import click

from gumore.util.projects import init_project


@click.group()
def cli():
    pass


@cli.command(name="init")
@click.argument("proj_dir", type=click.Path())
def init(proj_dir):
    init_project(proj_dir)


if __name__ == "__main__":
    cli()
