import os

from geoalchemy2 import Geometry
import geopandas as gpd
import networkx as nx
import numpy as np
import pandas as pd
from shapely.geometry import asLineString
from shapely import wkt


from gumore.util import global_vars

global_vars.proj_dir = os.path.join(global_vars.projects_path, "default")

# import append_sys_path
from gumore.core.graph import Distmat
from gumore.util.conf import get_config, get_absolute_path
from gumore.util.database import get_engine
from gumore.util.log import *

initialize_logger()
logger = logging.getLogger(__name__)


def run():
    NAME_COLUMN_NAME = "code"
    ADDITIONAL_COLUMN_NAMES = "a b c d e f g h i j k l m n o p q r s ew".split()
    DISTMAT_TABLE_NAME = "cost_zones_at_hgv_hours"
    ID_COLUMN_NAME = get_config().get("graph", "zones_id_column_name")

    SCHEMA = "scenario_1"

    engine = get_engine()

    # read data (zones, trips, and distmats)
    print("Loading zones ...")
    df_zones = gpd.read_postgis(
        """SELECT
               *
           FROM
               zones
        """,
        engine,
        geom_col="geom_point",
    )
    df_zones["x"] = df_zones.geometry.x
    df_zones["y"] = df_zones.geometry.y

    print("Loading trips ...")
    df_trips = pd.read_sql(
        f"""SELECT
               t.f, zf.bundesland f_bundesland, zt.bundesland t_bundesland, t.t, t.means, string_agg(t.vessel, '|' order by vessel) vessels, sum(t.count) count
           FROM
               {SCHEMA}.trips t
           join zones zf
               on t.f = zf.gid
           join zones zt
               on t.t = zt.gid
           GROUP BY
               t.f, t.t, t.means, zf.bundesland, zt.bundesland
        """,
        engine,
    )

    print("Loading distance matrix ...")
    dm_hours = Distmat()
    dm_hours.read_from_db(DISTMAT_TABLE_NAME)

    # add distmat data to trips
    print("Joining distance matrix to trips ...")
    df_trips["hours"] = df_trips.apply(lambda x: dm_hours.df.at[x.f, x.t], axis=1)
    df_trips = df_trips[df_trips.f != df_trips.t]

    # calc maxima per vessel class (for qgis symbology)
    # print("Calculating max_count ...")
    # max_counts = (
    #     df_trips.groupby("vessel").max()["count"].rename("max_count", inplace=True)
    # )
    # df_trips = df_trips.merge(
    #     max_counts, how="left", left_on="vessel", right_index=True
    # )

    print("Creating graph ...")
    # create emtpy graph object
    G = nx.MultiDiGraph()

    # create nodes from dataframe
    print("Creating nodes ...")
    for row in df_zones.itertuples():
        atts = {}
        atts["id"] = getattr(row, ID_COLUMN_NAME)
        atts["name"] = getattr(row, NAME_COLUMN_NAME)
        atts["x"] = row.x
        atts["y"] = row.y
        for add_col in ADDITIONAL_COLUMN_NAMES:
            atts[add_col] = getattr(row, add_col)
        G.add_nodes_from([(getattr(row, ID_COLUMN_NAME), atts)])

    # create edges
    print("Creating edges ...")
    attribute_names = (
        # "commodities",
        # "vessel",
        "f",
        "f_bundesland",
        "t",
        "t_bundesland",
        "hours",
        "means",
        "vessels",
        "count",
        # "max_count",
    )
    for row in df_trips.itertuples():
        att_dict = {x: getattr(row, x) for x in attribute_names}

        G.add_edges_from([(row.f, row.t, att_dict)])

    # convert graph to dataframe
    print("Converting graph to dataframe ...")
    vessel_types = df_trips.means.unique()
    edgelists = {}
    for vessel_type in vessel_types:
        edgelists[vessel_type] = [
            (x[0], x[1]) for x in G.edges(data="means") if x[2] == vessel_type
        ]

    node_mapping = {x[0]: (x[1]["x"], x[1]["y"]) for x in dict(G.nodes.data()).items()}
    G_out = nx.relabel_nodes(G, node_mapping)
    df_out = nx.to_pandas_edgelist(G_out)
    df_out["geom"] = df_out.apply(
        lambda x: str(asLineString(np.array([x.source, x.target]))), axis=1
    )
    df_out["geom"] = df_out.geom.apply(wkt.loads)

    df_out = gpd.GeoDataFrame(df_out, geometry="geom")
    df_out.crs = {"init": "epsg:4326"}
    df_out = df_out.to_crs(epsg=31256)
    df_out = df_out.drop(["source", "target"], axis=1)
    df_out = df_out[list(attribute_names + ("geom", ))]
    df_out.geom = df_out.geom.apply(lambda x: "SRID=31256;" + wkt.dumps(x))

    # write to database
    print("Write to database ...")
    df_out.to_sql(
        "tripsvis",
        engine,
        if_exists="replace",
        index=False,
        schema=SCHEMA,
        dtype={"geom": Geometry("LINESTRING", srid=31256)},
    )

    # write to file
    print("Write to file ...")
    graph_out_path = os.path.join(
        get_config().get("paths", "data"),
        get_config().get("paths", "data_trips_vis_graph_out"),
    )

    G_out.remove_nodes_from(list(nx.isolates(G_out)))

    for i, d in G_out.nodes(data="x"):
        G_out.nodes.data()[i]["x"] = G_out.nodes.data()[i]["x"] * 1000
    for i, d in G_out.nodes(data="y"):
        G_out.nodes.data()[i]["y"] = (50 - G_out.nodes.data()[i]["y"]) * 1000

    nx.write_graphml(G_out, graph_out_path)
    print("DONE!")


if __name__ == "__main__":
    run()
