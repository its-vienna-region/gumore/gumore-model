import os

# import sys

import click

# sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..")))
# sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..", "gumore", "util")))


@click.group()
def cli():
    pass


@cli.command(name="run")
@click.argument("project-name")
def run_gm(project_name):
    from gumore.util import global_vars

    global_vars.proj_dir = os.path.join(global_vars.projects_path, project_name)
    from gumore.run import run

    run()


if __name__ == "__main__":
    run_gm()
