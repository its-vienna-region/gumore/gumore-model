"""
Adds virtual links to graph table name.

Relevant configuration sections:

    [graph]
    virtual_links_file=<PATH_TO_CSV_FILE>

    [database.<DBNAME>]
    graph_table_name=<TABLE_NAME>

Virtual Links get virtual=True in graph table.
"""
import os
import sys

sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..")))
sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..", "gumore", "util")))

from gumore.core.graph import VirtualLinks


def run():
    links = VirtualLinks()
    links_namedtuples = links.read_virtual_links_from_csv()
    links.create_virtual_links_in_database(links_namedtuples)
    links.append_virtual_links_to_graph_table()


if __name__ == "__main__":
    run()
