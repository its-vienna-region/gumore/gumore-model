"""

"""

import os
import sys

sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..")))
sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..", "gumore", "util")))

import argparse

from gumore.core.graph import Distmat


parser = argparse.ArgumentParser(
    description="Calculates a new distance matrix and stores the result in the GÜMORE "
    "database.",
    epilog="Example usage: python calculate_distmat cost_vb_nuts1_dense_hgv_hours "
    "travel_time hgv connect__vb_nuts1__to__eu_dense_2po_4pgr__hgv True "
    "-d True",
)
parser.add_argument(
    "new_distmat_table_name",
    help="Name of new distance matrix table (e.g. " "'cost_vb_nuts1_dense_hgv_hours').",
)
parser.add_argument(
    "resistance_type",
    help="Type of resistance to calculate costs (e.g. 'travel_time').",
    choices=["travel_time", "distance", "beeline"],
)
parser.add_argument(
    "means_of_transport",
    help="Means of transport for which costs are calculated " "(e.g. 'hgv').",
    choices=["hgv", "train"],
)
parser.add_argument(
    "connector_table_name",
    help="Name of database table containing the mapping zone -> "
    "connector (e.g. connect__vb_nuts1__to__eu_dense_2po_4pgr__hgv"
    ").",
)
parser.add_argument(
    "directed", help="True if graph is directed (e.g. 'True').", type=bool
)
parser.add_argument(
    "-d",
    "--delete_existing_table",
    help="If True, delete distance matrix table in database, if "
    "it already exists (e.g. 'True'). Defaults to 'False'. ",
    default=False,
    type=bool,
)
parser.add_argument(
    "-c",
    "--create_sample_table",
    help="If True, create distance matrix only for a sample of "
    "all possible relations (e.g. 'False'). Defaults to 'False'.",
    default=False,
    type=bool,
)
args = parser.parse_args()


def run():
    dm = Distmat()
    dm.create_in_db(
        new_distmat_table_name=args.new_distmat_table_name,
        resistance_type=args.resistance_type,
        means_of_transport=args.means_of_transport,
        connector_table_name=args.connector_table_name,
        directed=args.directed,
        delete_existing_table=args.delete_existing_table,
        create_sample_table=args.create_sample_table,
    )
    dm.read_from_db(distmat_table_name=args.new_distmat_table_name)

    print("\nCreated Distmat object")
    print("======================")
    print(dm)

    print("\nDistmat Dataframe")
    print("=================")
    print(dm.df)

    print("\nDataframe holds NA-values")
    print("=========================")
    print(dm.holds_na)

    if dm.holds_na:
        print("\nRelations with NA-values")
        print("========================")
        for key, value in dm.na_relations.items():
            if len(value) > 0:
                print(key, value)


if __name__ == "__main__":
    run()
