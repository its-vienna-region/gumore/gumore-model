"""

"""

import os
import sys

sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..")))
sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..", "gumore", "util")))

import argparse

from gumore.util.zonesprep import add_centroids_for_zones_in_database

parser = argparse.ArgumentParser(
    description="Import traffic zones shapefile into database",
    epilog="Example usage: python add_centroids_to_zones.py vb_nuts1 gid",
)
parser.add_argument(
    "zones_table_name",
    help="Name of zones table. This table will be "
    "altered by adding a new geometry column.",
    type=str,
)
parser.add_argument(
    "primary_key_column", help="Name of column containing the primary " "key.", type=str
)
args = parser.parse_args()


def run():
    add_centroids_for_zones_in_database(
        zones_table_name=args.zones_table_name,
        primary_key_column=args.primary_key_column,
    )


if __name__ == "__main__":
    run()
