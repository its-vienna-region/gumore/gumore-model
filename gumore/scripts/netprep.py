import os

# import sys

import click

# sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..")))
# sys.path.append(os.path.normpath(os.path.join(sys.path[0], "..", "gumore", "util")))


@click.group()
def cli():
    pass


@cli.command(name="virtuallinks")
@click.argument("project-name")
def add_virtual_links(project_name: str):
    """
    Adds virtual links to graph table.

    This can be used to connect islands or to handle OSM data errors resulting in
    invalid connectors.

    Example usage::

        gm-netprep virtuallinks demo
    """
    from gumore.util import global_vars

    global_vars.proj_dir = os.path.join(global_vars.projects_path, project_name)
    from gumore.scripts.add_virtual_links import run

    run()


@cli.command(name="all")
@click.option(
    "-r",
    "--region",
    required=True,
    help="region part of the url, e.g. 'europe-latest' or 'europe/andorra-latest'",
)
@click.option(
    "-i",
    "--index",
    "indices",
    required=True,
    multiple=True,
    help="Column name for which an index will be created. Can be repeated",
)
@click.option(
    "-f",
    "--format",
    "fmt",
    default=".osm.pbf",
    show_default=True,
    help="Format part of the url, e.g. '.osm.pbf'",
)
@click.option(
    "-p",
    "--prefix",
    required=True,
    help="Prefix used by osm2po to create result directory to get input file, "
    "e.g. 'eu' or 'ad'."
    "The prefix will also be used for naming the database table.",
)
@click.argument("project-name")
def prepare(region, fmt, prefix, indices, project_name):
    """
    Wrapper function for download, conversion, import, boolean flags creation, reverse cost correction,
    and spatial index creation.

    Relevant configuration sections::

        \b
        [paths]
        data=<DIRECTORY_PATH>
        data_osm=<DIRECTORY_PATH>
        data_osm2po=<DIRECTORY_PATH>
        osm2po_jar=<PATH_TO_JAR_FILE>

        \b
        [model_config]
        osm2po_path=<PATH_TO_CONFIG_FILE>

    Example usage::

        gm-netprep all -r europe/iceland-latest -i hgv -i rail -p is demo
    """
    from gumore.util import global_vars

    global_vars.proj_dir = os.path.join(global_vars.projects_path, project_name)
    from gumore.util.netprep import prepare_osm_net

    prepare_osm_net(region, fmt, prefix, indices)


if __name__ == "__main__":
    cli()
