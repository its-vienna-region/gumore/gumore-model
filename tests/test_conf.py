import os

# import append_sys_path

import pytest


@pytest.fixture
def active_config_name():
    import gumore.util.conf

    active_config_name = gumore.util.conf._get_active_config_name()
    return active_config_name


@pytest.fixture
def config():
    import gumore.util.conf

    config = gumore.util.conf.get_config()
    return config


def test_active_config_name(active_config_name):
    projects_path = os.path.join(os.path.expanduser("~"), "GumoreProjects")
    assert active_config_name.split(".")[1] == "conf"
    assert os.path.exists(
        os.path.join(projects_path, "demo", "config", active_config_name)
    )


def test_config_mandatory_sections_and_keys(config):
    sections = config.sections()
    mandatory_sections = (
        "logging",
        "logging.console",
        "logging.error",
        "logging.debug",
        "graph",
        "model_config",
        "entities",
        "paths",
        "database",
    )

    mandatory_tuple = (
        ("logging", ("level", "log_rule_fitting")),
        ("logging.console", ("active", "level", "format", "datefmt")),
        ("logging.error", ("active", "level", "format", "datefmt", "filename")),
        ("logging.debug", ("active", "level", "format", "datefmt", "filename")),
        (
            "graph",
            (
                "distmat_hgv_hours",
                "distmat_hgv_km",
                "distmat_train_hours",
                "distmat_train_km",
                "distmat_beeline_km",
            ),
        ),
        ("model_config", ("sheet_path",)),
        ("entities", ("import",)),
        ("paths", ("log",)),
        ("database", ("active_db_conf_name",)),
    )

    # sections
    for section in mandatory_sections:
        assert section in sections

    sections_str = " ".join(sections)
    assert sections_str.count("database") > 1

    # keys
    for section, keys in mandatory_tuple:
        for key in keys:
            assert config.has_option(section, key)


def test_config_active_db(config):
    db_conf_name = "database." + config.get("database", "active_db_conf_name")
    assert db_conf_name in config.keys()
    assert config.get(db_conf_name, "type") in ("sqlite", "postgresql")

    # mandatory postgresql keys
    if config.get(db_conf_name, "type") == "postgresql":
        mandatory_keys = (
            "type",
            "password_file",
            "graph_table_name",
            "zones_table_name",
        )
        for key in mandatory_keys:
            assert config.has_option(db_conf_name, key)

    db_conf_file_path = os.path.join(
        config.get("paths", "basepath"),
        "config",
        config.get(db_conf_name, "password_file"),
    )
    assert os.path.exists(db_conf_file_path)


def test_get_config(config):
    import configparser
    import pandas as pd
    from gumore.util import global_vars

    assert isinstance(config, configparser.ConfigParser)
    assert isinstance(global_vars.config, configparser.ConfigParser)
    assert config == global_vars.config
    assert len(global_vars.matrices_model_config) > 0
    assert all(
        [
            isinstance(x, pd.core.frame.DataFrame)
            for x in global_vars.matrices_model_config.values()
        ]
    )


def test_get_absolute_path(config):
    section = "paths"
    path_key = "log"
    path = config.get(section, path_key)
    if not os.path.isabs(path):
        from gumore.util.conf import get_absolute_path

        abs_path = get_absolute_path(config, section, path_key)
        assert os.path.isabs(abs_path)
        assert os.path.basename(abs_path) == path


def test__add_model_config_from_spreadsheet(config):
    from collections.abc import MutableMapping
    import pandas as pd
    from gumore.util.conf import get_absolute_path, _add_model_config_from_spreadsheet

    sheet_path = get_absolute_path(config, "model_config", "sheet_path")
    config_new, matrices = _add_model_config_from_spreadsheet(config, sheet_path)
    assert isinstance(matrices, MutableMapping)
    assert len(matrices) > 0
    assert all([isinstance(x, pd.core.frame.DataFrame) for x in matrices.values()])
