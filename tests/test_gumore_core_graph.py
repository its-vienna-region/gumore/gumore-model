import pytest

# import append_sys_path


@pytest.fixture
def distmat():
    from gumore.core import graph

    dm = graph.Distmat()
    yield dm


def test_distmat_attributes(distmat):
    import pandas as pd

    assert type(distmat.df) == pd.core.frame.DataFrame
    assert len(distmat.df.shape) == 2

    assert type(distmat.metadata) == dict
    mandatory_metadata_keys = (
        "distmat_table_name",
        "resistance_type",
        "means_of_transport",
        "connector_table_name",
        "directed",
    )
    for key in mandatory_metadata_keys:
        assert key in distmat.metadata.keys()


# TODO: create dummy db (fixture ?) for testing of db-related methods
