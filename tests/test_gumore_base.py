import pytest

# import append_sys_path
import gumore.core.flow


@pytest.fixture
def app():
    from gumore.core.rules import means

    app = means.gumore_app
    return app


@pytest.fixture
def rule():
    from gumore.core.rules import means

    rule = means.GumoreRule("dummy_context")
    yield rule


@pytest.fixture
def flow():
    flow = gumore.core.flow.Flow(0, 1, "dummy_commodity", 10)
    yield flow


def test_gumore_rule_condition(rule, flow):
    assert rule.condition(flow) is True


def test_gumore_rule_action(rule, flow):
    with pytest.raises(NotImplementedError):
        rule.action(flow)
