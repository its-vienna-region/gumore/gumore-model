import inspect

import pytest

# import append_sys_path
import gumore.core.flow


@pytest.fixture
def app():
    from gumore.core import runner

    app = runner.gumore_app
    return app


@pytest.fixture
def graph():
    from gumore.core import runner

    graph = runner.gumore_app.graph
    return graph


@pytest.fixture
def rule():
    from gumore.core.rules import means

    rule = means.GumoreRule("dummy_context")
    yield rule


@pytest.fixture
def flow():
    flow = gumore.core.flow.Flow(0, 1, "dummy_commodity", 10)
    yield flow


@pytest.fixture
def gumore_gen_classes():
    from gumore.core.runner import gumore_app

    gen_classes = gumore_app.generators.generators
    return gen_classes


@pytest.fixture
def gumore_rule_classes():
    from gumore.core.runner import gumore_app

    rule_classes = gumore_app.rules.rules
    return rule_classes


def test_gumore_rule(gumore_rule_classes):
    assert 1 == 1
