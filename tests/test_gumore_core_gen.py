import pytest

# from gumore.core.entities import com_nst01
from gumore.core.rules.gen import GumoreGen, GumoreGenSet
from gumore.core.graph import Graph


@pytest.fixture(scope="module")
def graph():
    return Graph()


class GenNST1(GumoreGen):
    commodity = "com_nst01"

    def distribute(self):
        pass


class GenNST2(GumoreGen):
    commodity = "com_nst02"

    def distribute(self):
        pass


@pytest.fixture(scope="module")
def gen_nst1(graph):
    return GenNST1(graph=graph)


@pytest.fixture(scope="module")
def genset(graph):
    return GumoreGenSet(graph=graph)


class TestGumoreGen:
    def test_init(self, gen_nst1, graph):
        assert gen_nst1.graph == graph


class TestGumoreGenSet:
    def test_init(self, genset, graph):
        assert genset.graph == graph
        assert genset.generators == []

    def test_register(self, genset, gen_nst1):
        genset.register(GenNST1)
        assert genset.generators[0] == GenNST1
        assert len(genset.generators) == 1
        genset.register(GenNST2)
        assert genset.generators[1] == GenNST2
        assert len(genset.generators) == 2
