import pandas as pd

from gumore.run import run
from gumore.util.database import get_engine
from gumore.util.conf import get_config


def get_trips():
    engine = get_engine(get_config())
    df = pd.read_sql_table("trips", engine)
    return df


def run_model():
    try:
        run()
    except SystemExit:
        pass


def test_simple_run():
    run_model()
    df_trips = get_trips()
    trips_cases = [
        [[7, 1, "L75 NST 03"], [1753, "nst03,empty", "L75"]],
        # [[6, 29, "L26 Allround"], [1, "nst03", "L26"]],
    ]
    for case in trips_cases:
        assert (
            list(
                df_trips.loc[
                    (df_trips["f"] == case[0][0])
                    & (df_trips["t"] == case[0][1])
                    & (df_trips["vessel"] == case[0][2]),
                    ["count", "commodities", "means"],
                ].values[0]
            )
            == case[1]
        )
