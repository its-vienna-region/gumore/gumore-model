# CHANGELOG
### 2020-02-21 rl
* dockerize the model by adding Dockerfiles and docker-compose.yml

### 2019-05-15 rl
* add script for visualization of calculated trips (`scripts.create_trips_geometries`)

### 2019-05-10 rl
* implement functions for automatic zone creation (`util.zonesprep`)

### 2019-05-09 rl
* implement functions for automatic net creation (`util.netprep`)

### 2019-05-08 rl
* implement process for creation of virtual links (`core.graph.VirtualLinks`)

### 2019-04-09 rl
* implement vessel trip calculation (`core.vessels.SimpleVessel`, `SimpleVesselSet`)

### 2019-04-02 rl
* implement parallel processing for flow splitting / mode assignment

### 2019-04-01 rl
* implement parallel processing for flow generation
* add ability to pickle and unpickle distance matrices

### 2019-03-25 rl
* consider load factor depending on commodity for Vessel count calculation (`model.vessels`)

### 2019-03-22 rl
* read model config from excel (`util.conf`)

### 2019-03-18 rl
* add TransportMeansMix class (`core.entities`)
* add Commodity class (`core.entities`)
* add Vessel class and subclasses (`model.vessels`)

### 2019-03-15 rl
* add TransportMode and TransportMeans class (`core.entities`)

### 2019-03-12 rl
* refactor Graph class (including usage of Zones instead of Cell) (`core.graph`)

### 2019-03-07 rl
* add Distmat class

