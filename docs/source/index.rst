.. GÜMORE documentation master file, created by
   sphinx-quickstart on Fri Mar  1 10:12:37 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

GÜMORE
======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   core_modules
   util_modules
   scripts






Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
