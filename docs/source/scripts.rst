Command Line Scripts
====================

.. click:: gumore.scripts.netprep:cli
    :prog: gm-netprep
    :show-nested:

.. click:: gumore.scripts.zonesprep:cli
    :prog: gm-zonesprep
    :show-nested:
