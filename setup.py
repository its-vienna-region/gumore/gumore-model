import os

from setuptools import setup, find_packages
from setuptools.command.install import install
from setuptools.command.develop import develop

from gumore.util.projects import init_project, get_proj_path

PROJ_NAME = "demo"
PROJ_PATH = get_proj_path(PROJ_NAME)

# if os.path.exists(PROJ_PATH):
#     raise RuntimeError(
#         f"Demo directory '{PROJ_PATH}' already exists. Please delete it " f"manually."
#     )
#
#
# class PostDevelopCommand(develop):
#     def run(self):
#         init_project(PROJ_NAME)
#         develop.run(self)
#
#
# class PostInstallCommand(install):
#     def run(self):
#         init_project(PROJ_NAME)
#         install.run(self)


with open("README.md", "r") as fh:
    long_description = fh.read()


# TODO: include all console scripts
# TODO: add documentation

setup(
    name="gumore",
    version="0.3",
    author="Roland Lukesch",
    author_email="roland.lukesch@its-viennaregion.at",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/its-vienna-region/gumore",
    packages=find_packages(),
    python_requires=">=3.6.*",
    install_requires=[
        "Click>=7.0",
        "pandas>=1.0.0",
        "pathos==0.2.5",
        "psutil>=5.6.7",
        "psycopg2>=2.8.4",
        "requests>=2.22.0",
        "sqlalchemy>=1.3.13",
        "xlrd>=1.2.0",
    ],
    package_data={
        "gumore": [
            "config/*",
            "data/flows_emp/*",
            "data/geo/*",
            "data/mymodel/*",
            "data/osm2po/*",
            "data/osm2po/osm2po-doc/*",
            "data/osm2po/osm2po-plugins/*",
            "data/scenarios/*",
        ]
    },
    entry_points="""
        [console_scripts]
        gm-netprep=gumore.scripts.netprep:cli
        gm-projects=gumore.scripts.projects:cli
        gm-run=gumore.run:run
        gm-zonesprep=gumore.scripts.zonesprep:cli
    """,
)

# cmdclass = {"install": PostInstallCommand, "develop": PostDevelopCommand},
