# ToDos für GUMORE

## Prio 1 = ASAP
* Strings (z.B. Modi, ...) in Objkte umbauen = Sicherheit gegen Typos
* Mehrere Widerstandsmatrizen in der App nach Namen aufrufbar machen
* Widerstandsmatrizen laden und speichern
* ! Flexibles Laden von Zellattributen aus Datenbank



## Prio 2 = Nach Check mit Projektpartnern
* Stimmt die Richtung -> Moeckel, Hofer, Krause, Lukesch ?
* Datenbank-Format für Graphen festlegen -> Krause ?
* Import des Graphen aus GIP & Verkehrsmodell schreiben -> Krause ?
* Oberfläche in QGIS einbauen, Belastungen visualisieren -> Lukesch ?
* Einzelstreckenumlegung zum Testen in QGIS einbauen -> Lukesch ?
* Zellen-Attribute direkt aus der Roland-Applikation einlesen -> Lukesch ?
* Werkel zum Einsammeln und Bewerten der trips in Fahrtenmatrizen bauen -> Moeckel ?


## Prio 3 = upon request
* Syntactic sugar für Modell
* Weitere Funktionen und Verfahren (Probit, Cellenauswahl, ....) 


## Prio 4 = zur Produktivsetzung
* Laufzeitoptimierung
* Paralellisierung
