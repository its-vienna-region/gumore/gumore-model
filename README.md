# Introduction

__Attention: This project is in active development!
It may or may not be stable. In the future, stable releases will be provided.__

# Installation
First, you need to clone this Git repository:

    git clone https://gitlab.com/its-vienna-region/gumore/gumore-model.git
    
There are two options to run the GÜMORE model: 
* run inside Docker containers
* install locally

## Option 1: Run inside Docker containers
### Overview 
The application consists of two containers, `database` and `gumore`, which are
tied together by `docker-compose`. The `database` container needs
to be running for enabling the full functionality of the `gumore` container.

After the first run of the `gumore` container a `GumoreProjects` directory is created
(as Docker bind mount) in your user home directory (`~/GumoreProjects` in *nix systems). 
All project configurations can be edited there. Due to Docker's permission system, 
the directory can only be edited with root privileges. This directory is in sync 
with `/root/GumoreProjects` inside the container.

The PostgreSQL database can be reached on the __host machine__ via:

    host:     database
    port:     54321
    database: gumore_demo
    user:     postgres
    password: a16b32c64

e.g. by using `psql`:

    psql -h localhost -U postgres -p 54321 -d gumore_demo
    
You can use port 5432 if you want to reach it from __inside the `gumore` container__:

    psql -h database -U postgres -p 5432 -d gumore_demo

### Requirements
* __Docker__
* __docker-compose__

### Installation instructions
1. Install [Docker](https://docs.docker.com/install/) and 
[docker-compose](https://docs.docker.com/compose/install/)
1. `cd` into the git-cloned directory.
1. Build and run both containers:

        docker-compose up
    
    This stops the `gumore` container immediately after start, but leaves the `database` 
container running.

1. Open a second terminal.
1. `cd` into the cloned directory.
1. Start an interactive session in the `gumore` container:

        docker-compose run gumore

1. If you want to create and run a demo project, take a look at the section 
[Create Demo Database (Iceland) 
and run demo model](#create-demo-database-iceland-and-run-demo-model) below.

## Option 2: Install locally
### Requirements
On the machine where the GÜMORE model runner will be installed:
* __Python 3.6__ or above
* Java Runtime Environment (__JRE__)
* __GDAL__ command line tools `ogr2ogr`
* __PostgreSQL__ command line tool `psql`
* __PostGIS__ command line tool `shp2pgsql`

Additionally, on the same machine or somewhere else:
* __PostgreSQL__ database server with extensions `PostGIS` and `pgrouting` ready to be
created inside database 

### Installation instructions
##### Requirements
1. Install requirements
1. Make sure to set all environment variables correctly, e.g. `JAVA_HOME`, `PYTHON_HOME`, 
`PATH`. `PATH` has to include the path to the PostgreSQL command line binaries,
the Git binaries, the JAVA binaries etc.

##### Prepare PostgreSQL
1. Create a PostgreSQL database `gumore_demo`.
1. Consider creating a PostgreSQL password file 
(https://www.postgresql.org/docs/10/libpq-pgpass.html).

##### Create virtual environment
1. `virtualenv` (https://pypi.org/project/virtualenv/) is recommended.
1. Don't forget to activate the environment!

##### Install `gumore` Python package
1. `cd` into the cloned directory
1. pip-install:

        pip install .
    
##### Demo project
If you want to create and run a demo project, take a look at 
the [next](#create-demo-database-iceland-and-run-demo-model) section.


# Create Demo Database (Iceland) and run demo model
### Download and prepare demo data
1. Create a demo project:

        gm-projects init demo
        
1. Edit the config file `<USER_HOME>\GumoreProjects\demo\config\pw_postgres.conf` to 
add your database connection info.

1. Download and import demo OSM network:
    
        gm-netprep all -r europe/iceland-latest -i hgv -i rail -p is demo
1. Import demo zones:

        gm-zonesprep all -f is50v_mork_umdaemi_svaedi_24122019.shp -z zones -p gid -m hgv -g is -bb -24.3 63.4 -13.7 66.6 -d True demo
1. Calculate distance matrices:
    
        gm-zonesprep distmat -n cost_zones_is_hgv_hours -r travel_time -m hgv -c connect__zones__to__is_2po_4pgr__hgv demo
    
        gm-zonesprep distmat -n cost_zones_is_hgv_km -r distance -m hgv -c connect__zones__to__is_2po_4pgr__hgv demo

### Demo model configuration
The OSM import is configured via this config file: 
`<USER_HOME>/GumoreProjects/config/osm2po.conf`

The model is configured via the following files:
  * multiple spreadsheet tables in 
  `<USER_HOME>/GumoreRules/demo/mymodel/model_config.xlsx`
  * the INI file `<USER_HOME>/GumoreProjects/demo/config/gumore.conf`
  * the modules in the Python package `<USER_HOME>/GumoreProjects/demo/mymodel`

For a simple test run, the demo configuration does not need to be changed.

### Run the demo model

    gm-run demo

You may wish to take a look at the `trips` database table, which holds the final 
results of each model run. Additionally, think about visualizing your model data 
and results with [QGIS](https://qgis.org/de/site/) or any other GIS system.


# Documentation
## Overview
### Run GUMORE model
To run the GUMORE model with standard parameters:
* Activate your environment
* run `gm-run <project-name>`

### Command Line Scripts
* `gm-run`
* `gm-projects`
* `gm-netprep`
* `gm-zonesprep`

## Module level
[Documentation](https://its-vienna-region.gitlab.io/gumore/gumore-model/)

# Developer's Guide

## Documentation
Documentation is created from docstrings using `sphinx` library with autodoc-extension.

Docstring style: [Google Style](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)

To build html documentation run `docs/make_documentation_html.bat` (Windows) or 
`docs/make_documentation_html.sh` (Linux).

Install sphinx, the `readthedocs` theme, and the `sphinx-click` extension:

    pip install sphinx
    pip install sphinx-rtd-theme
    pip install sphinx-click
    
  

## Tests
Testing is done using `pytest` library. 
To run all tests start `test.bat` (Windows) or `test.sh` (Linux)


## Changelog
[CHANGELOG.md](CHANGELOG.md)

## Sources
* Iceland Districts: https://www.lmi.is/en/stafraen-gogn/is-50v-nytt/